<?php
    
    /**
     * The base configuration for WordPress
     *
     * The wp-config.php creation script uses this file during the
     * installation. You don't have to use the web site, you can
     * copy this file to "wp-config.php" and fill in the values.
     *
     * This file contains the following configurations:
     *
     * * MySQL settings
     * * Secret keys
     * * Database table prefix
     * * ABSPATH
     *
     * @link    https://codex.wordpress.org/Editing_wp-config.php
     *
     * @package WordPress
     */
    if ( file_exists ( dirname ( __FILE__ ) . '/local-config.php' ) ) {
        include ( dirname ( __FILE__ ) . '/local-config.php' );
    }
    else if ( file_exists ( dirname ( __FILE__ ) . '/staging-config.php' ) ) {
        include ( dirname ( __FILE__ ) . '/staging-config.php' );
    }
    else {
        /** WP Environment */
        define ( 'WP_ENV', 'live' );
        /** Turn Debugging OFF */
        define ( 'WP_DEBUG', false );
        /** The name of the database for WordPress */
        define ( 'DB_NAME', 'interfaceforce_wordpress' );
        /** MySQL database username */
        define ( 'DB_USER', 'interfaceforce_wpadmin' );
        /** MySQL database password */
        define ( 'DB_PASSWORD', 'asdasdasd' );
        /** MySQL hostname */
        define ( 'DB_HOST', 'localhost' );
        /* Home and Site Url */
        define ( 'WP_HOME', 'http://interfaceforce.com' );
        define ( 'WP_SITEURL', 'http://interfaceforce.com' );
    }
    /** Database Charset to use in creating database tables. */
    define ( 'DB_CHARSET', '' );
    /** The Database Collate type. Don't change this if in doubt. */
    define ( 'DB_COLLATE', '' );
    /**
     * Authentication Unique Keys and Salts.
     *
     * Change these to different unique phrases!
     * You can generate these using the
     * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service} You can change these at
     * any point in time to invalidate all existing cookies. This will force all users to have to log in again.
     *
     * @since 2.6.0
     */
    define ( 'AUTH_KEY', '*EO$ B-Q$ft9%p?t]TLdm-mO{/R+qk|$Z0RQ|s+mB#7[:h7a(&b-K8qrSQ-@qFKE' );
    define ( 'SECURE_AUTH_KEY', '9)N|X|z?5{wOlNDgPXV1t1`@q+d:*-+4e_>%MJWe:Avc`<74Gpiq6xa=N>R;Vc~_' );
    define ( 'LOGGED_IN_KEY', '_YxRo1mjc+uq_-LDCmSA;#J[mt=+=Sf7uctv)~=c8|_txl?L7DY>PDnprq,q:s0D' );
    define ( 'NONCE_KEY', '*f,z30lT0$-+GMK7WU: *{^n<J|{+BqI=Qtbl`{`?OWfCK|]d9<w^b/W8<%kVbCY' );
    define ( 'AUTH_SALT', 'w>]r@JRUZ8x-ME$Fezh*4a*85cEZ+C-yk0c*sM}-(De^gJB]ki7+MoDl{q{:]F%l' );
    define ( 'SECURE_AUTH_SALT', '%23=iDG|gf]|sNEz~B2Td[IbZbt`fHOK&hb+_pDG@:-+{mR|]@~!=-9)hy00KHBs' );
    define ( 'LOGGED_IN_SALT', 'tsG0.CR.4qiBdHKrB.Jp&^E-ww-n7;AcC5^7[71^xVyi^-hw+5.8z#_l0#r+{RAL' );
    define ( 'NONCE_SALT', '`Jf&+F+Zh]n?sy3Km3Q%3r<-JlZSNdO-)tW.U KA6@6zS0b7/,:N2bl$H`7u>IQa' );
    /**
     * WordPress Database Table prefix.
     *
     * You can have multiple installations in one database if you give each
     * a unique prefix. Only numbers, letters, and underscores please!
     */
    $table_prefix = 'wp_';
    /** Debug */
    if ( WP_DEBUG ) {
        /* Tells WordPress to log everything to the /wp-content/debug.log file */
        define ( 'WP_DEBUG_LOG', true );
        /* Doesn't force the PHP 'display_errors' variable to be on */
        define ( 'WP_DEBUG_DISPLAY', false );
        /* Hides errors from being displayed on-screen */
        @ini_set ( 'display_errors', 0 );
        /* Script Debug - default off */
        define ( 'SCRIPT_DEBUG', false );
        /* Save Queries - default off */
        define ( 'SAVEQUERIES', false );
        /* WC Debug */
        define ( 'WCS_DEBUG', true );
    }
    /* Increase Memory Limit */
    define ( 'WP_MEMORY_LIMIT', '2048M' );
    /* Define Revisions Limit */
    define ( 'WP_POST_REVISIONS', 3 );
    /* That's all, stop editing! Happy blogging. */
    /** Absolute path to the WordPress directory. */
    if ( ! defined ( 'ABSPATH' ) ) {
        define ( 'ABSPATH', dirname ( __FILE__ ) . '/' );
    }
    /** Sets up WordPress vars and included files. */
    require_once ( ABSPATH . 'wp-settings.php' );
