<?php
    /**
     * Staging Database Configuration File
     *
     * Used for local configuration of the WordPress database.
     *
     * @since   1.0.0
     */
    
    /** WP Environment */
    define ( 'WP_ENV', 'staging' );
    /** The name of the database for WordPress */
    define ( 'DB_NAME', 'interfaceforce_wp' );
    /** MySQL database username */
    define ( 'DB_USER', 'interfaceforce_wpu' );
    /** MySQL database password */
    define ( 'DB_PASSWORD', '1ybu2Z4suQ5wji!bz9nd5$' );
    /** MySQL hostname */
    define ( 'DB_HOST', 'localhost' );
    /** Turn Debugging OFF */
    define ( 'WP_DEBUG', false );
    /* Jetpack Development Mode */
    define ( 'JETPACK_DEV_DEBUG', true );
    /* Home and Site Url */
    define ( 'WP_HOME', 'http://interfaceforce.skybox0.com' );
    define ( 'WP_SITEURL', 'http://interfaceforce.skybox0.com' );
