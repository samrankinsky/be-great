<?php
    /**
     * Local Database Configuration File
     *
     * Used for local configuration of the WordPress database.
     *
     * @since   1.0.0
     */
    
    /** WP Environment */
    define ( 'WP_ENV', 'dev' );
    /** MySQL database name */
    define ( 'DB_NAME', 'local' );
    /** MySQL database username */
    define ( 'DB_USER', 'root' );
    /** MySQL database password */
    define ( 'DB_PASSWORD', 'root' );
    /** MySQL hostname */
    define ( 'DB_HOST', 'localhost' );
    /** Turn Debugging ON */
    define( 'WP_DEBUG', true );
    define( 'WP_DEBUG_DISPLAY', false );
    define( 'WP_DEBUG_LOG', true );
    
    /* Jetpack Development Mode */
    define ( 'JETPACK_DEV_DEBUG', true );
    /* Home and Site Url */
    define ( 'WP_HOME', 'http://be-great.local' );
    define ( 'WP_SITEURL', 'http://be-great.local' );
    /* Inserted by Local by Flywheel. See: http://codex.wordpress.org/Administration_Over_SSL#Using_a_Reverse_Proxy */
    if ( isset( $_SERVER['HTTP_X_FORWARDED_PROTO'] ) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ) {
        $_SERVER['HTTPS'] = 'on';
    }
