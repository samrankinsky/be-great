<?php
    /**
     * The base configuration for WordPress
     *
     * The wp-config.php creation script uses this file during the
     * installation. You don't have to use the web site, you can
     * copy this file to "wp-config.php" and fill in the values.
     *
     * This file contains the following configurations:
     *
     * * MySQL settings
     * * Secret keys
     * * Database table prefix
     * * ABSPATH
     *
     * @link    https://codex.wordpress.org/Editing_wp-config.php
     *
     * @package WordPress
     */
    
    // ** MySQL settings ** //
    /** The name of the database for WordPress */
    define( 'DB_NAME', 'local' );
    /** MySQL database username */
    define( 'DB_USER', 'root' );
    /** MySQL database password */
    define( 'DB_PASSWORD', 'root' );
    /** MySQL hostname */
    define( 'DB_HOST', 'localhost' );
    /** Database Charset to use in creating database tables. */
    define( 'DB_CHARSET', 'utf8' );
    /** The Database Collate type. Don't change this if in doubt. */
    define( 'DB_COLLATE', '' );
    /**
     * Authentication Unique Keys and Salts.
     *
     * Change these to different unique phrases!
     * You can generate these using the
     * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service} You can change these at
     * any point in time to invalidate all existing cookies. This will force all users to have to log in again.
     *
     * @since 2.6.0
     */
    define('AUTH_KEY',         '*EO$ B-Q$ft9%p?t]TLdm-mO{/R+qk|$Z0RQ|s+mB#7[:h7a(&b-K8qrSQ-@qFKE');
    define('SECURE_AUTH_KEY',  '9)N|X|z?5{wOlNDgPXV1t1`@q+d:*-+4e_>%MJWe:Avc`<74Gpiq6xa=N>R;Vc~_');
    define('LOGGED_IN_KEY',    '_YxRo1mjc+uq_-LDCmSA;#J[mt=+=Sf7uctv)~=c8|_txl?L7DY>PDnprq,q:s0D');
    define('NONCE_KEY',        '*f,z30lT0$-+GMK7WU: *{^n<J|{+BqI=Qtbl`{`?OWfCK|]d9<w^b/W8<%kVbCY');
    define('AUTH_SALT',        'w>]r@JRUZ8x-ME$Fezh*4a*85cEZ+C-yk0c*sM}-(De^gJB]ki7+MoDl{q{:]F%l');
    define('SECURE_AUTH_SALT', '%23=iDG|gf]|sNEz~B2Td[IbZbt`fHOK&hb+_pDG@:-+{mR|]@~!=-9)hy00KHBs');
    define('LOGGED_IN_SALT',   'tsG0.CR.4qiBdHKrB.Jp&^E-ww-n7;AcC5^7[71^xVyi^-hw+5.8z#_l0#r+{RAL');
    define('NONCE_SALT',       '`Jf&+F+Zh]n?sy3Km3Q%3r<-JlZSNdO-)tW.U KA6@6zS0b7/,:N2bl$H`7u>IQa');
    /**
     * WordPress Database Table prefix.
     *
     * You can have multiple installations in one database if you give each
     * a unique prefix. Only numbers, letters, and underscores please!
     */
    $table_prefix = 'wp_';
    define( ‘ALLOW_UNFILTERED_UPLOADS’, TRUE );
    define( 'WP_DEBUG', true );
    define( 'WP_DEBUG_DISPLAY', false );
    define( 'WP_DEBUG_LOG', true );
    
    /* Inserted by Local by Flywheel. See: http://codex.wordpress.org/Administration_Over_SSL#Using_a_Reverse_Proxy */
    if ( isset( $_SERVER['HTTP_X_FORWARDED_PROTO'] ) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ) {
        $_SERVER['HTTPS'] = 'on';
    }
    /* That's all, stop editing! Happy blogging. */
    /** Absolute path to the WordPress directory. */
    if ( ! defined( 'ABSPATH' ) ) {
        define( 'ABSPATH', dirname( __FILE__ ) . '/' );
    }
    /** Sets up WordPress vars and included files. */
    require_once ABSPATH . 'wp-settings.php';
