<?php

define( 'INVITATION_CODE_ADDON', '3.0' );

add_action( 'gform_loaded', array( 'Invitation_Code_AddOn_Bootstrap', 'load' ), 5 );

class Invitation_Code_AddOn_Bootstrap {

    public static function load() {

        if ( ! method_exists( 'GFForms', 'include_addon_framework' ) ) {
            return;
        }

        require_once( 'includes/class-invitation-field-addon.php' );

        GFAddOn::register( 'InvitationFieldAddOn' );
    }

}