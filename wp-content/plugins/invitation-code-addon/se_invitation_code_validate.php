<?php 
add_filter( 'gform_field_validation', 'valii', 10, 4 );
function valii( $result, $value, $form, $field ) {

	global $wpdb;
	$table_invitation = $wpdb->prefix . 'invitation_code';

    if ( $field->type == 'invitation_code_btn' ) {
		$sql_getcount = "SELECT * from $table_invitation where invitation_code_text = BINARY '$value'";
		$query_result_count=$wpdb->get_row($sql_getcount,ARRAY_A);
		if(!empty($query_result_count)){
			$used = $query_result_count['invitation_used_count'];
		}else{
			$used = 0;
		}
		
		if($query_result_count['invitation_code_expire'] == '0000-00-00' || is_null($query_result_count['invitation_code_expire']) || $query_result_count['invitation_code_expire'] == '0' ){
			$expireDate = "";
		}else{
			$expireDate = 'AND invitation_code_expire >= CURDATE()';
		}

		$sql_unlimited="SELECT * from $table_invitation where invitation_code_text = BINARY '$value' AND invitation_count = -1 $expireDate";
		$query_result_unlimited=$wpdb->get_row($sql_unlimited,ARRAY_A);
		
		$sql="SELECT * from $table_invitation where invitation_code_text = BINARY '$value' AND invitation_count > ".$used." $expireDate";
		$query_result=$wpdb->get_row($sql,ARRAY_A);
		
		
		if(!empty($query_result_unlimited)){
			$result['is_valid'] = true;
		}else if(!empty($query_result)){
			$result['is_valid'] = true;
		}else{
			$result['is_valid'] = false;
			$result['message']  = empty( $field->errorMessage ) ? __($field->custom_validation_msg, 'gravityforms' ) : $field->errorMessage;
		}
    }
    return $result;
}
add_shortcode("invitation_code_name","invitation_code_name");
function invitation_code_name($atts,$content = null){
	global $wpdb;
	$table_invitation = $wpdb->prefix . 'invitation_code';
	extract(shortcode_atts( array(
		'code' => ''
	), $atts));
	
	$sql_getname = "SELECT * from $table_invitation where invitation_code_text = BINARY '$code'";
	$query_result_name = $wpdb->get_row($sql_getname,ARRAY_A);
	if(!empty($query_result_name)){
		$name = $query_result_name['invitation_code_name'];
		return $name;
	}
}

add_action( 'gform_after_submission', 'se_set_post_content', 10, 2 );
function se_set_post_content( $entry, $form ) {
	global $wpdb;
	$user_id = get_current_user_id();
	$table_invitation = $wpdb->prefix . 'invitation_code';
	$table_used_codes = $wpdb->prefix . 'used_invitation_codes';

	foreach($form['fields'] as $forms){
		if($forms->type == "invitation_code_btn"){

			$fieldValue = $entry[$forms->id];

			$field_cpt_integration = $forms->field_cpt_integration;
			$postCustomFieldName = $forms->postCustomFieldName;
			if($field_cpt_integration)
			{
				//Check post exists
				$post_id = $entry['post_id'];

				if(!empty($post_id)){
					update_post_meta($post_id,$postCustomFieldName,$fieldValue);
				}else{
                    //create a new post
                    // Create post object
                    $my_post = array(
                        'post_title'    =>'untitled',
                        'post_content'  => '',
                        'post_status'   => 'draft',
                        'post_author'   => 1,
                    );

                    // Insert the post into the database
                    $post_id = wp_insert_post( $my_post );
                    update_post_meta($post_id,$postCustomFieldName,$fieldValue);
                }

			}


			$sql_getcount = "SELECT * from $table_invitation where invitation_code_text = BINARY '$fieldValue'";
			$query_result_count=$wpdb->get_row($sql_getcount,ARRAY_A);
			if(!empty($query_result_count)){
				$used = $query_result_count['invitation_used_count'];
			}else{
				$used = 0;
			}
			
			if($query_result_count['invitation_code_expire'] == '0000-00-00' || is_null($query_result_count['invitation_code_expire']) || $query_result_count['invitation_code_expire'] == '0' ){
				$expireDate = "";
			}else{
				$expireDate = 'AND `invitation_code_expire` >= CURDATE()';
			}
			
			$sql_unlimited="SELECT * from $table_invitation where invitation_code_text = BINARY '$fieldValue' AND invitation_count = -1 $expireDate";
			$query_result_unlimited=$wpdb->get_row($sql_unlimited,ARRAY_A);
			
			$sql="SELECT * from $table_invitation where invitation_code_text = BINARY '$fieldValue' AND invitation_count > ".$used." $expireDate";
			$query_result=$wpdb->get_row($sql,ARRAY_A);
			
			if(!empty($query_result_unlimited)){
				// echo "unlimited";
				// die;
				$count = $query_result_unlimited['invitation_used_count'];
				$res = $wpdb->update(
					$table_invitation, 
					array(
						'invitation_used_count' => ++$count // integer (number) 
					), 
					array( 'ID' => $query_result_unlimited['ID'] ), 
					array( '%d' ), 
					array( '%s' ) 
				);
				
				if($res){
					$result['is_valid'] = true;
					$wpdb->insert( 
						$table_used_codes, 
						array( 
							'invitation_code_text' => $query_result_unlimited['invitation_code_text'], 
							'invitation_code_name' => $query_result_unlimited['invitation_code_name'] ,
							'uid' => $user_id,
						)
					);
				}
			}else if(!empty($query_result)){
				// echo "not unlimited";
				// die;
				$count = $query_result['invitation_used_count'];
				$res = $wpdb->update(
					$table_invitation, 
					array(
						'invitation_used_count' => ++$count // integer (number) 
					), 
					array( 'ID' => $query_result['ID'] ), 
					array( '%d' ), 
					array( '%s' ) 
				);
				if($res){
					$result['is_valid'] = true;
					$wpdb->insert( 
						$table_used_codes, 
						array( 
							'invitation_code_text' => $query_result['invitation_code_text'], 
							'invitation_code_name' => $query_result['invitation_code_name'] ,
							'uid' => $user_id,
						)
					);
				}
			}else{
				$result['is_valid'] = false;
				$result['message']  = empty( $field->errorMessage ) ? __( 'Please enter a correct Invitation Code.', 'gravityforms' ) : $field->errorMessage;
			}
		}
	}
}
add_action("gform_user_registered", "se_add_user_codes", 10, 4);
function se_add_user_codes($user_id, $config, $entry, $user_pass) {
	global $wpdb;
	$table_invitation = $wpdb->prefix . 'invitation_code';
	$table_used_codes = $wpdb->prefix . 'used_invitation_codes';
	$form = GFAPI::get_form($entry["form_id"]);
	foreach($form['fields'] as $forms){
		if($forms->type == "invitation_code_btn"){
			$fieldValue = $entry[$forms->id];
			$sql_getcount = "SELECT * from $table_invitation where invitation_code_text = BINARY '$fieldValue'";
			$query_result_count=$wpdb->get_row($sql_getcount,ARRAY_A);
			if(!empty($query_result_count)){
				$used = $query_result_count['invitation_used_count'];
			}else{
				$used = 0;
			}
			
			if($query_result_count['invitation_code_expire'] == '0000-00-00' || is_null($query_result_count['invitation_code_expire']) || $query_result_count['invitation_code_expire'] == '0' ){
				$expireDate = "";
			}else{
				$expireDate = 'AND `invitation_code_expire` >= CURDATE()';
			}
			
			$sql_unlimited="SELECT * from $table_invitation where invitation_code_text = BINARY '$fieldValue' AND invitation_count = -1 $expireDate";
			$query_result_unlimited=$wpdb->get_row($sql_unlimited,ARRAY_A);
			
			
			$sql="SELECT * from $table_invitation where invitation_code_text = BINARY '$fieldValue' AND invitation_count >= ".$used." $expireDate";
			$query_result=$wpdb->get_row($sql,ARRAY_A);
			if(!empty($query_result_unlimited)){
				$wpdb->insert( 
					$table_used_codes, 
					array( 
						'invitation_code_text' => $query_result_unlimited['invitation_code_text'], 
						'invitation_code_name' => $query_result_unlimited['invitation_code_name'] ,
						'uid' => $user_id,
					)
				);
			}else if(!empty($query_result)){
				
				$wpdb->insert( 
						$table_used_codes, 
						array( 
							'invitation_code_text' => $query_result['invitation_code_text'], 
							'invitation_code_name' => $query_result['invitation_code_name'] ,
							'uid' => $user_id,
						)
					);
				
			}
		}
	}	    
}
# Delete user data
function se_delete_user( $user_id ) {
	global $wpdb;
	$table_used_codes = $wpdb->prefix . 'used_invitation_codes';
	$query = "delete from $table_used_codes where uid=".$user_id;
	$wpdb->query($query);        
}
add_action( 'delete_user', 'se_delete_user' );

#Add a custom class to the field li
add_action("gform_field_css_class", "custom_classs", 10, 3);
function custom_classs($classes, $field, $form){
	if( $field["type"] == "invitation_code_btn" ){
		$classes .= " gfield_contains_required";
	}
	return $classes;
}