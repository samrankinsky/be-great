<?php

GFForms::include_addon_framework();

class InvitationFieldAddOn extends GFAddOn {

	protected $_version = INVITATION_CODE_ADDON;
	protected $_min_gravityforms_version = '1.9';
	protected $_slug = 'invitation-code-addon';
	protected $_path = 'invitation-code-addon/includes/invitation-code-addon.php';
	protected $_full_path = __FILE__;
	protected $_title = 'Gravity Forms Invitation Field Add-On';
	protected $_short_title = 'Invitation Field Add-On';

	/**
	 * @var object $_instance If available, contains an instance of this class.
	 */
	private static $_instance = null;

	/**
	 * Returns an instance of this class, and stores it in the $_instance property.
	 *
	 * @return object $_instance An instance of this class.
	 */
	public static function get_instance() {
		if ( self::$_instance == null ) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	/**
	 * Include the field early so it is available when entry exports are being performed.
	 */
	public function pre_init() {
		parent::pre_init();

		if ( $this->is_gravityforms_supported() && class_exists( 'GF_Field' ) ) {
			require_once( 'field-settings.php' );
		}
	}

	public function init_admin() {
		parent::init_admin();

		add_filter( 'gform_tooltips', array( $this, 'tooltips' ) );
		add_action( 'gform_field_appearance_settings', array( $this, 'field_appearance_settings' ), 10, 2 );
		add_action( 'gform_field_standard_settings', array( $this, 'standard_settings' ), 10, 2 );
	}


	// # FIELD SETTINGS -------------------------------------------------------------------------------------------------

	/**
	 * Add the tooltips for the field.
	 *
	 * @param array $tooltips An associative array of tooltips where the key is the tooltip name and the value is the tooltip.
	 *
	 * @return array
	 */
	public function tooltips( $tooltips ) {
		$invitation_code_btn_tooltips = array(
			'tooltip_form_field_custom_msg' => sprintf( '<h6>%s</h6>%s', esc_html__( 'Validation Message', 'invitation-code-addon' ), esc_html__( 'Enter a message users will see if this field has not been completed/validated.', 'invitation-code-addon' ) ),
			'tooltip_form_field_cpt' => sprintf( '<h6>%s</h6>%s', esc_html__( 'Custom Post Type Integration', 'invitation-code-addon' ), esc_html__( 'Check this box to integrate invitation code with custom post type.', 'invitation-code-addon' ) ),
		);

		return array_merge( $tooltips, $invitation_code_btn_tooltips );
	}

	/**
	 * Add the custom setting for the invitation_code_btn field to the Appearance tab.
	 *
	 * @param int $position The position the settings should be located at.
	 * @param int $form_id The ID of the form currently being edited.
	 */
	public function field_appearance_settings( $position, $form_id ) {
		// Add our custom setting just before the 'Custom CSS Class' setting.
		if ( $position == 250 ) {
			?>
			<li class="custom_validation_msg field_setting ic-addon" style="display: list-item; ">
				<label for="custom_validation_msg">Validation Message
					<!-- Tooltip to help users understand what this field does -->
					<?php gform_tooltip( 'tooltip_form_field_custom_msg' ); ?>
				</label>
				<input type="text" id="custom_validation_msg" class="fieldwidth-3" size="35" onkeyup="SetValidationSetting(jQuery(this).val());" onchange="SetValidationSetting(jQuery(this).val());"/>
			</li>

			<?php
		}
	}
	public function standard_settings($position, $form_id){

        // Create settings on position 25 (right after Field Label)

		if($position == '250'){ ?>

			<li class="field_cpt_integration field_setting ic-addon" style="">
				<input type="checkbox" id="field_cpt_integration" class="" size="10" onclick="SetCPTProperty(this.checked);" >
				<label for="field_cpt_integration" class="inline"> CPT Integration
					<!-- Tooltip to help users understand what this field does -->
					<?php gform_tooltip( 'tooltip_form_field_cpt' ); ?>
				</label>
			</li>

			<li class="cpt_field field_setting " style="">
				<label for="field_custom_field_name">
					<?php esc_html_e( 'Custom Field Name', 'gravityforms' ); ?>
					<?php gform_tooltip( 'form_field_custom_field_name' ) ?>
				</label>

				<div style="width:100px; float:left;">
					<input type="radio"  name="field_custom_in" id="field_custom_existing_in" size="10" onClick="ToggleCustomFieldIn()" />
					<label for="field_custom_existing_in" class="inline">
						<?php esc_html_e( 'Existing', 'gravityforms' ); ?>
					</label>
				</div>
				<div style="width:100px; float:left;">
					<input type="radio" name="field_custom_in" id="field_custom_new" size="10" onClick="ToggleCustomFieldIn()" />
					<label for="field_custom_new" class="inline">
						<?php esc_html_e( 'New', 'gravityforms' ); ?>
					</label>
				</div>
				<div class="clear">
					<input type="text" id="field_custom_field_name_text_in" size="35" onkeyup="SetCustomFieldValue(jQuery(this).val());" onchange="SetCustomFieldValue(jQuery(this).val());" />
					<select id="field_custom_field_name_select_in" onchange="SetCustomFieldValue(jQuery(this).val());">
						<option value=""><?php esc_html_e( 'Select an existing custom field', 'gravityforms' ); ?></option>
						<?php
						$custom_field_names = RGFormsModel::get_custom_field_names();
						foreach ( $custom_field_names as $name ) {
							?>
							<option value="<?php echo esc_attr( $name ); ?>"><?php echo esc_html( $name ) ?></option>
							<?php
						}
						?>
					</select>
				</div>
			</li>
			<?php
		}

	}
}