<?php

if ( ! class_exists( 'GFForms' ) ) {
	die();
}

class Invitation_Code_Field extends GF_Field {

	/**
	 * @var string $type The field type.
	 */
	public $type = 'invitation_code_btn';

	/**
	 * Return the field title, for use in the form editor.
	 *
	 * @return string
	 */
	public function get_form_editor_field_title() {
		return esc_attr__( 'Invitation Code', 'invitation_code_btn' );
	}

	/**
	 * Assign the field button to the Advanced Fields group.
	 *
	 * @return array
	 */
	public function get_form_editor_button() {
		return array(
			'group' => 'advanced_fields',
			'text'  => $this->get_form_editor_field_title(),
		);
	}

	/**
	 * The settings which should be available on the field in the form editor.
	 *
	 * @return array
	 */
	function get_form_editor_field_settings() {
		return array(
			'label_setting',
			'description_setting',
			//'rules_setting',
			'placeholder_setting',
			'custom_validation_msg',
			'field_cpt_integration',
			'cpt_field',
			'css_class_setting',
			'size_setting',
			'admin_label_setting',
			'default_value_setting',
			'visibility_setting',
			'conditional_logic_field_setting',
		);
	}

	/**
	 * Enable this field for use with conditional logic.
	 *
	 * @return bool
	 */
	public function is_conditional_logic_supported() {
		return true;
	}

	/**
	 * The scripts to be included in the form editor.
	 *
	 * @return string
	 */
	public function get_form_editor_inline_script_on_page_render() {

		// set the default field label for the simple type field
		$script = sprintf( "function SetDefaultValues_simple(field) {field.label = '%s';}", $this->get_form_editor_field_title() ) . PHP_EOL;

		// initialize the fields custom settings
		$script .= "jQuery(document).bind('gform_load_field_settings', function (event, field, form) {" .
            "console.log(field);".
						"var custom_validation_msg = field.custom_validation_msg == undefined ? '' : field.custom_validation_msg;" .
						"var field_cpt_integration = field.field_cpt_integration;" .
                       	"jQuery('#custom_validation_msg').val(custom_validation_msg);" .
						"jQuery('#field_cpt_integration').attr('checked', field_cpt_integration);" .
						"jQuery('#field_custom_field_name_select_in').val(field.postCustomFieldName).attr('selected', 'selected');".
						"jQuery('#field_custom_field_name_text_in').val(field.postCustomFieldName);".
//                        "var isChecked = jQuery('#field_cpt_integration').is(':checked');
//                        if(isChecked){
//                            jQuery('.cpt_field').show();
//                        }".
//                        "jQuery('#field_cpt_integration').on('click',function(){
//                            jQuery('.cpt_field').toggle();
//                        });
//					".
            "if(field.type !=='invitation_code_btn'){jQuery('ic-addon').hide();}".
            "});" . PHP_EOL;

		// saving the simple setting
		$script .= "function SetValidationSetting(value) {SetFieldProperty('custom_validation_msg', value);}" . PHP_EOL;
		$script .= "function SetCPTProperty(value) {SetFieldProperty('field_cpt_integration', value);}" . PHP_EOL;
		$script .= "function SetCustomFieldValue(value) {SetFieldProperty('postCustomFieldName', value);}" . PHP_EOL;
		$script .= '
		function ToggleCustomFieldIn(isInit){

            var isExisting = jQuery("#field_custom_existing_in").is(":checked");
            show_element = isExisting ? "#field_custom_field_name_select_in" : "#field_custom_field_name_text_in"
            hide_element = isExisting ? "#field_custom_field_name_text_in"  : "#field_custom_field_name_select_in";

            var speed = isInit ? "" : "";

            jQuery(hide_element).hide(speed);
            jQuery(show_element).show(speed);
	    }' . PHP_EOL;


		return $script;
	}

	/**
	 * Define the fields inner markup.
	 *
	 * @param array $form The Form Object currently being processed.
	 * @param string|array $value The field value. From default/dynamic population, $_POST, or a resumed incomplete submission.
	 * @param null|array $entry Null or the Entry Object currently being edited.
	 *
	 * @return string
	 */
	public function get_field_input( $form, $value = '', $entry = null ) {
		$id              = absint( $this->id );
		$form_id         = absint( $form['id'] );
		$is_entry_detail = $this->is_entry_detail();
		$is_form_editor  = $this->is_form_editor();

		// Prepare the value of the input ID attribute.
		$field_id = $is_entry_detail || $is_form_editor || $form_id == 0 ? "input_$id" : 'input_' . $form_id . "_$id";

		$value = esc_attr( $value );

        // Prepare the input classes.
        $size         = $this->size;
        $class_suffix = $is_entry_detail ? '_admin' : '';
        $class        = $size . $class_suffix . ' ';

		// Prepare the other input attributes.
		$tabindex              = $this->get_tabindex();
		$logic_event           = ! $is_form_editor && ! $is_entry_detail ? $this->get_conditional_logic_event( 'keyup' ) : '';
		$placeholder_attribute = $this->get_field_placeholder_attribute();
		$required_attribute    = $this->isRequired ? 'aria-required="true"' : '';
		$invalid_attribute     = $this->failed_validation ? 'aria-invalid="true"' : 'aria-invalid="false"';
		$disabled_text         = $is_form_editor ? 'disabled="disabled"' : '';

		// Prepare the input tag for this field.
		$input = "<input name='input_{$id}' id='{$field_id}' type='text' value='{$value}' class='{$class}' {$tabindex} {$logic_event} {$placeholder_attribute} {$required_attribute} {$invalid_attribute} {$disabled_text}/>";

		return sprintf( "<div class='ginput_container ginput_container_%s'>%s</div>", $this->type, $input );
	}
}

GF_Fields::register( new Invitation_Code_Field() );
