<?php
/*
Plugin Name: Gravity Forms Invitation Codes Add-on
Plugin URI: http://wpmach.com
Description: Gravity Form Invitation Codes Add-on
Version: 3.0
Author: WP MACH
Author URI: http://wpmach.com
*/
require_once plugin_dir_path( __FILE__ ) . 'DataSource.php';
require_once plugin_dir_path( __FILE__ ) . 'invitation-field-intializer.php';
require_once plugin_dir_path( __FILE__ ) . 'se_invitation_code_validate.php';
require_once plugin_dir_path( __FILE__ ) . 'includes/class.merge_tags.php';

function ilc_admin_tabs( $current = 'dashboard' ) {
	$tabs = array( 'dashboard' => 'Dashboard', 'registercode' => 'Register Codes', 'bulkuploadcsv' => 'Bulk Upload CSV', 'addcodesperline' => 'Add Codes per line' );
	$links = array();
	echo '<div id="icon-themes" class="icon32"><br></div>';
	echo '<h2 class="nav-tab-wrapper">';
	foreach( $tabs as $tab => $name ){
		$class = ( $tab == $current ) ? ' nav-tab-active' : '';
		echo "<a class='nav-tab$class' href='?page=se-invitation-codes&tab=$tab'>$name</a>";

	}
	echo '</h2>';
}

function ilc_settings_page($r=array()) {
	?>

	<div class="wrap">
		<?php
		if ( 'true' == esc_attr( isset($_GET['updated'] )) ) echo '<div class="updated" ><p>Settings updated.</p></div>';

		if ( isset ( $_GET['tab'] ) ) ilc_admin_tabs($_GET['tab']); else ilc_admin_tabs('dashboard');
		?>

		<div id="poststuff">
				<?php
				if ( $_GET['page'] == 'se-invitation-codes' ){

					if ( isset ( $_GET['tab'] ) ) $tab = $_GET['tab'];
					else $tab = 'dashboard';

					switch ( $tab ){
						case 'registercode' : ?>
                            <form method="post" action="<?php admin_url( 'admin.php?page=se-invitation-codes' ); ?>">
                                <table class="form-table" name="dataInput" style="width:70%">

                                    <tr>
                                        <td>Invitation Code</td>
                                        <td><input <?php if(@isset($_GET['edit'])){ echo "readonly='readonly'"; } ?> type="text" id="invitation_code_text" name="invitation_code_text" size='23' value="<?php echo @$r[0]['invitation_code_text'];?>"/> </td>
                                    </tr>
                                    <tr>
                                        <td>Invitation Code Name</td>
                                        <td><input type="text" id="invitation_code_name" name="invitation_code_name" size='23' value="<?php echo @$r[0]['invitation_code_name'];?>"/> </td>
                                    </tr>
                                    <tr>
                                        <td>Amount of Times Code Can Be Used<br/><small>(Use -1 for Unlimited)</small></td>
                                        <td><input type="text" id="invitation_count" size='23' name="invitation_count" value="<?php echo @$r[0]['invitation_count'];?>"/> </td>
                                    </tr>
                                    <?php

									if(isset($r[0]['invitation_code_expire']) && @$r[0]['invitation_code_expire'] !== "" && @$r[0]['invitation_code_expire'] != 0  ){
                                        $date = DateTime::createFromFormat("Y-m-d" , $r[0]['invitation_code_expire']);
                                        $invitation_code_expire = $date->format('m-d-Y');
                                    }
                                    ?>
                                    <tr>
                                        <td>Expiration Date<br/><small>(mm-dd-yyyy or mm/dd/yyyy)<br/>If no date is chosen, code will not expire</small></td>
                                        <td><input type="text" class="datepicker" id="invitation_code_expire" size='23' name="invitation_code_expire" value="<?php echo @$invitation_code_expire;?>"/> </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td><input type="submit" name="single_invitation_save" class='button button-primary' name="submit" value="Save" /></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </form>
							<?php
							break;
						case 'bulkuploadcsv' : ?>
                            <form method="post" action="<?php admin_url( 'admin.php?page=se-invitation-codes' ); ?>" name="csv_uploader" enctype="multipart/form-data">
                                <h1>Bulk Upload via CSV file</h1>
                                <p>All CSV files must have a correctly formatted header in the following order: <br/><strong>invitation_code_text</strong> (Invitation Code) <br/><strong>invitation_count</strong> (Amount of Times Code Can Be Used) <br/><strong>invitation_code_name</strong> (Name of Invitation Code)
                                </p>
                                <div style="margin-top:20px;">
                                    <div style="float:left;">
                                        <table class="wp-list-table widefat fixed" cellspacing="0" style="width:80%">
                                            <thead>
                                            <tr>
                                                <th>
                                                    Upload the CSV File
                                                </th>
                                            </tr>
                                            </thead>
                                            <tfoot>
                                            <tr>
                                                <th>
                                                    Upload the CSV File
                                                </th>
                                            </tr>
                                            </tfoot>
                                            <tbody>
                                            <tr>
                                                <td>Upload CSV File</td>
                                                <td>
                                                    <input type="file" name="uploadedfile" id="uploadedfile" />
                                                    <input type="hidden" name="MAX_FILE_SIZE" value="1000000" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Expiration Date<br/><small>(mm-dd-yyyy or mm/dd/yyyy)<br/>If no date is chosen, code will not expire</small></td>
                                                <td><input type="text" class="datepicker" id="invitation_code_expire" size='23' name="invitation_code_expire" value=""/> </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <p class="submit" style="margin-left:10px;">
                                            <input type="submit" name="invitation_code_file" class="button-primary" value="<?php _e('Upload File') ?>" />
                                        </p>
                                    </div>
                                </div>
                            </form>
							<?php
							break;

                        case 'addcodesperline' : ?>
                            <form method="post" action="<?php echo admin_url( 'admin.php?page=se-invitation-codes&tab=addcodesperline' ); ?>" name="codes_per_line">
                                <h1>Add codes per line</h1>

								<table class="" cellspacing="0" style="width:80%">
									<tbody>
									<tr>
										<td>Amount of Times Code Can Be Used<br/><small>(Use -1 for Unlimited)</small></td>
										<td><input type="text" id="invitation_count" size='23' name="invitation_count" value="<?php echo @$r[0]['invitation_count'];?>"/> </td>
									</tr>
									<tr>
										<td>Invitation Codes Expiration Date<br/><small>(mm-dd-yyyy or mm/dd/yyyy)<br/>If no date is chosen, code will not expire</small></td>
										<td><input type="text" class="datepicker" id="invitation_code_expire" size='23' name="invitation_code_expire" value=""/> </td>
									</tr>
									<tr>
										<td>Invitation Codes (Enter 1 code per line)</td>
										<td>
											<textarea  id="invitation_code_per_line" cols="50" rows="10" name="invitation_code_per_line"></textarea>
											<br /><br />
										</td>
									</tr>
									</tbody>
								</table>
                                <div style="margin-top:20px;">
									<input type="submit" name="invitation_code_pls" class="button-primary" value="<?php _e('Save Codes') ?>" />
                                </div>



                            </form>
							<?php
							break;
						case 'dashboard' : ?>
                            <h1>Invitation Code Addon Shortcodes</h1>
                            <small>See documentation for further explanation...</small>
                            <table class="form-table" name="dataInput" style="width:70%">
                                <tr>
                                    <td>To generate a random code when a page is opened:</td>
                                    <td>[invitation_code_generator]</td>
                                </tr>
                                <tr>
                                    <td>To display the name of a code on the form confirmation page:</td>
                                    <td>[invitation_code_name code="{The Label of Invitation Code Field in Your Form Goes Here}"]</td>
                                </tr>
                            </table>
							<?php
							break;
					}
				}
				?>
        </div>

	</div>
	<?php
}

function date_manipulator($date){

    $date = new DateTime($date);
    if (method_exists("DateTime", "add")) {
        $date->add(new DateInterval("P84Y"));
    } else {
        $date->modify("+84 years");
    }

    return $date->format("Y-m-d");
}
//shortcode
add_shortcode('invitation_code_generator','invitation_code_generator');
function invitation_code_generator(){
	global $wpdb;
	$content = "";
	$gen_inv_code = couponCodeGenerator();

	$table_invitation = $wpdb->prefix . 'invitation_code';
	$sqlCheck="SELECT * from $table_invitation where invitation_code_text = BINARY '".$gen_inv_code."'";
		$result=$wpdb->get_row($sqlCheck,ARRAY_A);
		if(!$result){
			$wpdb->insert( 
				$table_invitation, 
				array( 
					'invitation_code_text' => $gen_inv_code, 
					'invitation_count' => 1,
					'invitation_code_expire' => '0',
					'invitation_used_count' => 0, 
					'invitation_code_name' => 'Generated Code' 
				)
			);
			$content .= '<p class="gen_inv_code">Code : '.$gen_inv_code.' </p>';
		}
	return $content;
}
function couponCodeGenerator($length = 6, $pre = '', $pos = ''){
	$key = '';
	list($usec, $sec) = explode(' ', microtime());
	mt_srand((float) $sec + ((float) $usec * 100000));
	$inputs = array_merge(range('z','a'),range(0,9),range('A','Z'));
	for($i=0; $i<$length; $i++){
		$key .= $inputs{mt_rand(0,61)};
	}
	return $pre . strtoupper($key) . $pos;
}
function invitation_js(){
	wp_enqueue_style('jquery-style', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');
	wp_enqueue_script('jquery-ui-datepicker');
	wp_enqueue_script( 'invitation-myscript', plugin_dir_url(__FILE__) . "js/script.js" );
}
add_action( "init", 'invitation_js' );

add_action( "init", 'on_form_activate' );
register_activation_hook( __FILE__, 'on_form_activate' );
add_action( 'wpmu_new_blog', 'on_form_activate' );
#Create table on plugin activation
function on_form_activate( $network_wide ) {
    global $wpdb;

    if ( is_multisite()) {
        // store the current blog id
        $current_blog = $wpdb->blogid;

        // Get all blogs in the network and activate plugin on each one
        $blog_ids = $wpdb->get_col( "SELECT blog_id FROM $wpdb->blogs" );
        foreach ( $blog_ids as $blog_id ) {
            switch_to_blog( $blog_id );
            create_form_table();
            restore_current_blog();
        }
    } else {
        create_form_table();
    }
}
function create_form_table() {
    global $wpdb;
	$table_create_old = 'invitation_code';
	$table_used_codes_old = 'used_invitation_codes';
	$table_create = $wpdb->prefix . 'invitation_code';
	$table_used_codes = $wpdb->prefix . 'used_invitation_codes';
	
	if ($wpdb->get_var("SHOW TABLES LIKE '" . $table_create_old . "'") == $table_create_old) {
		$sql_table_rename = "RENAME TABLE `$table_create_old` TO `$table_create`";
		$wpdb->query($sql_table_rename);
	}
	if ($wpdb->get_var("SHOW TABLES LIKE '" . $table_used_codes_old . "'") == $table_used_codes_old) {
		$sql_table_rename = "RENAME TABLE `$table_used_codes_old` TO `$table_used_codes`";
		$wpdb->query($sql_table_rename);
	}
    if ($wpdb->get_var("SHOW TABLES LIKE '" . $table_create . "'") != $table_create) {
        $sql_table_create = "CREATE TABLE IF NOT EXISTS `$table_create` (
          `ID` INT(11) NOT NULL AUTO_INCREMENT,
		  `invitation_code_text` VARCHAR(30) NOT NULL,
		  `invitation_count` INT(20) NOT NULL,
		  `invitation_code_expire` VARCHAR(30) NOT NULL,
		  `invitation_used_count` INT(20) NOT NULL,
		  `invitation_code_name` VARCHAR(50) NULL,
		   PRIMARY KEY (`ID`)
		)";									
		$wpdb->query($sql_table_create);
	}

	 $query = "SELECT COLUMN_NAME
				FROM INFORMATION_SCHEMA.COLUMNS
				WHERE table_name = '{$table_create}'
				AND table_schema = '" .DB_NAME ."'
				AND column_name = 'invitation_code_expire'";

	if($wpdb->query($query) <=0){

		$wpdb->query("ALTER TABLE $table_create ADD invitation_code_expire VARCHAR(30) NOT NULL AFTER invitation_count");

	}

	if ($wpdb->get_var("SHOW TABLES LIKE '" . $table_used_codes . "'") != $table_used_codes) {
        $sql_table_uc = "CREATE TABLE IF NOT EXISTS `$table_used_codes` (
          `ID` INT(11) NOT NULL AUTO_INCREMENT,
		  `invitation_code_text` VARCHAR(30) NOT NULL,
		  `invitation_code_name` VARCHAR(50) NULL,
		  `uid` INT(20) NOT NULL,
		   PRIMARY KEY (`ID`)
		)";									
		$wpdb->query($sql_table_uc);
	}
}
#Navigation Menu
add_filter("gform_addon_navigation","se_invitation_menu");
function se_invitation_menu($menu_items){
	$menu_items[] = array("name" => "se-invitation-codes", "label" => "Invitation Codes", "callback" => "se_invitation_settings", "permission" => "gravityforms_invitation_codes");
	$menu_items[] = array("name" => "se-used-invitation-codes", "label" => "Used Codes", "callback" => "se_used_invitation_settings", "permission" => "gravityforms_invitation_codes");
	return $menu_items;
}
#Call Back function for Navigation Menu
function se_invitation_settings(){

	$resultSet = array();
	?>

	<div class="wrap">
		<div class="widefat">
            <h1>Invitation Codes</h1>
			<?php
				global $wpdb;
				$table_invitation = $wpdb->prefix . 'invitation_code';

				if(isset($_GET['del'])){
					$sql="delete from $table_invitation where ID=".$_GET['del'];
					$wpdb->query($sql);
				}
				if(isset($_GET['edit'])){
					$sql="select * from $table_invitation where ID=".$_REQUEST['edit'];
					$resultSet=$wpdb->get_results($sql,ARRAY_A);
				}
				if(isset($_REQUEST['deleteInvitationCodes'])){
					if(isset($_POST['checkBoxInvitation'])) {
						$arrInvitation = implode(",", $_POST['checkBoxInvitation']);

						$del2 = $wpdb->query(
							"DELETE FROM $table_invitation
							 WHERE ID in ($arrInvitation)"
						);

						if($del2){
							echo '<div id="message" class="updated fade" style="margin:15px 0px 15px"><p><strong>';
							echo 'Selected Codes Deleted Successfully!';
							echo '</strong></p></div>';
						}
					}else{
						echo '<div id="message" class="error fade" style="margin:15px 0px 15px"><p><strong>';
						echo 'No code was selected!';
						echo '</strong></p></div>';
					}
				}
			?>
			<script type='text/javascript'>
				jQuery(function($){
					$('.select_all_codes').change(function() {
						var checkboxes = $(this).closest('form').find(':checkbox').not($(this));
						if($(this).prop('checked')) {
						  checkboxes.prop('checked', true);
						} else {
						  checkboxes.prop('checked', false);
						}
					});
				});
				function Deleteqry(id){
					if(confirm("Are you sure you want to delete this record")==true)
						window.location="<?php echo admin_url();?>admin.php?page=se-invitation-codes&del="+id;
					return false;
				}
				function Editqry(id){
					window.location="<?php echo admin_url();?>admin.php?page=se-invitation-codes&tab=registercode&edit="+id;
				}
			</script>
			<?php
			if(@isset($_POST['single_invitation_save'])){
				if(@isset($_POST['invitation_code_text']) && $_POST['invitation_code_text']!= "" && @isset($_POST['invitation_count']) && $_POST['invitation_count'] != "") {

                    $invitation_code_expire = $_POST['invitation_code_expire'];

                    if(isset($invitation_code_expire) && $invitation_code_expire != ""){
                        $date = DateTime::createFromFormat("m-d-Y" , $invitation_code_expire);
                        if($date){
                            $invitation_code_expire = $date->format('Y-m-d');
                        }else{
                            $date = DateTime::createFromFormat("Y-m-d" , $invitation_code_expire);
                            $invitation_code_expire = $date->format('Y-m-d');
                        }
                    }

					if(empty($invitation_code_expire)){
						$invitation_code_expire = '0';
					}

                    if(@isset($_GET['edit'])){

						$wpdb->update(
							$table_invitation,
							array(
								'invitation_count' => $_POST['invitation_count'],	// integer (number)
								'invitation_code_expire' => $invitation_code_expire,
								'invitation_code_name' => $_POST['invitation_code_name']
							),
							array( 'ID' => $_GET['edit'] ),
							array(
								'%d','%s','%s'	// value2
							),
							array( '%d','%s','%s' )
						);
						$sql="select * from $table_invitation where ID=".$_REQUEST['edit'];
						$resultSet=$wpdb->get_results($sql,ARRAY_A);
						echo '<div class="updated"><p>Successfully Updated!</p></div>';
					}else{
						$sqlCheck="SELECT * from $table_invitation where invitation_code_text = BINARY '".$_POST['invitation_code_text']."'";
						$result=$wpdb->get_row($sqlCheck,ARRAY_A);
						if(!$result){
							$wpdb->insert(
								$table_invitation,
								array(
									'invitation_code_text' => $_POST['invitation_code_text'],
									'invitation_count' => $_POST['invitation_count'],
									'invitation_code_expire' => $invitation_code_expire,
									'invitation_used_count' => 0,
									'invitation_code_name' => $_POST['invitation_code_name']
								)
							);
							echo '<div class="updated"><p>Successfully Added!</p></div>';
						}else{
							echo '<div class="error"><p>This Code Already Exists!</p></div>';
						}
					}
				}else{
					echo '<div class="error"><p>Please fill all fields!</p></div>';
				}
			}

			/*File Uploading */

			if(isset($_POST['invitation_code_file'])){

                $invitation_code_expire = $_POST['invitation_code_expire'];

                if(isset($invitation_code_expire) && $invitation_code_expire != ""){
                    $date = DateTime::createFromFormat("m-d-Y" , $invitation_code_expire);
                    if($date){
                        $invitation_code_expire = $date->format('Y-m-d');
                    }else{
                        $date = DateTime::createFromFormat("Y-m-d" , $invitation_code_expire);
                        $invitation_code_expire = $date->format('Y-m-d');
                    }
                }

                if(empty($invitation_code_expire)){
                    $invitation_code_expire = '0';
                }

                if(!empty($_FILES['uploadedfile']['name'])){
					$target_path = ABSPATH.'wp-content/uploads/temp/';
					if (!is_dir($target_path)){
						mkdir($target_path, 0777);
					}
					$target_path = $target_path . basename( $_FILES['uploadedfile']['name']);
					if(move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $target_path)) {
						importCodes($_FILES['uploadedfile']['name'],$invitation_code_expire);
					}else{
						echo "<div class='error fade' id='error'><strong>There was an error uploading the file, please try again!</strong></div>";
					}
				}else{
					echo '<div class="error" style="padding: 10px;">Please upload the valid csv file.</div>';
				}
			}

			/* Add codes per line*/

			if(isset($_POST['invitation_code_pls'])){

				if(!empty($_POST['invitation_code_per_line'])){

					//split the codes based on new line.
					$icodes = explode(PHP_EOL, $_POST['invitation_code_per_line']);

					if(!empty( $icodes ) ){
						foreach($icodes as $key => $code_data){
                            $invitation_code_text = $code_data;
                            $invitation_count = $_POST['invitation_count'];
                            $invitation_code_name = $code_data;

                            $invitation_code_expire = $_POST['invitation_code_expire'];

                            if(isset($invitation_code_expire) && $invitation_code_expire != ""){
                                $date = DateTime::createFromFormat("m-d-Y" , $invitation_code_expire);
                                if($date){
                                    $invitation_code_expire = $date->format('Y-m-d');
                                }else{
                                    $date = DateTime::createFromFormat("Y-m-d" , $invitation_code_expire);
                                    $invitation_code_expire = $date->format('Y-m-d');
                                }
                            }

                            if(empty($invitation_code_expire)){
                                $invitation_code_expire = '0';
                            }

                            // Do insertion or update
                            if(!empty($invitation_code_text)){
                                $sqlCheck="SELECT * from $table_invitation where invitation_code_text = BINARY '".$invitation_code_text."'";
                                $result=$wpdb->get_row($sqlCheck,ARRAY_A);

                                if(!$result){
                                    $wpdb->insert(
                                        $table_invitation,
                                        array(
                                            'invitation_code_text' => $invitation_code_text,
                                            'invitation_count' => $invitation_count,
                                            'invitation_code_name' => $invitation_code_name,
                                            'invitation_used_count' => 0,
                                            'invitation_code_expire' => $invitation_code_expire,
                                        )
                                    );


                                }else{
                                    // Update the old code

                                    $wpdb->update(
                                        $table_invitation,
                                        array(
                                            'invitation_count' => $invitation_count,	// integer (number)
                                            'invitation_code_name' => $invitation_code_name,
                                            'invitation_code_expire' => $invitation_code_expire
                                        ),
                                        array( 'ID' => $result['ID']),
                                        array(
                                            '%d','%s','%s'	// value2
                                        ),
                                        array( '%d','%s','%s' )
                                    );
                                }
                            }

						}
					}
				}else{
					echo '<div class="error" style="padding: 10px;">Please enter the coupon codes.</div>';
				}
			}
			?>
		</div>
	</div>
	<?php
		$pagenum = isset( $_GET['pagenum'] ) ? absint( $_GET['pagenum'] ) : 1;
		$limit = 50;
		$offset = ( $pagenum - 1 ) * $limit;
		$search_code = "";
		if(isset($_GET['s'])){
			$search_code = $_GET['s'];
			$sql="select * from $table_invitation where invitation_code_text like '%$search_code%' or invitation_code_name like '%$search_code%' LIMIT $offset, $limit";
			$total=$wpdb->get_var("select count(*) from $table_invitation where invitation_code_text like '%$search_code%' or invitation_code_name like '%$search_code%'");
		}else{
			$sql="select * from $table_invitation LIMIT $offset, $limit";
			$total=$wpdb->get_var("select count(*) from $table_invitation");
		}
		$result=$wpdb->get_results($sql,ARRAY_A);
		$rows = count($result);

		ilc_settings_page($resultSet);

	?>
	<h2>Invitation Codes Listing</h2>

	<form action="<?php menu_page_url( 'se-invitation-codes' ); ?>" method="GET" >
		<?php if($search_code != ""){ ?>
			<span class="subtitle" >Search results for '<strong><?php echo $search_code; ?></strong>'</span>
		<?php } ?>
		<p class="search-box" style="margin: 0px 18px 10px 0px;">
			<label for="post-search-input" class="screen-reader-text">Search Codes:</label>
			<input type="search" value="<?php echo $search_code; ?>" name="s" >
			<input type="hidden" value="se-invitation-codes" name="page" >
			<button type="submit" class="button" name="search_code_submit" id="search_code_submit">Search Codes</button>
		</p>
	</form>

	<form action="<?php menu_page_url( 'se-invitation-codes' ); ?>" method="post" >	
		<table class="widefat" id="mytable" style="width:98.5%;table-layout:auto;">
			<thead>
				<tr>
					<th><input type="checkbox" name="select_all_codes" class="select_all_codes" value="" /></th>
					<th id="invitation_code_text" class="manage-column"   scope="col">Invitation Code</th>
					<th id="invitation_code_text" class="manage-column"   scope="col">Invitation Code Name</th>
					<th id="invitation_code_expire" class="manage-column"   scope="col">Invitation Code Expire Date</th>
					<th id="invitation_count_issued" class="manage-column column-title"  scope="col">Invitation Codes Issued</th>
					<th id="invitation_count" class="manage-column column-title"  scope="col">Amount of Times Code Has Been Used</th>
					<th id="message" class="manage-column"  scope="col">Actions</th>
				</tr>
			</thead>
				<tbody class="list:user user-list">
<?php
				if(!empty($rows)){
					for($i=0; $i<$rows; $i++){
						echo "<tr  class='author-self status-inherit gf-locking alternate' valign='top' data-id='".$i."'>";
						echo "<td><input type='checkbox' name='checkBoxInvitation[]' value='".$result[$i]['ID']."' /></td>";
						echo "<td class='author-date'>".$result[$i]['invitation_code_text']."</td>";
						echo "<td class='author-date'>".$result[$i]['invitation_code_name']."</td>";
						if(is_null($result[$i]['invitation_code_expire']) || $result[$i]['invitation_code_expire']=='0'){
							echo "<td class='author-date'>Life Time</td>";
						}else{
							echo "<td class='author-date'>".$result[$i]['invitation_code_expire']."</td>";
						}
						echo "<td class='author-date'>".$result[$i]['invitation_count']."</td>";
						echo "<td class='author-date'>".$result[$i]['invitation_used_count']."</td>";
						echo "<td class='author-date manage-column' colspan='2' style='width:13%;'><span style='float:right;'><form id='form".$i."' action='".admin_url( 'admin.php?page=se-invitation-codes&tab=registercode' )."' method='get'><input type='button' name='Delete' value='Delete' id='delete' class='button button-primary' onclick='Deleteqry(".$result[$i]['ID'].")'/></form></span>";
						echo " <span style=''><form id='formm".$i."' action=". admin_url( 'admin.php?page=se-invitation-codes' ) ." method='get'><input type='hidden' name='tab' value='registercode'><input type='button' name='edit' value='Edit' id='edit' class='button button-primary' onclick='Editqry(".$result[$i]['ID'].")' /></form> </span></td>";
						echo "</tr>";
					}
				}else{
					echo "<tr  class='author-self status-inherit gf-locking alternate' valign='top'>";
					echo "<td colspan='7'><strong>No Codes found!</strong></td>";
					echo "</tr>";
				}	
?>
				</tbody>
				<tfoot>
					<tr>
						<th><input type="checkbox" name="select_all_codes" class="select_all_codes" value="" /></th>
						<th id="invitation_code_text" class="manage-column"   scope="col">Invitation Code</th>
						<th id="invitation_code_text" class="manage-column"   scope="col">Invitation Code Name</th>
						<th id="invitation_code_expire" class="manage-column"   scope="col">Invitation Code Expire Date</th>
						<th id="invitation_count" class="manage-column column-title"  scope="col">Invitation Codes Issued</th>
						<th id="invitation_count" class="manage-column column-title"  scope="col">Amount of Times Code Has Been Used</th>
						<th id="message" class="manage-column"  scope="col">Actions</th>
					</tr>
				</tfoot>
		</table>
		<?php
			$num_of_pages = ceil( $total / $limit );
			$page_links = paginate_links( array(
				'base' => add_query_arg( 'pagenum', '%#%' ),
				'format' => '',
				'prev_text' => __( '&laquo;', 'aag' ),
				'next_text' => __( '&raquo;', 'aag' ),
				'total' => $num_of_pages,
				'current' => $pagenum
			) );
			if ( $page_links ) {
				echo '<div class="tablenav" style="width:98.5%;"><div class="tablenav-pages">' . $page_links . '</div></div>';
			}
		?>
		<input style="margin-top: 10px;" type="submit" name="deleteInvitationCodes" class="button-primary" value="Delete Selected Codes" onClick="return confirm('Are you sure you want to delete selected records?')" />
	</form>	
<?php
}

function importCodes($filename,$expire) {
	global $wpdb;
	$table_invitation = $wpdb->prefix . 'invitation_code';
	set_time_limit ( 0 );//run it unlimited
	$path = ABSPATH.'wp-content/uploads/temp/'; 
	$csv   = new File_CSV_DataSource;
	$files = array($filename);
	
	foreach($files as $file) {
		//load csv file
		$csv->load($path.$file);
		$rows = $csv->getRows();
		$rows_count = count($rows);
		$i=0;
		
		foreach( $rows as $data) {
			$invitation_code_text 	= $data[0];	
			$invitation_count 		= $data[1];	
			$invitation_code_name 	= $data[2];
			if(!empty($invitation_code_text)){
				$sqlCheck="SELECT * from $table_invitation where invitation_code_text = BINARY '".$invitation_code_text."'";
				$result=$wpdb->get_row($sqlCheck,ARRAY_A);
				
				if(!$result){
					$wpdb->insert( 
						$table_invitation, 
						array( 
							'invitation_code_text' => $invitation_code_text, 
							'invitation_count' => $invitation_count, 
							'invitation_code_name' => $invitation_code_name,
							'invitation_used_count' => 0,
							'invitation_code_expire' => $expire,
						)
					);
					
					
				}else{
					// Update the old code
					
					$wpdb->update( 
						$table_invitation, 
						array( 
							'invitation_count' => $invitation_count,	// integer (number)
							'invitation_code_name' => $invitation_code_name,
							'invitation_code_expire' => $expire
						), 
						array( 'ID' => $result['ID']), 
						array( 
							'%d','%s','%s'	// value2
						), 
						array( '%d','%s','%s' ) 
					);
				}
				$i++;
				
				if($i>=$rows_count){
					echo '<div class="updated"><p>CSV file uploaded successfully.</p></div>';
				}
			}	
		}
		
	}
}

function se_used_invitation_settings(){

	global $wpdb;
	$table_used_codes = $wpdb->prefix . 'used_invitation_codes';

	$pagenum = isset( $_GET['pagenum'] ) ? absint( $_GET['pagenum'] ) : 1;
	$limit = 50;
	$offset = ( $pagenum - 1 ) * $limit;
	
	$search_code = "";
	if(isset($_GET['s'])){
		$search_code = $_GET['s'];
		$sql="select * from $table_used_codes where invitation_code_text like '%$search_code%' or invitation_code_name like '%$search_code%' LIMIT $offset, $limit";
		$total=$wpdb->get_var("select count(*) from $table_used_codes where invitation_code_text like '%$search_code%' or invitation_code_name like '%$search_code%'");
	}else{
		$sql="select * from $table_used_codes LIMIT $offset, $limit";
		$total=$wpdb->get_var("select count(*) from $table_used_codes");
	}
	$result=$wpdb->get_results($sql,ARRAY_A);
	$rows = count($result);
	
?>
	<h2>List of Used Codes with Registered Users</h2>
	
	<form action="<?php menu_page_url( 'se-used-invitation-codes' ); ?>" method="GET" >
		<?php if($search_code != ""){ ?>
			<span class="subtitle" >Search results for '<strong><?php echo $search_code; ?></strong>'</span>
		<?php } ?>
		<p class="search-box" style="margin: 0px 18px 10px 0px;">
			<label for="post-search-input" class="screen-reader-text">Search Codes:</label>
			<input type="search" value="<?php echo $search_code; ?>" name="s" >
			<input type="hidden" value="se-used-invitation-codes" name="page" >
			<button type="submit" class="button" name="search_code_submit" id="search_code_submit">Search Codes</button>
		</p>
	</form>	
	
	<table class="widefat" id="mytable" style="width:98.5%;table-layout:auto;">
		<thead>
			<tr>
				<th id="name" class="manage-column" scope="col">Name</th>
				<th id="user_name" class="manage-column" scope="col">Username</th>
				<th id="email" class="manage-column" scope="col">Email</th>
									<th id="invitation_code_text" class="manage-column"   scope="col">Invitation Code Name</th>
				<th id="invitation_code_text" class="manage-column"   scope="col">Invitation Code</th>
				
			</tr>
		</thead>
		<tbody class="list:user user-list">
		<?php
			if(!empty($rows)){	
				for($i=0; $i<$rows; $i++){
					echo "<tr  class='author-self status-inherit gf-locking alternate' valign='top' data-id='".$i."'>";
					$uid = $result[$i]['uid'];
					$author_obj = get_user_by('id', $uid);
                    if(!empty($author_obj)) {
                        echo "<td class='author-date'><a href='" . admin_url('user-edit.php?user_id=' . $uid) . "'>" . $author_obj->first_name . ' ' . $author_obj->last_name . "</a></td>";
                        echo "<td class='author-date'>" . $author_obj->user_login . "</td>";
                        echo "<td class='author-date'>" . $author_obj->user_email . "</td>";
                        echo "<td class='author-date'>" . $result[$i]['invitation_code_name'] . "</td>";
                        echo "<td class='author-date'>" . $result[$i]['invitation_code_text'] . "</td>";
                    }
					echo "</tr>";
				}
			}	
		?>
		</tbody>
		<tfoot>
			<tr>
			<th id="name" class="manage-column" scope="col">Name</th>
			<th id="user_name" class="manage-column" scope="col">Username</th>
			<th id="email" class="manage-column" scope="col">Email</th>
							<th id="invitation_code_text" class="manage-column"   scope="col">Invitation Code Name</th>
			<th id="invitation_code_text" class="manage-column"   scope="col">Invitation Code</th>
		</tr>
		</tfoot>
	</table>	
	<?php
	$num_of_pages = ceil( $total / $limit );
	$page_links = paginate_links( array(
		'base' => add_query_arg( 'pagenum', '%#%' ),
		'format' => '',
		'prev_text' => __( '&laquo;', 'aag' ),
		'next_text' => __( '&raquo;', 'aag' ),
		'total' => $num_of_pages,
		'current' => $pagenum
	) );
	if ( $page_links ) {
		echo '<div class="tablenav" style="width:98.5%;"><div class="tablenav-pages">' . $page_links . '</div></div>';
	}
}

function gw_post_content_merge_tags( $args = array() ) {
	return GW_Post_Content_Merge_Tags::get_instance( $args );
}
gw_post_content_merge_tags( array(
		'encrypt_eid'     => true
) );

add_action('admin_head','invitation_code_styles');
function invitation_code_styles(){ ?>
	<style>
	.manage-column.column-cb.check-column {
	    width: 10%;
	}
	</style>
	<?php
}

if(!function_exists('print_rr')){
	function print_rr($arr){
		echo "<pre>";
		print_r($arr);
		echo "</pre>";
	}
}
?>