<?php
/**
 * The main template file.
 *
 * @package Titan_Theme
 * @since   1.0.0
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
		<link rel="manifest" href="/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">
		<?php wp_head(); ?>

		<?php if ( !defined( 'WP_ENV') || WP_ENV == 'live' ){  ?>
		<script type="text/javascript">
			function _gaLt(event){
				var el = event.srcElement || event.target;

				/* Loop up the DOM tree through parent elements if clicked element is not a link (eg: an image inside a link) */
				while(el && (typeof el.tagName == 'undefined' || el.tagName.toLowerCase() != 'a' || !el.href)){
					el = el.parentNode;
				}

                if(jQuery(el).attr('class') == 'magnific-video'){
                	// console.log('leave');
                    return false;
                }else{
                	// console.log('continue');
                }

				if(el && el.href){
					if(el.href.indexOf(location.host) == -1){ /* external link */
						/* HitCallback function to either open link in either same or new window */
						var hitBack = function(link, target){
							target ? window.open(link, target) : window.location.href = link;
						};
						/* link */
						var link = el.href;
						/* Is target set and not _(self|parent|top)? */
						var target = (el.target && !el.target.match(/^_(self|parent|top)$/i)) ? el.target : false;
						/* send event with callback */
						ga(
							"send", "event", "Outgoing Links", link,
							document.location.pathname + document.location.search,
							{"hitCallback": hitBack(link, target)}
						);

						/* Prevent standard click */
						event.preventDefault ? event.preventDefault() : event.returnValue = !1;
					}

				}
			}

			/* Attach the event to all clicks in the document after page has loaded */
			var w = window;
			w.addEventListener ? w.addEventListener("load",function(){document.body.addEventListener("click",_gaLt,!1)},!1)
				: w.attachEvent && w.attachEvent("onload",function(){document.body.attachEvent("onclick",_gaLt)});
		</script>
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-111954612-1"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());
		 
		  gtag('config', 'UA-111954612-1');
		</script>
		<?php } ?>

	</head>
	<body <?php body_class(); ?>>
		<?php
		do_action( 'get_header' );
			get_template_part( 'templates/header' );
				include titan_get_main_template();
			get_template_part( 'templates/footer' );
		do_action( 'get_footer' );
		wp_footer();
		?>

        <?php /*<!-- Google Code for Remarketing tag -->
        <!-- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. For instructions on adding this tag and more information on the above requirements, read the setup guide: google.com/ads/remarketingsetup -->
        <script type="text/javascript">
            var google_conversion_id = 975915548;
            var google_conversion_label = "VwEfCPzW5lEQnJSt0QM";
            var google_custom_params = window.google_tag_params;
            var google_remarketing_only = true;
        </script>
        <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>

        <noscript>
            <div style="display:inline;">
                <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/975915548/?value=0&amp;label=VwEfCPzW5lEQnJSt0QM&amp;guid=ON&amp;script=0"/>
            </div>
        </noscript>
        */ ?>

        <!-- Google Code for Link Tracking tag -->
        <script type="text/javascript">
            function recordOutboundLink ( link, category, action ) {
                try {
                    var pageTracker = _gat._getTracker("UA-21809781-1");
                    pageTracker._trackEvent(category, action);
                    setTimeout('document.location = "' + link.href + '"', 100)
                } catch ( err ) {

                }
            }
        </script>

	</body>
</html>