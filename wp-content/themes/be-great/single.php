<?php
/**
 * The single template file.
 *
 * This template is used for all single posts or custom post type.
 *
 * @package Titan_Theme
 * @since   1.0.0
 */

get_template_part( 'templates/single', get_post_type() );
