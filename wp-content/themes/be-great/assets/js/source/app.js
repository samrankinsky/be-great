function smoothScroll(target) {

	if (target.length) {
		jQuery('html, body').animate({
			scrollTop: target.offset().top - 160
		}, 1000);

		return false;
	}
}

(function ( $ ) {
	$( document ).ready( function () {

        /* Disable Right Click */
        // $(document).bind('contextmenu', function(e) {return false;});
        // $('img').bind('dragstart', function(event) { event.preventDefault(); });

		// Changing the defaults
        window.sr = ScrollReveal({

            distance: '60px',
            duration: 1000,
            opacity: 0.35,
			//reset: true,
            scale: 1

        });

		// Content 1 column with background
        sr.reveal('.content-1col-bg .component-bg');
        sr.reveal('.content-1col-bg .component-bg .content-wrapper');

        // 2 Column w Image
        sr.reveal('.content-2col-image .image-col');

        // News / Blog
        sr.reveal('.news-grid .flex-col', {
            distance: '30px',
            duration: 750,
            opacity: 0
        }, 250);

        // CTA
        sr.reveal('.cta');
        sr.reveal('.cta .cta-border');
        sr.reveal('.cta .subheading');
        sr.reveal('.cta .cta-border p', {
                duration: 1500
            }
        );

        // Hero BG
        sr.reveal('.hero-image-bg', {
                distance: '-20px',
                duration: 1500,
                opacity: 0
            }
        );
        sr.reveal('.hero-image-caption h1', {
                delay: 1500,
                distance: '20px',
                duration: 1000,
                opacity: 0
            }
        );
        sr.reveal('.hero-image-caption .hero-image-text', {
                delay: 2000,
                distance: '-20px',
                duration: 1000,
                opacity: 0
            }
        );

        // Header navigation
        sr.reveal('body.home #menu-main-menu > li', {
            delay: 2500,
            distance: '-10px',
            duration: 600,
            opacity: 0
        }, 350);

        // Client Logo
        sr.reveal('.logo-grid');
        sr.reveal('.logo-grid .logo-item', {
            delay: 400,
            distance: '20px',
            duration: 800,
            opacity: 0
        }, 400);
        sr.reveal('.logo-grid .btn', {
            distance: '-20px',
            delay: 800,
            opacity: 0
        });

        sr.reveal('.testimonial-grid .testimonial-wrapper', {
            distance: '70px',
            duration: 900,
            opacity: 0
        }, 400);



        // Page Intro
        /*sr.reveal('.page-intro', {
         distance: '30px',
         duration: 1200
         });
         sr.reveal('.page-intro .heading', {
         distance: '20px',
         duration: 1200
         });*/

        // Content Subheading
        /*sr.reveal('section.content .subheading:first-of-type', {
            //delay: 1000
        });*/

        // Featured News
        /*sr.reveal('.blog .intro-text', {
         distance: '20px',
         duration: 1300,
         opacity: 0
         }, 350);*/

        // Recent Posts
        /*sr.reveal('.recent-posts .recent-post', {
         distance: '60px',
         duration: 1300,
         opacity: 0
         }, 500);*/

        // Content component
        /*sr.reveal('section.content');
         sr.reveal('section.content .content-item', {
         distance: '60px',
         duration: 750,
         opacity: 0
         }, 250);*/

        // Testimonial
        /*sr.reveal('.testimonial');
        sr.reveal('.testimonial blockquote');
        sr.reveal('.testimonial footer', {
                duration: 1800
            }
        );*/

        // Featured Properties
        /*sr.reveal('.property-featured .subheading');
         sr.reveal('.property-featured .btn', {
         //delay: 500
         });
         sr.reveal('.property-featured .property', {
         distance: '50px',
         duration: 1200,
         opacity: 0
         }, 350);*/


		// CSS Tricks - Smooth Scrolling
		$('a[href*="#"]:not([href="#"])').click(function() {
			
			if( !$(this).hasClass('no-scroll')) {

				if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
					var target = $(this.hash);
					target = target.length ? target : $('[name=' + this.hash.slice(1) +']');

					return smoothScroll(target);
				}

			}
		});

		if(window.location.hash) {

			var hash = window.location.hash;
			smoothScroll($(hash));

		}

	} );
})( jQuery );