function smoothScroll(target) {

	if (target.length) {
		jQuery('html, body').animate({
			scrollTop: target.offset().top - 160
		}, 1000);

		return false;
	}
}

(function ( $ ) {
	$( document ).ready( function () {

        /* Disable Right Click */
        // $(document).bind('contextmenu', function(e) {return false;});
        // $('img').bind('dragstart', function(event) { event.preventDefault(); });

		// Changing the defaults
        window.sr = ScrollReveal({

            distance: '60px',
            duration: 1000,
            opacity: 0.35,
			//reset: true,
            scale: 1

        });

		// Content 1 column with background
        sr.reveal('.content-1col-bg .component-bg');
        sr.reveal('.content-1col-bg .component-bg .content-wrapper');

        // 2 Column w Image
        sr.reveal('.content-2col-image .image-col');

        // News / Blog
        sr.reveal('.news-grid .flex-col', {
            distance: '30px',
            duration: 750,
            opacity: 0
        }, 250);

        // CTA
        sr.reveal('.cta');
        sr.reveal('.cta .cta-border');
        sr.reveal('.cta .subheading');
        sr.reveal('.cta .cta-border p', {
                duration: 1500
            }
        );

        // Hero BG
        sr.reveal('.hero-image-bg', {
                distance: '-20px',
                duration: 1500,
                opacity: 0
            }
        );
        sr.reveal('.hero-image-caption h1', {
                delay: 1500,
                distance: '20px',
                duration: 1000,
                opacity: 0
            }
        );
        sr.reveal('.hero-image-caption .hero-image-text', {
                delay: 2000,
                distance: '-20px',
                duration: 1000,
                opacity: 0
            }
        );

        // Header navigation
        sr.reveal('body.home #menu-main-menu > li', {
            delay: 2500,
            distance: '-10px',
            duration: 600,
            opacity: 0
        }, 350);

        // Client Logo
        sr.reveal('.logo-grid');
        sr.reveal('.logo-grid .logo-item', {
            delay: 400,
            distance: '20px',
            duration: 800,
            opacity: 0
        }, 400);
        sr.reveal('.logo-grid .btn', {
            distance: '-20px',
            delay: 800,
            opacity: 0
        });

        sr.reveal('.testimonial-grid .testimonial-wrapper', {
            distance: '70px',
            duration: 900,
            opacity: 0
        }, 400);



        // Page Intro
        /*sr.reveal('.page-intro', {
         distance: '30px',
         duration: 1200
         });
         sr.reveal('.page-intro .heading', {
         distance: '20px',
         duration: 1200
         });*/

        // Content Subheading
        /*sr.reveal('section.content .subheading:first-of-type', {
            //delay: 1000
        });*/

        // Featured News
        /*sr.reveal('.blog .intro-text', {
         distance: '20px',
         duration: 1300,
         opacity: 0
         }, 350);*/

        // Recent Posts
        /*sr.reveal('.recent-posts .recent-post', {
         distance: '60px',
         duration: 1300,
         opacity: 0
         }, 500);*/

        // Content component
        /*sr.reveal('section.content');
         sr.reveal('section.content .content-item', {
         distance: '60px',
         duration: 750,
         opacity: 0
         }, 250);*/

        // Testimonial
        /*sr.reveal('.testimonial');
        sr.reveal('.testimonial blockquote');
        sr.reveal('.testimonial footer', {
                duration: 1800
            }
        );*/

        // Featured Properties
        /*sr.reveal('.property-featured .subheading');
         sr.reveal('.property-featured .btn', {
         //delay: 500
         });
         sr.reveal('.property-featured .property', {
         distance: '50px',
         duration: 1200,
         opacity: 0
         }, 350);*/


		// CSS Tricks - Smooth Scrolling
		$('a[href*="#"]:not([href="#"])').click(function() {
			
			if( !$(this).hasClass('no-scroll')) {

				if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
					var target = $(this.hash);
					target = target.length ? target : $('[name=' + this.hash.slice(1) +']');

					return smoothScroll(target);
				}

			}
		});

		if(window.location.hash) {

			var hash = window.location.hash;
			smoothScroll($(hash));

		}

	} );
})( jQuery );
( function( $ ) {
    'use strict';
    $(document).ready(function() {

        var $select = $('.news-filter');
        $select.select2({
            minimumResultsForSearch: Infinity // hide the search box
        });

        $select.on( 'select2:select', function(e) {
            window.location = $(this).val();
        });


        /// Add hover opacity to all un-hovered news articles on blog page
        /*var news_column = $(".news-grid .flex-col");

        news_column.mouseenter(function(){
            $('.flex-col').addClass('hover-fade');
            $(this).removeClass('hover-fade');
            console.log('hover it');
        });
        news_column.mouseleave(function(){
            $('.flex-col').removeClass('hover-fade');
            console.log('leave it');
        });*/
    });

}(jQuery) );

( function( $ ) {
    'use strict';
    $(document).ready(function() {

        // Change the URLs for the videos so they don't autoplay or show related videos
        var magnific = $('.magnific-video');
        magnific.magnificPopup({
            mainClass: 'mfp-fade',
            removalDelay: 160,
            preloader: false,
            fixedContentPos: false,
            type: 'iframe',
            iframe: {
                patterns: {
                    youtube: {
                        index: 'youtube.com/',
                        id: 'v=',
                        src: 'http://www.youtube.com/embed/%id%?rel=0&autoplay=1'
                    },
                    youtubeshare: {
                        index: 'youtu.be/',
                        id: '/',
                        src: 'http://www.youtube.com/embed/%id%?rel=0&autoplay=1'
                    },
                    vimeo: {
                        index: 'vimeo.com/',
                        id: '/',
                        src: '//player.vimeo.com/video/%id%?autoplay=1'
                    }
                }
            }
        });


    });
}(jQuery) );
( function( $ ) {
    'use strict';
    $(document).ready(function() {

        var $select = $('.gallery-filter');
        $select.select2({
            minimumResultsForSearch: Infinity // hide the search box
        });

        $select.on( 'select2:select', function(e) {
            window.location = $(this).val();
        });
        
    });

	$('.single-gallery-image').magnificPopup({
		delegate : 'a',
		type     : 'image',
		tLoading : 'Loading image #%curr%...',
		mainClass: 'mfp-img-mobile',
		gallery  : {
			enabled           : true,
			navigateByImgClick: true,
			preload           : [0, 1] // Will preload 0 - before current, and 1 after the current image
		},
		image    : {
			tError  : '<a href="%url%">The image #%curr%</a> could not be loaded.'
		}
	});


	var magnificPopup = $.magnificPopup.instance;

	/* Loads the function when clicking the lightbox (otherwise it does not take the used class) */
	$("a.dsc-gallery-image").click(function(e) {

		/* Expect to load lightbox */
		setTimeout(function() {
			/* Swipe to the left - Next */
			$(".mfp-container").swipe( {
				swipeLeft:function(event, direction, distance, duration, fingerCount) {
					console.log("swipe right");
					magnificPopup.next();
				},

				/* Swipe to the right - Previous */
				swipeRight:function(event, direction, distance, duration, fingerCount) {
					console.log("swipe left");
					magnificPopup.prev();
				},
			});
		}, 100);
	});


}(jQuery) );
( function( $ ) {
    'use strict';
    $(document).ready(function() {

        /* Main Menu Desktop Hover Intent & Slide */
        // mouseenter
        function showMenuSlide(){

            if($('.navbar-toggle').css('display') === 'none') {
                $(this).siblings().children('.dropdown-menu').hide();
                var dropdownMenu = $(this).find('.dropdown-menu').first();
                dropdownMenu.slideDown(300);
            }
        }
        // mouseleave
        function hideMenuSlide() {
            if($('.navbar-toggle').css('display') === 'none') {
                var dropdownMenu = $(this).find('.dropdown-menu').first();
                dropdownMenu.slideUp(300);
            }
        }

        // Main nav hover intent & fade in/out
        // borrowed from: http://jsfiddle.net/g9JJk/9/, http://www.reddit.com/r/webdev/comments/2893vx/bootstrap_3_dropdown_with_hover_using_hoverintent/
        var config = {
            timeout: 400,
            over: showMenuSlide,
            out: hideMenuSlide
        };

        $('#menu-main-menu .dropdown').hoverIntent(config);

        $('#menu-main-menu a.dropdown-toggle').click(function(e) {
            if( $(this).attr('href') === "#") {
                e.preventDefault();
            }
        });

        // Optional: Make the first link a working link!
        // - expected behaviour on hover menus.
        $('#menu-main-menu .dropdown').on('show.bs.dropdown', function () {
            if($('.navbar-toggle').css('display') === 'none' ) {
                url = $(this).attr('href');

                if( url != '#' ) {
                    window.location.href = url;
                }
                return false;
            }
        });

        // Changing background on header when mobile menu is open
        // Changing background on header when mobile menu is open
        $('.header #navbar-collapse').on('show.bs.collapse', function () {
            $('.header').removeClass('mobile-menu-closed');
            $('.header').addClass('mobile-menu-open');

            $('body').removeClass('mobile-menu-closed');
            $('body').addClass('mobile-menu-open');
        });

        $('.header #navbar-collapse').on('hide.bs.collapse', function () {
            $('.header').removeClass('mobile-menu-open');
            $('.header').addClass('mobile-menu-closed');

            $('body').removeClass('mobile-menu-open');
            $('body').addClass('mobile-menu-closed');
        });

    });

	// ===== Scroll to Top ====
	$(window).scroll(function() {
		if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
			$('#return-to-top').fadeIn(200);    // Fade in the arrow
		} else {
			$('#return-to-top').fadeOut(200);   // Else fade out the arrow
		}
	});
	$('#return-to-top').click(function() {      // When arrow is clicked
		$('body,html').animate({
			scrollTop : 0                       // Scroll to top of body
		}, 500);
	});

}(jQuery) );
( function( $ ) {
	'use strict';
	$(document).ready(function() {

		var owl = $('.sky-carousel');
		owl.owlCarousel({
			loop           : true,
			responsiveClass: true,
			items          : 1, // slides to show
			autoplay       : true,
			autoplayTimeout: 5000, // timeout between slides
			smartSpeed     : 1000, // transition speed
			nav            : true, // prev/next navigation
			dots           : true // dot navigation
		});

	})
}(jQuery) );
( function( $ ) {
	'use strict';
	$(document).ready(function() {
		var owl = $('.dsc-image-carousel');
		owl.owlCarousel({
            animateOut: 'fadeOut',
			loop           : true,
			responsiveClass: true,
			items          : 1, // slides to show
			autoplay       : false,
			autoplayTimeout: 5000, // timeout between slides
			smartSpeed     : 1000, // transition speed
			nav            : true, // prev/next navigation
			navText		   : ['<span class="simple-icon-arrow-left"></span>', '<span class="simple-icon-arrow-right"></span>'],
			dots           : true // dot navigation
		});

	})
}(jQuery) );
( function( $ ) {
	'use strict';
	$(document).ready(function() {

		var testimonials = $('.testimonials-slider');
		testimonials.owlCarousel({
			loop           : true,
			responsiveClass: true,
			items          : 1, // slides to show
			autoplay       : true,
			autoplayTimeout: 5000, // timeout between slides
			smartSpeed     : 1000, // transition speed
			nav            : false, // prev/next navigation
			dots           : false // dot navigation
		});

	})
}(jQuery) );