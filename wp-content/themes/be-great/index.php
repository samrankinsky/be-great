<?php
/**
 * The Index Template File
 *
 * @package Titan_Theme
 * @since   1.0.0
 */

// Action to hook.
do_action( 'titan/template/index' );
