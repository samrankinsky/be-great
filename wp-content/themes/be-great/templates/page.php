<?php
/**
 * Partial: Page
 *
 * This template partial includes any html for the page template.
 *
 * @package Titan_Theme
 * @since   1.0.0
 */
?>
<section class="section page">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<?php the_content(); ?>
			</div>
		</div>
	</div>
</section>
