<?php
/**
 * Partial: No Results
 *
 * On the index template file or a post type archive,
 * this template will display the no results message.
 *
 * @package Titan_Theme
 * @since   1.0.0
 */
if ( ! have_posts() ) { ?>
	<div class="alert alert-warning">
		<?php _e( 'Sorry, no results were found.', 'titan' ); ?>
	</div>
	<?php get_search_form(); ?>
<?php }
