<?php
/**
 * Browsehappy
 *
 * @package Titan_Theme
 * @since   1.0.0
 */
$alert = sprintf(
	__( 'You are using an <strong>outdated</strong> browser. Please <a href="%s">upgrade your browser</a> to improve your experience.', 'titan' ),
	'http://browsehappy.com/'
);
?>
<!--[if IE]>
	<div class="alert alert-warning">
		<?php echo $alert; ?>
	</div>
<![endif]-->
