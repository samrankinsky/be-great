<?php
/**
 * Partial: Single
 *
 * This template partial includes any html for the single template file.
 *
 * @package Titan_Theme
 * @since   1.0.0
 */

while ( have_posts() ) { the_post(); ?>
	<article <?php post_class(); ?>>
		<header>
			<h1 class="entry-title"><?php echo titan_get_the_title(); ?></h1>
			<?php get_template_part( 'templates/meta' ); ?>
		</header>
		<div class="entry-content">
			<?php the_content(); ?>
		</div>
		<?php comments_template( '/templates/comments.php' ); ?>
	</article>
<?php }
