<?php
/**
 * Partial: Header
 *
 * This includes the header, including the primary navigation.
 *
 * @package Titan_Theme
 * @since   1.0.0
 */

titan_get_component( 'header' );