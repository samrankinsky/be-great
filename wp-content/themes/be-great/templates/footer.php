<?php
/**
 * Partial: Footer
 *
 * This includes the footer which contains the copyright.
 *
 * @package Titan_Theme
 * @since   1.0.0
 */

titan_get_component( 'footer' );
