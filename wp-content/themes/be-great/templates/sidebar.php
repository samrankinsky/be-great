<?php
/**
 * Partial: Sidebar
 *
 * This includes any html for the sidebar included in the base.php file.
 *
 * @package Titan_Theme
 * @since   1.0.0
 */

dynamic_sidebar( 'sidebar-primary' ); ?>
