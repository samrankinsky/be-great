<?php
/**
 * Partial: Title
 *
 * This is the html for the h1 title of the page.
 * Included in various places
 *
 * @package Titan_Theme
 * @since   1.0.0
 */

?>

<section class="section page-header">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<h1><?php titan_the_title(); ?></h1>
			</div>
		</div>
	</div>
</section>
