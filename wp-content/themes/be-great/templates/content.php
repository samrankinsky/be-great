<?php
/**
 * Partial: Content
 *
 * This template includes the html for the index template.
 *
 * @package Titan_Theme
 * @since   1.0.0
 */
?>
<article <?php post_class(); ?>>
	<header>
		<h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php echo get_the_title(); ?></a></h2>
		<?php get_template_part( 'templates/meta' ); ?>
	</header>
	<div class="entry-summary">
		<?php the_excerpt(); ?>
	</div>
</article>
