/**
 * EngineBase Gulpfile.js
 *
 * @since   1.0.0
 * @authors Skyhook Interactive
 * @package Titan
 */

/* Load Gulp */
var gulp = require( 'gulp' );

/* CSS related plugins. */
var sass         = require( 'gulp-sass' );
var sassGlob     = require( 'gulp-sass-glob' );
var minifycss    = require( 'gulp-uglifycss' );
var autoprefixer = require( 'gulp-autoprefixer' );
var mmq          = require( 'gulp-merge-media-queries' );

/* JS related plugins. */
var concat = require( 'gulp-concat' );
var uglify = require( 'gulp-uglify' );

/* Image realted plugins. - Minify PNG, JPEG, GIF and SVG images with imagemin. */
var imagemin = require( 'gulp-imagemin' );

/* Utility related plugins. */
var gutil       = require( 'gulp-util' );
var notify      = require( 'gulp-notify' );
var rename      = require( 'gulp-rename' );
var lineec      = require( 'gulp-line-ending-corrector' );
var filter      = require( 'gulp-filter' );
var sourcemaps  = require( 'gulp-sourcemaps' );
var browserSync = require( 'browser-sync' ).create();

/**
 * Local Config
 *
 * Create a 'local.json' file in the root
 * and copy the following, including brackets,
 * to that file and customize accordingly.
 *
 * {
 *    "project" : "Titan",
 *    "url": "http://titan.dev",
 * }
 *
 * @since 1.0.0
 */
var config;
try {
	config = require( './local.json' );
} catch ( error ) {
	// gutil.log( gutil.colors.magenta( error ) );
	// gutil.log( gutil.colors.magenta( 'You are missing your local.json file.' ) );
}

/* Project Related */
var project = ( config && config.project ) ? config.project : 'Be Great';     // Project Name.
var url     = ( config && config.url ) ? config.url : 'http://be-great.local/'; // Project Url.

/* Components */
var components = 'includes/components/'; // Components Path

/* Styles */
var styles      = 'assets/css/scss/app.scss'; // Path to main .scss file.
var styles_dest = 'assets/css/';              // Path to place the compiled CSS file.
var styles_file = 'app';                        // Compiles CSS file name. Default: app.css

/* Vendor JS */
var jsvendor      = 'assets/js/vendor/*.js'; // Path to JS vendor folder.
var jsvendor_dest = 'assets/js/';            // Path to place the compiled JS vendor file.
var jsvendor_file = 'vendor';                  // Compiled JS vendor file name. Default: vendor.js

/* App JS */
var jsapp      = [ 'assets/js/source/*.js', components + '**/*.js', components + '**/**/*.js' ]; // Path to JS custom scripts folder.
var jsapp_dest = 'assets/js/';            // Path to place the compiled JS app scripts file.
var jsapp_file = 'app';                     // Compiled JS app file name. Default: app.js

/* Images */
var images      = 'assets/img/raw/**/*.{png,jpg,gif,svg}'; // Source folder of images which should be optimized.
var images_dest = 'assets/img/'; // Destination folder of optimized images. Must be different from the images folder.

/* Watch SCSS Files */
var watch_styles = [
	'assets/css/scss/**/*.scss',
	'assets/css/scss/mixins/**/*.scss',
	'assets/css/scss/components/**/*.scss',
	components + '**/*.scss',
	components + '**/**/*.scss',
	components + '**/**/**/*.scss'
];

/* Watch JS Vendor Files */
var watch_jsvendor = [
	'assets/js/vendor/*.js'
];

/* Watch JS App Files */
var watch_jsapp = [
	'assets/js/source/*.js',
	components + '**/*.js',
	components + '**/**/*.js',
	components + '**/**/**/*.js'
];

/* Watch PHP Files */
var watch_php = '**/*.php'; // Path to all PHP files.

/* Browsers you care about for autoprefixing. */
/* Browserlist https://github.com/ai/browserslist */
const AUTOPREFIXER_BROWSERS = [
	'last 2 version',
	'> 1%',
	'ie >= 9',
	'ie_mob >= 10',
	'ff >= 30',
	'chrome >= 34',
	'safari >= 7',
	'opera >= 23',
	'ios >= 7',
	'android >= 4',
	'bb >= 10'
];

/**
 * Error Handler
 *
 * @since   1.0.0
 */
var onError = function ( error ) {
	gutil.log( gutil.colors.magenta( error ) );
	this.emit( 'end' );
};

/**
 * Custom Notify
 *
 * @since   1.0.0
 */
var customNotify = notify.withReporter( function ( options, callback ) {
	callback();
} );

/**
 * Task: `app-css`.
 *
 * Compiles Sass, Autoprefixes it and Minifies CSS.
 *
 * This task does the following:
 *    1. Gets the source scss file
 *    2. Compiles Sass to CSS
 *    3. Writes Sourcemaps for it
 *    4. Autoprefixes it and generates app.css
 *    5. Renames the CSS file with suffix .min.css
 *    6. Minifies the CSS file and generates app.min.css
 *    7. Injects CSS or reloads the browser via browserSync
 *
 * @since   1.0.0
 */
gulp.task( 'css', function () {
	gulp.src( styles )
		.pipe( sourcemaps.init() )
		.pipe( sassGlob() )
		.pipe( sass( {
			errLogToConsole: true,
			outputStyle    : 'compact',
			// outputStyle: 'compressed',
			// outputStyle: 'nested',
			// outputStyle: 'expanded',
			precision      : 10
		} ) )
		.on( 'error', console.error.bind( console ) )
		.pipe( sourcemaps.write( { includeContent: false } ) )
		.pipe( sourcemaps.init( { loadMaps: true } ) )
		.pipe( autoprefixer( AUTOPREFIXER_BROWSERS ) )

		.pipe( sourcemaps.write( '.', { sourceRoot: './assets/css/' } ) )
		.pipe( lineec() ) // Consistent Line Endings for non UNIX systems.
		.pipe( gulp.dest( styles_dest ) )

		.pipe( filter( '**/*.css' ) ) // Filtering stream to only css files
		.pipe( mmq( { log: false } ) ) // Merge Media Queries only for .min.css version.

		.pipe( browserSync.stream() ) // Reloads style.css if that is enqueued.

		.pipe( rename( { basename: styles_file, suffix: '.min' } ) )
		.pipe( minifycss( {
			maxLineLen: 10
		} ) )
		.pipe( sourcemaps.write( '.', { sourceRoot: './assets/css/' } ) )
		.pipe( lineec() ) // Consistent Line Endings for non UNIX systems.
		.pipe( gulp.dest( styles_dest ) )

		.pipe( filter( '**/*.css' ) )  // Filtering stream to only css files
		.pipe( browserSync.stream() )  // Reloads app.min.css if that is enqueued.
		.pipe( customNotify( { message: '-------------- CSS Completed! --------------' } ) );
} );

/**
 * Task: `js-vendor`.
 *
 * Concatenate and uglify vendor JS scripts.
 *
 * This task does the following:
 *     1. Gets the source folder for JS vendor files
 *     2. Concatenates all the files and generates vendors.js
 *     3. Renames the JS file with suffix .min.js
 *     4. Uglifes/Minifies the JS file and generates vendors.min.js
 *
 * @since   1.0.0
 */
gulp.task( 'js-vendor', function () {
	gulp.src( jsvendor )
		.pipe( concat( jsvendor_file + '.js' ) )
		.pipe( lineec() ) // Consistent Line Endings for non UNIX systems.
		.pipe( gulp.dest( jsvendor_dest ) )
		.pipe( rename( {
			basename: jsvendor_file,
			suffix  : '.min'
		} ) )
		.pipe( uglify() )
		.pipe( lineec() ) // Consistent Line Endings for non UNIX systems.
		.pipe( gulp.dest( jsvendor_dest ) )
		.pipe( customNotify( { message: '-------------- Vendor JS Completed! --------------' } ) );
} );

/**
 * Task: `js-vendor-watch`
 *
 * Creat a task that ensures the
 * `js-app`
 * task completes before reloading.
 *
 * @since   1.0.0
 */
gulp.task( 'js-vendor-watch', [ 'js-vendor' ], function ( done ) {
	browserSync.reload();
	done();
} );

/**
 * Task: `js-app`.
 *
 * Concatenate and uglify custom JS scripts.
 *
 * This task does the following:
 *     1. Gets the source folder for JS custom files
 *     2. Concatenates all the files and generates custom.js
 *     3. Renames the JS file with suffix .min.js
 *     4. Uglifes/Minifies the JS file and generates custom.min.js
 *
 * @since   1.0.0
 */
gulp.task( 'js-app', function () {
	gulp.src( jsapp )
		.pipe( concat( jsapp_file + '.js' ) )
		.pipe( lineec() ) // Consistent Line Endings for non UNIX systems.
		.pipe( gulp.dest( jsapp_dest ) )
		.pipe( rename( {
			basename: jsapp_file,
			suffix  : '.min'
		} ) )
		.pipe( uglify() )
		.pipe( lineec() ) // Consistent Line Endings for non UNIX systems.
		.pipe( gulp.dest( jsapp_dest ) )
		.pipe( customNotify( { message: '-------------- APP JS Completed! --------------' } ) );
} );

/**
 * Task: `js-app-watch`
 *
 * Creat a task that ensures the
 * `js-app`
 * task completes before reloading.
 *
 * @since   1.0.0
 */
gulp.task( 'js-app-watch', [ 'js-app' ], function ( done ) {
	browserSync.reload();
	done();
} );

/**
 * Task: `images`.
 *
 * Minifies PNG, JPEG, GIF and SVG images.
 *
 * This task does the following:
 *     1. Gets the source of images raw folder
 *     2. Minifies PNG, JPEG, GIF and SVG images
 *     3. Generates and saves the optimized images
 *
 * This task will run only once, if you want to run it
 * again, do it with the command `gulp images`.
 *
 * @since 1.0.0
 */
gulp.task( 'images', function () {
	gulp.src( images )
		.pipe( imagemin( {
			progressive      : true,
			optimizationLevel: 3, // 0-7 low-high
			interlaced       : true,
			svgoPlugins      : [ { removeViewBox: false } ]
		} ) )
		.pipe( gulp.dest( images_dest ) )
		.pipe( customNotify( { message: '-------------- Images Completed! --------------', onLast: true } ) );
} );

/**
 * Build
 *
 * `gulp build`
 *
 * @since   1.0.0
 */
gulp.task( 'build', [ 'css', 'js-vendor', 'js-app' ] );

/**
 * Build Assets
 *
 * `gulp build-assets`
 *
 * @since   1.0.0
 */
gulp.task( 'build-assets', [ 'images' ] );

/**
 * Watch
 *
 * `gulp watch`
 *
 * @since   1.0.0
 */
gulp.task( 'watch', function () {

	// For more options
	// @link http://www.browsersync.io/docs/options/
	browserSync.init( {

		// Watch Files
		files: watch_php,

		// Proxy Url
		proxy: url,

		// Inject CSS changes.
		// Commnet it to reload browser for every CSS change.
		injectChanges: true,

		// Ignore certain paths
		// Used for wp-admin and ajax calls.
		snippetOptions: {
			whitelist: [ '/wp-admin/admin-ajax.php' ],
			blacklist: [ '/wp-admin/**' ]
		},

		// Use a specific port (instead of the one auto-detected by Browsersync).
		// port: 5000
	} );

	// Reload on css file changes.
	gulp.watch( watch_styles, { cwd: './' }, [ 'css' ] );

	// Reload on js-vendor file changes.
	gulp.watch( watch_jsvendor, { cwd: './' }, [ 'js-vendor-watch' ] );

	// Reload on js-app file changes.
	gulp.watch( watch_jsapp, { cwd: './' }, [ 'js-app-watch' ] );

	// Reload on gulfile changes
	gulp.watch( [ 'gulpfile.js' ], { cwd: './' }, [ 'build', 'build-assets' ] );
} );

/**
 * Watcher Tasks.
 *
 * Watches for file changes and runs specific tasks.
 */
gulp.task( 'default', [ 'build', 'build-assets' ] );
