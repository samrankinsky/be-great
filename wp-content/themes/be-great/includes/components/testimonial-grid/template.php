<?php
    /**
     * Testimonial Component - Template
     *
     * @package     Titan
     * @subpackage  Component/Template
     * @version     1.0.0
     * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
     */

?>
<section <?php titan_component_id (); ?> <?php titan_component_class (); ?><?php titan_component_style (); ?>>
	<div class="container">
        <?php
            
            if ( have_rows ( 'testimonial_grid_rows' ) ):
                $row_idx = 1;
                while ( have_rows ( 'testimonial_grid_rows' ) ): the_row ();
                    $layout = get_sub_field ( 'row_layout' );
                    ?>
					<div class="row-flex <?php echo 'testimonial-row-' . $row_idx . ' testimonial-row-' . $layout; ?>">
                        <?php
                            
                            if ( have_rows ( 'row_content' ) ):
                                $idx = 1;
                                while ( have_rows ( 'row_content' ) ): the_row ();
                                    if ( get_row_layout () == 'grid_block' ):
                                        $display = get_sub_field ( 'display' );
                                        $bg = get_sub_field ( 'background_color' );
                                        $content = '';
                                        $type = '';
                                        switch ( $bg ) {
                                            case 'light-gray':
                                                $bg_color = '#F5F5F5';
                                                break;
                                            case 'medium-gray':
                                                $bg_color = '#8d9da9';
                                                break;
                                            case 'dark-gray':
                                                $bg_color = '#728089';
                                                break;
                                            default:
                                                $bg_color = '#FFF';
                                        }
                                        $bg_style = $bg ? 'style="background-color: ' . $bg_color . ';"' : '';
                                        // figure out what the content of the block should be and save it for later
                                        if ( $display == 'testimonial' ):
                                            $testimonial = get_sub_field ( 'testimonial' );
                                            $author = get_sub_field ( 'testimonial_author' );
                                            $type = 'quote';
                                            $content .= $testimonial ? '<blockquote>' . $testimonial .
                                                                       '</blockquote>' : '';
                                            $content .= $author ? '<footer>' . $author . '</footer>' : '';
										elseif ( $display == 'image' ):
                                            $image = get_sub_field ( 'image' );
                                            $type = 'image';
                                            $content .= wp_get_attachment_image ( $image['id'], 'full', false,
                                                                                  array ( 'class' => 'img-responsive' ) );
                                            $bg_style = 'style="background-image: url(' . $image['url'] . ');"';
										elseif ( $display == 'video' ):
                                            $type = 'video';
                                            $image = get_sub_field ( 'screenshot' );
                                            $bg_style = 'style="background-image: url(' . $image . ');"';
                                            if ( have_rows ( 'video' ) ):
                                                $content = '<video autoplay loop class="testimonial-video">';
                                                // loop through the rows of data
                                                while ( have_rows ( 'video' ) ) : the_row ();
                                                    if ( get_row_layout () == 'mp4_file' ):
                                                        $video_file = get_sub_field ( 'mp4_file' );
                                                        $content .= '<source src="' . $video_file['url'] . '" type="video/mp4">';
													elseif ( get_row_layout () == 'ogg_file' ):
                                                        $video_file = get_sub_field ( 'ogg_file' );
                                                        $content .= '<source src="' . $video_file['url'] .'" type="video/ogg">';
													elseif ( get_row_layout () == 'webm_file' ):
                                                        $video_file = get_sub_field ( 'webm_file' );
                                                        $content .= '<source src="' . $video_file['url'] . '" type="video/webm">';
                                                    endif;
                                                endwhile;
                                                $content .= '</video>';
                                            endif;
                                        else:
                                            $content = '';
                                        endif; ?>
                                        
                                        <?php
                                        
                                        // figure out what the column/layout should be
                                        if ( $layout == '3col' ): ?>
											<div class="col-xs-12 col-sm-4">
												<div class="testimonial-wrapper <?php echo $type; ?>" <?php echo $bg_style; ?>>
													<div class="testimonial-content">
                                                        <?php echo $content; ?>
													</div>
												</div>
											</div>
                                        <?php
                                        else:
                                            if ( $idx > 1 ): ?>
												<div class="testimonial-wrapper <?php echo $type; ?>" <?php echo $bg_style; ?>>
													<div class="testimonial-content">
                                                        <?php echo $content; ?>
													</div>
												</div>
                                            <?php else: ?>
												<div class="col-xs-12 col-ms-6">
													<div class="testimonial-wrapper <?php echo $type; ?>" <?php echo $bg_style; ?>>
														<div class="testimonial-content">
                                                            <?php echo $content; ?>
														</div>
													</div>
												</div>
												<div class="col-xs-12 col-ms-6 flex-col">
                                            <?php endif; ?>
                                            
                                            <?php if ( $idx == 3 ): ?>
											</div>
                                        <?php endif; ?>
                                        
                                        <?php endif; ?>
                                    
                                    <?php endif; ?>
                                    <?php $idx ++; endwhile;
                            endif; ?>
					</div>
                    <?php
                    
                    $row_idx ++;
                endwhile;
            endif;
        ?>
	</div>
</section>