<?php
/**
 * Callout Icon Component
 *
 * @package     Titan
 * @subpackage  Component
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

# Add width to advanced settings
add_filter( "titan/component/{$component->name}/acf/advanced", 'advanced_callout_icon' );

/**
 * Add 'width' field to advanced settings
 *
 * @since  1.0.0
 *
 * @param  array $advanced
 *
 * @return array
 */
function advanced_callout_icon( $advanced ) {
	return wp_parse_args( array( 'width' => get_sub_field( 'width' ) ), $advanced );
}
