<?php
/**
 * Callout Icon Component - Template
 *
 * @package     Titan
 * @subpackage  Component/Template
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */
?>
<section <?php titan_component_id(); ?> <?php titan_component_class(); ?> <?php titan_component_style(); ?>>
	<div class="<?php echo ( 'container' === titan_component_get_advanced_by_key( 'width' ) ) ? 'container' : 'container-fluid'; ?>">
		<div class="row">
			<div class="col-xs-12">
				<?php titan_component_repeater( 'callout-icons' ); ?>
			</div>
		</div>
	</div>
</section>
