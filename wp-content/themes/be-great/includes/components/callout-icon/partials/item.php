<?php
/**
 * Callout Icon Component - Template
 *
 * @package     Titan
 * @subpackage  Component/Template
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

// Variables
$icon     = get_sub_field( 'icon' );
$title    = get_sub_field( 'title' );
$text     = get_sub_field( 'text' );
$btn_link = get_sub_field( 'btn_link' );
$btn_text = get_sub_field( 'btn_text' );

// Check
if ( ! $icon && ! $text ) return;
?>
<div class="callout">
	<?php if ( $icon ) { ?>
		<div class="icon"><i class="fa <?php echo $icon; ?> round"></i></div>
	<?php } ?>
	<?php if ( $title ) { ?>
		<h3><?php echo $title; ?></h3>
	<?php } ?>
	<?php echo $text; ?>
	<?php if ( $btn_link && $btn_text ) { ?>
		<a href="<?php echo $btn_link; ?>" class="align-bottom btn btn-primary"><?php echo $btn_text; ?></a>
	<?php } ?>
</div>
