( function( $ ) {
	'use strict';
	$(document).ready(function() {

		var owl = $('.sky-carousel');
		owl.owlCarousel({
			loop           : true,
			responsiveClass: true,
			items          : 1, // slides to show
			autoplay       : true,
			autoplayTimeout: 5000, // timeout between slides
			smartSpeed     : 1000, // transition speed
			nav            : true, // prev/next navigation
			dots           : true // dot navigation
		});

	})
}(jQuery) );