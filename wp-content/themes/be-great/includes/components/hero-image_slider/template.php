<?php
/**
 * Hero Image Slider Component - Template
 *
 * @package     Titan
 * @subpackage  Component/Template
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

// Check
if ( ! have_rows( 'images' ) ) {
	return;
}
?>
<section <?php titan_component_id(); ?> <?php titan_component_class(); ?> <?php titan_component_style(); ?>>
	<div class="sky-carousel">
		<?php while ( have_rows( 'images' ) ) {
			the_row(); ?>
			<div class="item">
				<?php
				$image = get_sub_field( 'image' );
				if ( $image ) {
					echo wp_get_attachment_image( $image[ 'id' ], 'full' );
				}
				?>
			</div>
		<?php } ?>
	</div>
</section>