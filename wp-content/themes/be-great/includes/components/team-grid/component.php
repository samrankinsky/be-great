<?php

# Remove component layout options for this component
//add_filter( "titan/component/{$component->name}/layout", 'titan_component_remove_layout_support' );

# Repeater Layouts
add_filter( "titan/component/{$component->name}/repeater/layouts", 'titan_teamgrid_repeater_layouts' );

/*-----------------------------------------------------------------------------------*/
/* Properties CPT
/*-----------------------------------------------------------------------------------*/
# Register Testimonials Custom Post Type
add_action( 'titan/loaded', 'titan_team_register' );


/**
 * Register Property Post Type + Options
 *
 * @since   1.0.0
 *
 * @return  void
 */
function titan_team_register() {

	// Register Post Type
	$team = new CPT( array(
		'post_type_name' => 'teammember',
		'singular'       => 'Our Team',
		'plural'         => 'Our Team',
		'slug'           => 'our-team'
	), array(
		'supports'          => array( 'title', 'editor', 'page-attributes', 'thumbnail' ),
		'menu_icon'         => 'dashicons-businessman',
		'rewrite'           => array( 'slug' => 'our-team', 'with_front' => false ),
		//'taxonomies'        => false,
		'has_archive'       => false,
		'hierarchical'      => true,
		'capability_type'   => 'post'
	) );







	// Set Component - This will set the path for all the templates including:
	// 'archive.php' 'taxonomy.php' 'single.php'
	$team->set_component( 'team-grid' );
}



function dsc_team_screen_options($options) {

	return array(
		//0  => 'the_content',
		1  => 'excerpt',
		2  => 'custom_fields',
		3  => 'discussion',
		4  => 'comments',
		5  => 'revisions',
		6  => 'slug',
		7  => 'author',
		8  => 'format',
		9  => 'categories',
		10 => 'tags',
		11 => 'send-trackbacks',
		//12 => 'featured_image',
	);

}
add_filter('titan/builder/single-team-builder/hide_on_screen', 'dsc_team_screen_options', 10, 2);


/**
 * Update Conent Component Repeater Layouts
 *
 * @since   1.0.0
 *
 * @param   array $layouts Default layouts
 *
 * @return  array $layouts Adjusted layouts
 */
function titan_teamgrid_repeater_layouts( $layouts ) {

	$newlayouts[ '3_col' ] = array(
		'name' => __( 'Three Column' ),
		'classes' => 'col-xs-12 col-sm-4 col-md-4'
	);

	$newlayouts[ '4_col_' ] = array(
		'name' => __( 'Four Column' ),
		'classes' => 'col-xs-12 col-sm-4 col-md-3'
	);

	// Return
	return $newlayouts;
}