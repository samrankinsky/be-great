<?php
/**
 * Team Template File
 *
 * This custom template is used for builder modules.
 *
 * @package Titan_Theme
 * @since   1.0.0
 *
 * Template Name: Builder
 */

the_post();

$profile_title = get_field('team_title');

?>
	<section class="component blog-component blog-single">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-10 col-sm-offset-1">

					<article <?php post_class(); ?>>
						<header>
							<h1 class="heading text-center"><?php echo titan_get_the_title(); ?></h1>
							<h2 class="profile-title"><?php echo $profile_title; ?></h2>
						</header>
						<div class="entry-content">
							<div class="profile-image" style="background-image: url(<?php the_post_thumbnail_url('large'); ?>);"></div>
							<?php the_content(); ?>
						</div>
						<?php comments_template( '/templates/comments.php' ); ?>
					</article>

				</div>
			</div>
		</div>
	</section>

<?php

/// Include blog post builder
titan_builder( 'single-team-builder' );

?>