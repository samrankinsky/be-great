<?php
/**
 * Team Grid Component - Template
 *
 * @package     Titan
 * @subpackage  Component/Template
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */


global $post;

$team_grid_members = get_sub_field('team_grid_members');
$include_all_team_members = get_sub_field('include_all_team_members');


// Use normal post query if the user has selected to include all team embers, or get team grid relationship field
if ($include_all_team_members == true) {
    $args = array(
        'posts_per_page' => 100,
        'post_status' => 'publish',
        'post_type' => array('teammember'),
        'order' => 'ASC'
    );
    $posts = new WP_Query( $args );
    $posts = $posts->get_posts();
} else {
	$posts = get_sub_field('team_grid_members');
}


$layout = get_sub_field( 'component_layout' );
?>

<section <?php titan_component_id(); ?> <?php titan_component_class(); ?> <?php titan_component_style(); ?>>
	<div class="container">
        <div class="row">

            <?php if( $posts ): ?>
                <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
                    <?php setup_postdata($post); ?>
                    <?php $featured_image = get_the_post_thumbnail_url($post, 'full'); ?>
                    <?php $team_title = get_field('team_title', $post); ?>

                    <div class="<?php echo titan_component_repater_get_column_classes( $layout ); ?> team-member">
                        <a href="<?php echo get_the_permalink(); ?>" class="member-link" style="background-image: url( <?php echo $featured_image; ?> );">
                            <h2 class="name"><?php echo get_the_title(); ?></h2>
                            <p class="bio-link">View Bio</p>
                        </a>
                        <div class="member-footer">
                            <h2 class="name"><?php echo get_the_title(); ?></h2>
                            <h3 class="role"><?php echo $team_title; ?></h3>
                        </div>
                    </div>

                <?php endforeach; ?>
                <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
	        <?php endif; ?>

        </div>
	</div>
</section>