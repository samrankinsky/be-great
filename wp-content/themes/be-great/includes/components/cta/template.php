<?php
/**
 * Call to Action Component - Template
 *
 * @package     Titan
 * @subpackage  Component/Template
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

$use_global = get_sub_field( 'use_global_cta_settings' );

if( $use_global ) {
	$heading    = get_field( 'cta_heading', 'options' );
	$text       = get_field( 'cta_text', 'options' );
	$btn_text   = get_field( 'cta_button_text', 'options' );
	$btn_link   = get_field( 'cta_button_link', 'options' );
	$target     = get_field( 'cta_open_link_new_window', 'options' ) ? ' target="_blank"' : '';
} else {
	$heading    = get_sub_field( 'cta_heading' );
	$text       = get_sub_field( 'cta_text' );
	$btn_text   = get_sub_field( 'cta_button_text' );
	$btn_link   = get_sub_field( 'cta_button_link' );
	$target     = get_sub_field( 'cta_open_link_new_window' ) ? ' target="_blank"' : '';
}
?>

<section <?php titan_component_id(); ?> <?php titan_component_class(); ?> <?php titan_component_style(); ?>>
	<div class="container">
		<div class="cta-inner text-center">
			<div class="cta-border">
				<?php if( $heading ): ?>
					<h2 class="subheading"><?php echo $heading; ?></h2>
				<?php endif; ?>

				<?php echo $text ?>

				<?php if( $btn_text && $btn_link ): ?>
					<a class="btn btn-primary" href="<?php echo $btn_link; ?>"<?php echo $target; ?>><?php echo $btn_text; ?></a>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>