<?php
/**
 * Property Template File
 *
 * This custom template is used for builder modules.
 *
 * @package Titan_Theme
 * @since   1.0.0
 *
 * Template Name: Builder
 */

the_post();

# Output Builder
titan_builder( 'builder-property' );
