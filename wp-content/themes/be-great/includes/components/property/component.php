<?php

/*-----------------------------------------------------------------------------------*/
/* Properties CPT
/*-----------------------------------------------------------------------------------*/
# Register Testimonials Custom Post Type
add_action( 'titan/loaded', 'titan_property_register' );


/**
 * Register Property Post Type + Options
 *
 * @since   1.0.0
 *
 * @return  void
 */
function titan_property_register() {

	// Register Post Type
	$properties = new CPT( array(
		'post_type_name' => 'property',
		'singular'       => 'Property',
		'plural'         => 'Properties',
		'slug'           => 'custom-home-builder'
	), array(
		'supports'          => array( 'title', 'thumbnail', 'page-attributes' ),
		'menu_icon'         => 'dashicons-admin-home',
		'rewrite'           => array( 'slug' => 'custom-home-builder', 'with_front' => true ),
		//'taxonomies'        => array( 'property_types' ),
		'has_archive'       => false,
		'hierarchical'      => true,
		'capability_type'   => 'post'
	) );

	// Register Taxonomy
	//$properties->register_taxonomy( array(
	//	'taxonomy_name' => 'property_types',
	//	'singular'      => 'Property Type',
	//	'plural'        => 'Property Types'
	//), array(
	//	'rewrite' => true
	//) );


	// Set Component - This will set the path for all the templates including:
	// 'archive.php' 'taxonomy.php' 'single.php'
	$properties->set_component( 'property' );
}


# Show featured image on properties
add_filter( "titan/builder/builder-property/hide_on_screen", 'dsc_property_builder_options');
/**
 * Show the featured image for the property post type
 * This is targeted by the builder name - builder-property
 * @return array
 */
function dsc_property_builder_options(){
	return array(
		0  => 'the_content',
		1  => 'excerpt',
		2  => 'custom_fields',
		3  => 'discussion',
		4  => 'comments',
		5  => 'revisions',
		6  => 'slug',
		7  => 'author',
		8  => 'format',
		9  => 'categories',
		10 => 'tags',
		11 => 'send-trackbacks',
		//12 => 'featured_image',
	);
}