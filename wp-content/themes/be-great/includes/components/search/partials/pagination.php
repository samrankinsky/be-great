<?php
/**
 * Search Component - Template Partial - Pagination
 *
 * @package     Titan
 * @subpackage  Component/Template/Partial
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */
global $wp_query;
if ( $wp_query->max_num_pages > 1 ) { ?>
	<nav class="post-nav">
		<?php titan_pagination( $wp_query->max_num_pages, 3 ); ?>
	</nav>
<?php }
