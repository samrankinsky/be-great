<?php
/**
 * Search Component - Template Partial - Meta
 *
 * @package     Titan
 * @subpackage  Component/Template/Partial
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */
?>
<div class="meta">
	<time class="updated" datetime="<?php echo get_post_time( 'c', true ); ?>"><?php echo get_the_date( 'F j, Y'); ?></time>
</div>
