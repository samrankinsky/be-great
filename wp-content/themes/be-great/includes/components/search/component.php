<?php
/**
 * Search Template Component
 *
 * @package     Titan
 * @subpackage  Component
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

# Override the Search Template Path
add_filter( 'titan/template/search/path', 'titan_search_component_path' );

/**
 * Search Path
 *
 * @since   1.0.0
 * @access  public
 * @return  string
 */
function titan_search_component_path() {
	return trailingslashit( TITAN_COMPONENTS . 'search' );
}
