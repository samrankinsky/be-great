<?php
/**
 * Search Template File
 *
 * @package Titan
 * @since   1.0.0
 */
?>

<section class="component search">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 title">
				<h1><?php titan_the_title(); ?></h1>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 index">
				<?php if ( have_posts() ) { ?>
					<?php while ( have_posts() ) { the_post(); ?>
						<?php titan_component_part( 'search', 'partials/content' ); ?>
					<?php } ?>
				<?php } else { ?>
					<div class="no-results">
						<p><?php printf( __( 'Sorry, no search results were found using the term <strong>"%s"</strong>. Please try again using the search form below.', 'titan' ), get_search_query() ); ?></p>
						<?php get_search_form(); ?>
					</div>
				<?php } ?>
				<?php titan_component_part( 'search', 'partials/pagination' ); ?>
			</div>
		</div>
	</div>
</section>
