<?php
/**
 * Recent Posts Component - Template
 *
 * @package     Titan
 * @subpackage  Component/Template
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

// Global
global $post;

// Variables
$heading    = get_sub_field( 'heading' );
$type       = get_sub_field( 'display' );

// Specific
$rps = ( $type == 'specific' ) ? get_sub_field( 'recent_posts' ) : array();

// All
if ( $type == 'all' ) {
	$rps_query = new WP_Query( array(
		'post_type'           => 'post',
		'posts_per_page'      => 3,
		'ignore_sticky_posts' => true,
	) );
	$rps = $rps_query->posts;
}

// Check
if ( empty( $rps ) ) return;
?>
<section <?php titan_component_id(); ?> <?php titan_component_class(); ?><?php titan_component_style(); ?>>
	<div class="container">
		<div class="row">
			<?php if( $heading ): ?>
				<h2 class="subheading text-center"><?php echo $heading; ?></h2>
			<?php endif; ?>
		</div>
		<div class="row-flex">
			<?php foreach ( $rps as $post ) { setup_postdata( $post ); ?>
				<div class="col-xs-12 col-sm-4">
					<div class="recent-post">

                        <?php if( has_post_thumbnail() ) { ?>
							<a href="<?php the_permalink(); ?>" class="post-thumbnail" style="background-image: url( <?php the_post_thumbnail_url('large'); ?> );"></a>
						<?php } ?>

						<div class="post-details">
							<h4 class="h2 categories"><?php echo get_the_category_list( __( ', ', 'titan' ) ); ?></h4>
							<a class="post-title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
						</div>
					</div>
				</div>
			<?php } wp_reset_postdata(); ?>
		</div>
		<p class="text-center"><a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>" class="btn btn-primary">See more News</a></p>
	</div>
</section>