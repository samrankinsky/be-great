<h3><?php the_title(); ?></h3>
<?php the_excerpt(); ?>
<p><a href="<?php the_permalink(); ?>" class="btn btn-primary"><?php _e( 'Read Post', TITAN_DOMAIN ); ?></a></p>