<?php
/**
 * Property Credits Component - Template
 *
 * @package     Titan
 * @subpackage  Component/Template
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

$heading    = get_sub_field( 'heading' );

/* Credit Items */
$rows       = get_sub_field( 'credits_list' );
$row_count  = count($rows);
$idx        = 1;

/*
	$split = the index to split the list at. if there are more than 3 links, split at the halfway point
	if there are  fewer than 4 items on the list,
	don't split (set index to 1 more than the number of rows)
*/
$split      = ( $row_count > 3 ) ? $row_count / 2 : $row_count + 1;

?>

<section <?php titan_component_id(); ?> <?php titan_component_class(); ?> <?php titan_component_style(); ?>>
	<div class="container">
		<?php

		if( have_rows( 'credits_list' )): ?>
			<?php if( $heading ): ?>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-5">
						<h4 class="gold-heading"><?php echo $heading; ?></h4>
				</div>
			</div>
			<?php endif; ?>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-5">

					<ul class="list-unstyled prop-list">

						<?php while( have_rows( 'credits_list' )): the_row();
						$link   = get_sub_field( 'credit_text_link' );

						if( $link ): ?>
							<li><?php the_sub_field( 'credit_label' ); ?>: <a href="<?php echo $link ?>" target="_blank"><?php the_sub_field( 'credit_text' ); ?></a></li>
						<?php else: ?>
							<li><?php the_sub_field( 'credit_label' ); ?>: <?php the_sub_field( 'credit_text' ); ?></li>
						<?php endif; ?>

						<?php
						if( $idx == $split ): ?>
					</ul>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1">
					<ul class="list-unstyled prop-list">
						<?php endif; ?>

					<?php
						$idx++;
						endwhile; ?>

					</ul>
				</div>
			</div>
		<?php endif; ?>
	</div>  
</section>