<?php
/**
 * Property Details Component
 *
 * @package     Titan
 * @subpackage  Component
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

# Remove component layout options for this component
add_filter( "titan/component/{$component->name}/layout", 'titan_component_remove_layout_support' );

// find unaltered paragraph tags, set the class to lead then add dropcap class to first paragraph
function dsc_format_lead_text($text) {

	$formatted_text     = str_replace( '<p>', '<p class="lead">', $text );
	$pos                = strpos( $formatted_text, '<p class="lead">' );

	if ($pos !== false) {
		$formatted_text = substr_replace($formatted_text,'<p class="lead dropcap">',$pos,strlen('<p class="lead">'));
	}

	echo $formatted_text;
}