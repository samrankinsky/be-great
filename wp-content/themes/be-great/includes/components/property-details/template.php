<?php
/**
 * Property Details Component - Template
 *
 * @package     Titan
 * @subpackage  Component/Template
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

$text           = get_sub_field( 'text' );
$share_link     = get_sub_field( 'include_share_link' );
$share_text     = get_sub_field( 'share_link_text' );
?>

<section <?php titan_component_id(); ?> <?php titan_component_class(); ?> <?php titan_component_style(); ?>>
	<div class="container">
		<div class="row">
			<?php if( $text ): ?>
			<div class="col-xs-12 col-sm-7">
				<?php dsc_format_lead_text( $text ); ?>
			</div>
			<?php endif; ?>
			<?php if( have_rows( 'property_details' ) ): ?>
			<div class="col-xs-12 col-sm-4 col-sm-offset-1 col-md-3">
				<h4 class="gold-heading">Property Details</h4>
				<ul class="list-unstyled prop-list">					
				<?php while( have_rows( 'property_details' ) ): the_row(); ?>
					<li><?php the_sub_field( 'detail_label' ); ?>: <?php the_sub_field( 'detail_text' ); ?></li>
				<?php endwhile; ?>
					<?php if( $share_link ): ?>
						<?php $share_url = ( $share_url = get_field( 'short_url' ) ) ? $share_url : get_permalink(); ?>
						<li><?php the_sub_field( 'share_link_text' ); ?> <span class='st_sharethis_large' st_title='<?php the_title(); ?>' st_url='<?php echo $share_url; ?>'></span></li>
					<?php endif; ?>
				</ul>
			</div>
			<?php endif; ?>
		</div>
	</div>  
</section>