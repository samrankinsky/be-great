<?php
/**
 * Settings Component
 *
 * @package     Titan
 * @subpackage  Component
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

# ACF Settings Page
add_action( 'init', 'titan_acf_settings_page', 0 );

/**
 * Add Settings Page
 *
 * @since   1.0.0
 * @access  public
 * @return  void
 */
function titan_acf_settings_page() {
	if ( function_exists( 'acf_add_options_page' ) ) {

		// Default Settings Page
		$default_settings = acf_add_options_page( array(
			'icon_url'   => apply_filters( 'titan/acf/settings/default/icon', 'dashicons-screenoptions' ),
			'page_title' => apply_filters( 'titan/acf/settings/default/page_title', __( 'Options', 'titan' ) ),
			'menu_title' => apply_filters( 'titan/acf/settings/default/menu_title', __( 'Options', 'titan' ) ),
			'menu_slug'  => apply_filters( 'titan/acf/settings/default/menu_slug', 'titan-acf-options' )
		) );

		// Add General Settings
		acf_add_options_sub_page( array(
			'page_title'  => 'General Options',
			'menu_title'  => 'General Options',
			'menu_slug'   => 'titan-general-options',
			'parent_slug' => $default_settings[ 'menu_slug' ],
		) );

		// Actions
		do_action( 'titan/acf/settings/register' );
		do_action( 'titan/acf/settings/register/sub', $default_settings );
	}
}
