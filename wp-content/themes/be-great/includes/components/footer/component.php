<?php
/**
 * Footer Component
 *
 * @package     Titan
 * @subpackage  Component
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

# Add Menu to theme menus array
add_filter( 'titan/menus', 'titan_footer_component_add_menu' );

# Register Sub Setting Page
add_action( 'titan/acf/settings/register/sub', 'titan_footer_register_settings' );

/**
 * Menu
 *
 * @since   1.0.0
 * @access  public
 * @param   array   $menus
 * @return  array
 */
function titan_footer_component_add_menu( $menus ) {
	$menus[ 'footer_top_navigation' ] = __( 'Footer Top Navigation', 'titan' );
	$menus[ 'footer_bottom_navigation' ] = __( 'Footer Bottom Navigation', 'titan' );
	return $menus;
}

/**
 * Footer - Register Sub Setting Page
 *
 * @since   1.0.0
 * @access  public
 * @param   array  $parent_settings_slug    The slug of the parent settings page.
 * @return  void
 */
function titan_footer_register_settings( $parent_settings_slug ) {
	if ( function_exists( 'acf_add_options_sub_page' ) ) {
		acf_add_options_sub_page( array(
			'page_title'  => 'Footer Options',
			'menu_title'  => 'Footer Options',
			'menu_slug'   => 'titan-footer-options',
			'parent_slug' => $parent_settings_slug[ 'menu_slug' ]
		) );
	}
}
