<?php
/**
 * Footer Component - Template
 *
 * @package     Titan
 * @subpackage  Component/Template
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

$phone          = get_field( 'phone', 'options' );
$phone_label    = get_field( 'phone_label', 'options' );
$social_label   = get_field( 'social_media_label', 'options' );
$share_label    = get_field( 'share_label', 'options' );

$facebook  = get_field( 'facebook', 'options' );
$instagram = get_field( 'instagram', 'options' );
$linkedin  = get_field( 'linkedin', 'options' );
?>
<footer <?php titan_component_class( 'content-info' ); ?> <?php titan_component_style(); ?>>
	<div class="footer-top">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-10 col-md-offset-1 text-center">
					<?php
					if ( has_nav_menu( 'footer_top_navigation' ) ) {
						wp_nav_menu( array(
							'theme_location' => 'footer_top_navigation',
							'menu_class'     => 'list-inline'
						) );
					}
					?>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-10 col-xs-offset-1 col-md-8 col-md-offset-2 text-center">
					<div class="connect-row">
						<div class="call-us">
                            <span><?php echo $phone_label . '</span> <a class="phone-mobile" href="tel:' . $phone . '">' . $phone . '</a>'; ?>

                        </div>
						<div class="social">
							<span class="social-label"><?php echo $social_label; ?></span>
							<?php if ( $facebook ) : ?><a href="<?php echo $facebook; ?>" target="_blank"><span class="fa fa-facebook"></span></a><?php endif; ?>
							<?php if ( $instagram ) : ?><a href="<?php echo $instagram; ?>" target="_blank"><span class="fa-stack"><i class="fa fa-square fa-stack-1x"></i><i class="fa fa-instagram fa-stack-1x fa-inverse"></i></span></a><?php endif; ?>
							<?php if ( $linkedin ) : ?><a href="<?php echo $linkedin; ?>" target="_blank"><span class="fa fa-linkedin"></span></a><?php endif; ?>
						</div>

						<?php $share_url = ( $share_url = get_field( 'short_url' ) ) ? $share_url : get_permalink(); ?>
						<div class="share"><?php echo $share_label; ?> <span class='st_sharethis_large' st_title='<?php the_title(); ?>' st_url='<?php echo $share_url; ?>'></span></div>
					</div>
					</div>
				</div>
		</div>
	</div>

	<div class="footer-bottom">
		<div class="container">
			<div class="row">
				<div class="col-xs-10 col-xs-offset-1 text-center border-top">
					<?php
					if( have_rows( 'licenses', 'options' )): ?>
					<ul class="list-inline licenses">
						<?php while( have_rows( 'licenses', 'options' )): the_row(); ?>
							<li><?php the_sub_field( 'license' ); ?></li>
						<?php endwhile; ?>
					</ul>
					<?php endif; ?>

					<?php
					if ( has_nav_menu( 'footer_bottom_navigation' ) ) {
						wp_nav_menu( array(
							'theme_location' => 'footer_bottom_navigation',
							'menu_class'     => 'list-inline'
						) );
					}
					?>
					<div class="copyright"><?php echo str_replace( "{year}", date( 'Y' ), get_field( 'copyright', 'options' ) ); ?></div>
				</div>
			</div>
		</div>
	</div>

</footer>

<a href="javascript:" id="return-to-top"><i class="fa fa-chevron-up"></i></a>