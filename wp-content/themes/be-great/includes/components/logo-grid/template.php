<?php
/**
 * Logo Grid Component - Template
 *
 * @package     Titan
 * @subpackage  Component/Template
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

$heading        = get_sub_field( 'heading' );
$text           = get_sub_field( 'text' );
$btn_text       = get_sub_field( 'btn_text' );
$btn_link       = get_sub_field( 'btn_link' );

?>

<section <?php titan_component_id(); ?> <?php titan_component_class(); ?> <?php titan_component_style(); ?>>
	<div class="container">
		<div class="row intro">
			<div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
				<?php if( $heading ): ?>
					<h2 class="subheading text-center"><?php echo $heading; ?></h2>
				<?php endif; ?>
				<?php echo $text; ?>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-10 col-sm-offset-1">
				<?php
				if( have_rows( 'logos' )): ?>
					<div class="row-flex">
						<?php while( have_rows( 'logos' ) ): the_row();
							$image      = get_sub_field( 'logo' );
							$img_html   = wp_get_attachment_image( $image['id'], 'full', false, array( 'class' => 'img-responsive' ) );
							$link       = get_sub_field( 'logo_link' );
							?>
							<div class="col-xs-6 col-md-3 logo-item">
								<?php if( $link ): ?>
									<a href="<?php echo $link; ?>" target="_blank"><?php echo $img_html; ?></a>
								<?php else:
									echo $img_html;
								endif; ?>
							</div>
						<?php endwhile; ?>
					</div>
				<?php endif; ?>
				<?php if( $btn_text && $btn_link ): ?>
					<p class="text-center"><a href="<?php echo $btn_link; ?>" class="btn btn-primary"><?php echo $btn_text; ?></a></p>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>