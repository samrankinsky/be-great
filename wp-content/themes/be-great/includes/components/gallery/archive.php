<?php
/**
 * Gallery Component - Archive
 *
 * @package     Titan
 * @subpackage  Component/Template
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

$heading        = '';
$text_layout    = '';
$text           = '';
$hero           = '';

// Modify the query so we order by menu_order rather than published_date
$spaces_qv     = get_query_var( 'spaces' );
$types_qv      = get_query_var( 'types' );

$is_landing     = ( $spaces_qv == null && $types_qv == null ) ? true : false;
$is_combo       = ( $spaces_qv != null && $types_qv != null ) ? true : false;
$is_type        = ( $spaces_qv == null && $types_qv != null ) ? true : false;
$is_space       = ( $spaces_qv != null && $types_qv == null ) ? true : false;

$type           = get_term_by( 'slug', $types_qv, 'types' );
$space          = get_term_by( 'slug', $spaces_qv, 'spaces' );
$gallery_id     = dsc_get_gallery_page_id();

if( $is_landing ):
	titan_builder( 'builder', $gallery_id );

elseif( $is_combo ):
	$heading        = $type->name . ' ' . $space->name;
	$text_layout    = 'standard';
	$text           = '';

elseif( $is_type ):
	$heading        = $type->name;
	$text_layout    = 'standard';
	$text           = '<p>' . $type->description . '</p>';
	$hero           = get_field( 'hero_image', $type->taxonomy . '_' . $type->term_id );

elseif( $is_space ):
	$heading        = $space->name;
	$text_layout    = 'standard';
	$text           = '<p>' . $space->description . '</p>';
	$hero           = get_field( 'hero_image', $space->taxonomy . '_' . $space->term_id );
endif;

if ( ! $is_landing ) {
	?>
	<section class="component page-intro">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-10 col-sm-offset-1">
					<?php if ( $heading ): ?>
						<h1 class="h2 text-center"><?php echo $heading; ?></h1>
					<?php endif; ?>

					<?php if ( $text_layout == 'dropcap' ):
						echo str_replace( '<p>', '<p class="lead dropcap">', $text );
					else:
						echo '<div class="text-center">' . $text . '</div>';
					endif; ?>

				</div>
			</div>
		</div>
	</section>
	<?php
}

if( $hero ): /*?>
	<section class="component tax-featured-image">
		<div class="container">
			<?php
			if ( $hero ) {
				echo wp_get_attachment_image( $hero[ 'id' ], 'full', false, array( 'class' => 'img-responsive' ) );
			}
			?>
		</div>
	</section>
<?php*/ endif; ?>

<section class="component gallery-filters">
	<div class="container">
		<div class="gallery-filter-wrap">
			<span class="filter-icon">
				<svg width="33px" height="29px" viewBox="0 0 33 29" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
				    <defs></defs>
				    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
				        <g id="Gallery" transform="translate(-133.000000, -456.000000)" stroke-width="2">
				            <g id="filter" transform="translate(134.000000, 446.000000)">
				                <g id="icon" transform="translate(0.000000, 11.000000)">
				                    <path d="M0.5,4 L30.0169443,4" id="Line" stroke="#00374D" stroke-linecap="square"></path>
				                    <path d="M0.5,23 L30.0169443,23" id="Line-Copy-3" stroke="#00374D" stroke-linecap="square"></path>
				                    <path d="M0.5,13 L30.0169443,13" id="Line-Copy-2" stroke="#00374D" stroke-linecap="square"></path>
				                    <circle class="filter-first-circle" id="Oval" stroke="#D1B275" fill="#FFFFFF" cx="22" cy="4" r="4"></circle>
				                    <circle class="filter-second-circle" id="Oval-Copy" stroke="#D1B275" fill="#FFFFFF" cx="9" cy="13" r="4"></circle>
				                    <circle class="filter-third-circle" id="Oval-Copy-2" stroke="#D1B275" fill="#FFFFFF" cx="22" cy="23" r="4"></circle>
				                </g>
				            </g>
				        </g>
				    </g>
				</svg>
			</span>
			<span class="filter-label"><?php _e( 'Filter by', 'dsc' ); ?></span>
			<?php
			echo dsc_get_gallery_type_filter( $spaces_qv, $types_qv );
			echo dsc_get_gallery_spaces_filter( $spaces_qv, $types_qv );

			$clear_class = (  $is_landing ) ? 'hide-me' : '';
			?>
			<a href="<?php echo get_the_permalink( $gallery_id ); ?>" class="clear-filters <?php echo $clear_class; ?>"><?php _e( 'x clear filters', 'dsc' ); ?></a>
		</div>
	</div>
</section>

<?php
$galleryclass = ( $is_landing ) ? '' : 'gallery-list-images';
?>
<section class="component gallery-component gallery-archive <?php echo $galleryclass; ?>">
	<div class="container">
		<div class="row-flex">
		<?php
		// if is main gallery page, list all the taxonomies otherwise list all the photos
		if( $is_landing ): ?>
			<?php titan_component_part( 'gallery', 'partials/archive' ); ?>
		<?php else: ?>
			<?php while ( have_posts() ) { the_post(); ?>
				<?php titan_component_part( 'gallery', 'partials/content' ); ?>
			<?php } ?>
		</div>
		<?php titan_component_part( 'gallery', 'partials/pagination' ); ?>
		<?php endif; ?>
	</div>
</section>