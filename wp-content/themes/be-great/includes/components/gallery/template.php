<?php

/**
 * Gallery Component - Template
 *
 * @package     Titan
 * @subpackage  Component/Template
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

$spaces_qv     = get_query_var( 'spaces' );
$types_qv      = get_query_var( 'types' );

$is_landing     = ( $spaces_qv == null && $types_qv == null ) ? true : false;
$is_combo       = ( $spaces_qv != null && $types_qv != null ) ? true : false;
$is_type        = ( $spaces_qv == null && $types_qv != null ) ? true : false;
$is_space       = ( $spaces_qv != null && $types_qv == null ) ? true : false;


if( $is_landing ):
	$heading        = get_sub_field( 'page_title' );
	$text_layout    = get_sub_field( 'intro_text_display' );
	$text           = get_sub_field( 'intro_text' );

elseif( $is_combo ):
	$heading        = ucwords( $types_qv . ' ' . $spaces_qv );
	$text_layout    = 'standard';
	$text           = '';

elseif( $is_type ):
	$tax            = get_term_by( 'slug', $types_qv, 'types' );
	$heading        = $tax->name;
	$text_layout    = 'standard';
	$text           = '<p>' . $tax->description . '</p>';
	$hero           = get_field( 'hero_image', $tax->term_id );
elseif( $is_space ):
	$tax            = get_term_by( 'slug', $spaces_qv, 'spaces' );
	$heading        = $tax->name;
	$text_layout    = 'standard';
	$text           = '<p>' . $tax->description . '</p>';
	$hero           = get_field( 'hero_image', $tax->term_id );
?>

<section class="component page-intro">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-10 col-sm-offset-1">
				<?php if( $heading ): ?>
					<h1 class="h2 text-center"><?php echo $heading; ?></h1>
				<?php endif; ?>

				<?php if( $text_layout == 'dropcap' ):
					echo str_replace( '<p>', '<p class="lead dropcap">', $text );
				else:
					echo '<div class="text-center">' . $text . '</div>';
				endif; ?>

			</div>
		</div>
	</div>
</section>
<?php endif; ?>

<section <?php titan_component_class(); ?> <?php titan_component_style(); ?>>
	<div class="container">
		<h1>template.php of gallery</h1>
		<?php
			echo dsc_get_gallery_type_filter( $spaces_qv, $types_qv );
			echo dsc_get_gallery_spaces_filter( $spaces_qv, $types_qv );
		?>
	</div>
</section>