<?php
/**
 * Blog Component - Template Partial - Content
 *
 * @package     Titan
 * @subpackage  Component/Template/Partial
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */
?>

<div class="col-xs-6 col-sm-6 col-md-3 single-gallery-image">
	<article <?php post_class(); ?>>
		<?php $img = get_field( 'lg_image' ); ?>
		<a class="dsc-gallery-image" href="<?php echo $img['sizes']['large']; ?>">
            <?php echo $img['id']; ?>
			<?php echo wp_get_attachment_image( $img['id'], 'gallery_thumb', FALSE, array( 'class' => 'img-responsive' ) ); ?>
		</a>
	</article>
</div>