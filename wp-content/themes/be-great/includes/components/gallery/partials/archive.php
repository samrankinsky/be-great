<?php
/**
 * Gallery Archive - Template Partial
 *
 * @package     Titan
 * @subpackage  Component/Template/Partial
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

$types = get_terms( array(
	'taxonomy' => 'types',
	'hide_empty' => false,
	'order_by' => 'name',
) );

$spaces = get_terms( array(
	'taxonomy' => 'spaces',
	'hide_empty' => false,
	'order_by' => 'name',
) );


// combine spaces and types
$terms = array_merge( $types, $spaces );


/**
 * Helper function to compare terms alphabetically
 *
 * @param $a
 * @param $b
 *
 * @return int
 */
function dsc_compare_terms( $a, $b ){
	return strcmp( $a->name, $b->name );
}

// sort alphabetically
usort( $terms, 'dsc_compare_terms' );
//var_dump( $terms );

if ( !$terms ){ ?>

	<p><?php _e('Sorry, no galleries found.', 'dsc' ); ?></p>

<?php } else {

	$gallery_id = dsc_get_gallery_page_id();
	$page = get_permalink(  $gallery_id );
	foreach ( $terms as $term ){ ?>
		<?php
		$hero = get_field( 'hero_image', $term->taxonomy.'_' . $term->term_id );
		$term_url = $page . '?' . $term->taxonomy . '=' . $term->slug;
		$feat_img = wp_get_attachment_image( $hero[ 'id' ], 'gallery_thumb', false, array( 'class' => 'img-responsive' ) );
		if ( ! $feat_img ){
			$feat_img = '<img src="' . get_template_directory_uri() . '/assets/img/dsc-placeholder.jpg">';
		}
		?>
		<div class="col-xs-6 col-sm-4 col-md-3">
			<article class="archive-image">
				<a class="img" href="<?php echo $term_url; ?>">
					<?php echo $feat_img ?>
				</a>
				<h6><a href="<?php echo $term_url; ?>"><?php echo $term->name; ?></a></h6>
			</article>
		</div>
	<?php
	}

}