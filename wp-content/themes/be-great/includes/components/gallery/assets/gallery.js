( function( $ ) {
    'use strict';
    $(document).ready(function() {

        var $select = $('.gallery-filter');
        $select.select2({
            minimumResultsForSearch: Infinity // hide the search box
        });

        $select.on( 'select2:select', function(e) {
            window.location = $(this).val();
        });
        
    });

	$('.single-gallery-image').magnificPopup({
		delegate : 'a',
		type     : 'image',
		tLoading : 'Loading image #%curr%...',
		mainClass: 'mfp-img-mobile',
		gallery  : {
			enabled           : true,
			navigateByImgClick: true,
			preload           : [0, 1] // Will preload 0 - before current, and 1 after the current image
		},
		image    : {
			tError  : '<a href="%url%">The image #%curr%</a> could not be loaded.'
		}
	});


	var magnificPopup = $.magnificPopup.instance;

	/* Loads the function when clicking the lightbox (otherwise it does not take the used class) */
	$("a.dsc-gallery-image").click(function(e) {

		/* Expect to load lightbox */
		setTimeout(function() {
			/* Swipe to the left - Next */
			$(".mfp-container").swipe( {
				swipeLeft:function(event, direction, distance, duration, fingerCount) {
					console.log("swipe right");
					magnificPopup.next();
				},

				/* Swipe to the right - Previous */
				swipeRight:function(event, direction, distance, duration, fingerCount) {
					console.log("swipe left");
					magnificPopup.prev();
				},
			});
		}, 100);
	});


}(jQuery) );