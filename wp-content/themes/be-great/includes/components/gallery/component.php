<?php
/**
 * Gallery Component
 *
 * @package     Titan
 * @subpackage  Component
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

# Remove component layout options for this component
add_filter( "titan/component/{$component->name}/layout", 'titan_component_remove_layout_support' );


# Register Testimonials Custom Post Type
add_action( 'titan/loaded', 'titan_gallery_register' );

/**
 * Register Gallery Post Type + Options
 *
 * @since   1.0.0
 *
 * @return  void
 */
function titan_gallery_register() {
	// Register Post Type
	$galleries = new CPT( array(
		'post_type_name' => 'dscphoto',
		'singular'       => 'Gallery Photo',
		'plural'         => 'Gallery Photos',
		'slug'           => 'gallery',
	), array(
		'supports'      => array( 'title', 'thumbnail' ),
		'taxonomies' => array( 'types', 'spaces' ),
		'menu_icon'     => 'dashicons-images-alt2',
		'hierarchical'  => true,
		'rewrite'       => array( 'slug' => 'gallery', 'with_front' => true ),
		'has_archive'   => true
	) );

	// Register Taxonomy
	$galleries->register_taxonomy( array(
		'taxonomy_name' => 'types',
		'singular'      => 'Project Type',
		'plural'        => 'Project Types'
	), array(
		'public' => true,
		'show_in_nav_menus' => false,
		'show_ui' => true,
		'show_tagcloud' => false,
		'hierarchical' => false,
		'rewrite' => true,
		'query_var' => true
	) );

	// Register Taxonomy
	$galleries->register_taxonomy( array(
		'taxonomy_name' => 'spaces',
		'singular'      => 'Space',
		'plural'        => 'Spaces'
	), array(
		'public' => true,
		'show_in_nav_menus' => false,
		'show_ui' => true,
		'show_tagcloud' => false,
		'hierarchical' => false,
		'rewrite' => true,
		'query_var' => true
	) );

	// Set Component - This will set the path for all the templates including:
	// 'archive.php' 'taxonomy.php' 'single.php'
	$galleries->set_component( 'gallery' );
}

if ( ! function_exists( 'dsc_get_gallery_type_filter' ) ) {

	function dsc_get_gallery_type_filter( $spaces_qv, $types_qv ) {

		$types_select  = '';
		$types         = get_terms( array(
				'taxonomy'   => 'types',
				'hide_empty' => false
			)
		);

		/* Property Types Select */
		if ( $spaces_qv != null ) {
			$typesq   = '/gallery/?spaces=' . $spaces_qv . '&types=';
			$typesall = '/gallery/?spaces=' . $spaces_qv;
		} else {
			$typesq   = '/gallery/?types=';
			$typesall = '/gallery/?';
		}

		$types_select .= '<select id="filter-property-types" class="gallery-filter">';
		$types_select .= '<option value="' . $typesall . '">All Project Types</option>';

		foreach ( $types as $type ) {
			$types_select .= '<option';
			if ( $type->slug == $types_qv ) {
				$types_select .= ' selected="selected"';
			}
			$types_select .= ' value="' . $typesq . $type->slug . '">' . $type->name . '</option>';
		}

		$types_select .= '</select>';

		return $types_select;
	}
}

if ( ! function_exists( 'dsc_get_gallery_spaces_filter' ) ) {

	function dsc_get_gallery_spaces_filter( $spaces_qv, $types_qv ) {

		$spaces_select = '';
		$spaces        = get_terms( array(
				'taxonomy'   => 'spaces',
				'hide_empty' => false
			)
		);

		/* Spaces Select */
		if ($types_qv != null){
			$spacesq = '/gallery/?types='.$types_qv.'&spaces=';
			$spaceall = '/gallery/?types='.$types_qv;
		} else {
			$spacesq = '/gallery/?spaces=';
			$spaceall = '/gallery/?';
		}

		$spaces_select .= '<select id="filter-spaces" class="gallery-filter">';
		$spaces_select .= '<option value="' . $spaceall . '">All Spaces</option>';

		foreach ($spaces as $space) {
			$spaces_select .= '<option';

			if ( $space->slug == $spaces_qv ){
				$spaces_select .= ' selected="selected"';
			}
			$spaces_select .= ' value="' . $spacesq . $space->slug.'">'.$space->name.'</option>';
		}

		$spaces_select .= '</select>';


		return $spaces_select;

	}
}



// Add post states for CPT list pages
// 1.0.0
// -----------------------------------------------------------------------------
if ( ! function_exists( 'dscadd_cpt_post_states' ) ) {
	function dsc_add_cpt_post_states( $post_states, $post ) {

		// Modify Page
		if ( ( $events_page_id = dsc_get_gallery_page_id() ) && $post->ID == $events_page_id ) {
			$post_states[ 'page_for_events' ] = __( 'Gallery Page' );
		}

		return $post_states;
	}

	add_filter( 'display_post_states', 'dsc_add_cpt_post_states', 10, 2 );
}


// Get the gallery page ID set in ACF options
// 1.0.0
// -----------------------------------------------------------------------------
if ( ! function_exists( 'dsc_get_gallery_page_id' ) ) {
	function dsc_get_gallery_page_id() {

		// Check ACF, Get Events Page, Check if valid
		if ( ! function_exists( 'get_field' ) || ! ( $gallery_page = get_field( 'gallery_page', 'options' ) ) ) {
			return false;
		}

		// Return Events Page Id
		return $gallery_page->ID;
	}
}


function dsc_add_gallery_meta_box(){
	$gallery_id = dsc_get_gallery_page_id();
	global $post;
	if ( $gallery_id == $post->ID ){
		add_meta_box( 'dsc_gallery_meta', "Gallery Instructions", 'dsc_gallery_meta_instruction', 'page', 'side', 'high' );
	}
}
add_action( 'add_meta_boxes', 'dsc_add_gallery_meta_box' );
function dsc_gallery_meta_instruction(){
	?>
	<p>This page has been selected as the gallery page in the <a href="<?php echo admin_url('/admin.php?page=titan-general-options'); ?>">theme options</a>. After the builder, the gallery filters and images will automatically display.</p>
	<p>If you want to edit gallery images, you can do that <a href="<?php echo admin_url('/edit.php?post_type=dscphoto') ; ?>">here</a>.</p>
	<p>If you need to edit the featured image or headline for a gallery type or space, you can do that in the <a href="<?php echo admin_url('/edit-tags.php?taxonomy=types&post_type=dscphoto') ; ?>">Project Types</a> and <a href="<?php echo admin_url('/edit-tags.php?taxonomy=spaces&post_type=dscphoto') ; ?>">Spaces</a> area.</p>
	<?php
}