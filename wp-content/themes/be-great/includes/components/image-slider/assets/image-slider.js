( function( $ ) {
	'use strict';
	$(document).ready(function() {
		var owl = $('.dsc-image-carousel');
		owl.owlCarousel({
            animateOut: 'fadeOut',
			loop           : true,
			responsiveClass: true,
			items          : 1, // slides to show
			autoplay       : false,
			autoplayTimeout: 5000, // timeout between slides
			smartSpeed     : 1000, // transition speed
			nav            : true, // prev/next navigation
			navText		   : ['<span class="simple-icon-arrow-left"></span>', '<span class="simple-icon-arrow-right"></span>'],
			dots           : true // dot navigation
		});

	})
}(jQuery) );