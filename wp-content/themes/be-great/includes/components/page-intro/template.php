<?php
/**
 * Page Intro Component - Template
 *
 * @package     Titan
 * @subpackage  Component/Template
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

$heading        = get_sub_field( 'page_title' );
$text_layout    = get_sub_field( 'intro_text_display' );
$text           = get_sub_field( 'intro_text' );
?>

<section <?php titan_component_id(); ?> <?php titan_component_class(); ?> <?php titan_component_style(); ?>>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-10 col-sm-offset-1">
				<?php if( $heading ): ?>
					<h1 class="heading text-center"><?php echo $heading; ?></h1>
				<?php endif; ?>

				<?php if( $text_layout == 'dropcap' ):
					echo str_replace( '<p>', '<p class="lead dropcap">', $text );
				else:
					echo '<div class="text-center">' . $text . '</div>';
				endif; ?>
				
			</div>
		</div>
	</div>
</section>