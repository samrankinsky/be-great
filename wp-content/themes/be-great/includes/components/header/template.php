<?php
/**
 * Header Component - Template
 *
 * @package     Titan
 * @subpackage  Component/Template
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

$logo               = get_field( 'logo', 'options' );
$alt_logo           = get_field( 'alternate_logo', 'options' );
$post_id            = get_the_ID();
$meta_navbar_color  = get_post_meta( $post_id, '_header_color_scheme', true );
$navbar_color       = $meta_navbar_color ? $meta_navbar_color : 'navbar-default';
$header_class       = ( $alt_logo ) ? array( 'header-light', 'has-alt-logo' ) : 'header-light';

?>
<header <?php titan_component_class( $header_class ); ?> <?php titan_component_style(); ?>>
    <div class="navbar-bg"></div>
    <nav class="main-nav navbar <?php echo $navbar_color; ?> navbar-static-top ">
		<div class="container-fluid">
			<div class="flex">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
						<span class="sr-only"><?php _e( 'Toggle Navigation', 'titan' ); ?></span>
						<span class="icon-wrap"><span class="icon-bar top-bar"></span><span class="icon-bar middle-bar"></span><span class="icon-bar bottom-bar"></span></span>
					</button>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
						<?php
						if( $logo && isset( $logo['url'] ) ): ?>
							<img src="<?php echo $logo['url']; ?>" class="brand-img img-responsive site-logo" alt="<?php bloginfo( 'name' ); ?>">
						<?php else:
							bloginfo( 'name' );
						endif; ?>
						<?php
						if( $alt_logo && isset( $alt_logo['url'] ) ): ?>
							<img src="<?php echo $alt_logo['url']; ?>" class="brand-img img-responsive alt-logo" alt="<?php $alt_logo['alt']; ?>">
						<?php endif; ?>
					</a>
				</div>
				<div id="navbar-collapse" class="nav-primary navbar-collapse collapse slide-left">
					<?php
					if ( has_nav_menu( 'primary_navigation' ) ) {
						wp_nav_menu( array(
							'theme_location' => 'primary_navigation',
							'menu_class'     => 'nav navbar-nav',
							'walker'         => new DSC_Nav_Walker()
						) );
					}
					?>
				</div>
				<div class="nav-utility navbar-collapse collapse">
					<?php
					if ( has_nav_menu( 'utility_navigation' ) ) {
						wp_nav_menu( array(
							'theme_location' => 'utility_navigation',
							'menu_class'     => 'nav navbar-nav navbar-right'
						) );
					}
					?>
				</div>
			</div>
		</div>
	</nav>
</header>
