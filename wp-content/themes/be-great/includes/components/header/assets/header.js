( function( $ ) {
    'use strict';
    $(document).ready(function() {

        /* Main Menu Desktop Hover Intent & Slide */
        // mouseenter
        function showMenuSlide(){

            if($('.navbar-toggle').css('display') === 'none') {
                $(this).siblings().children('.dropdown-menu').hide();
                var dropdownMenu = $(this).find('.dropdown-menu').first();
                dropdownMenu.slideDown(300);
            }
        }
        // mouseleave
        function hideMenuSlide() {
            if($('.navbar-toggle').css('display') === 'none') {
                var dropdownMenu = $(this).find('.dropdown-menu').first();
                dropdownMenu.slideUp(300);
            }
        }

        // Main nav hover intent & fade in/out
        // borrowed from: http://jsfiddle.net/g9JJk/9/, http://www.reddit.com/r/webdev/comments/2893vx/bootstrap_3_dropdown_with_hover_using_hoverintent/
        var config = {
            timeout: 400,
            over: showMenuSlide,
            out: hideMenuSlide
        };

        $('#menu-main-menu .dropdown').hoverIntent(config);

        $('#menu-main-menu a.dropdown-toggle').click(function(e) {
            if( $(this).attr('href') === "#") {
                e.preventDefault();
            }
        });

        // Optional: Make the first link a working link!
        // - expected behaviour on hover menus.
        $('#menu-main-menu .dropdown').on('show.bs.dropdown', function () {
            if($('.navbar-toggle').css('display') === 'none' ) {
                url = $(this).attr('href');

                if( url != '#' ) {
                    window.location.href = url;
                }
                return false;
            }
        });

        // Changing background on header when mobile menu is open
        // Changing background on header when mobile menu is open
        $('.header #navbar-collapse').on('show.bs.collapse', function () {
            $('.header').removeClass('mobile-menu-closed');
            $('.header').addClass('mobile-menu-open');

            $('body').removeClass('mobile-menu-closed');
            $('body').addClass('mobile-menu-open');
        });

        $('.header #navbar-collapse').on('hide.bs.collapse', function () {
            $('.header').removeClass('mobile-menu-open');
            $('.header').addClass('mobile-menu-closed');

            $('body').removeClass('mobile-menu-open');
            $('body').addClass('mobile-menu-closed');
        });

    });

	// ===== Scroll to Top ====
	$(window).scroll(function() {
		if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
			$('#return-to-top').fadeIn(200);    // Fade in the arrow
		} else {
			$('#return-to-top').fadeOut(200);   // Else fade out the arrow
		}
	});
	$('#return-to-top').click(function() {      // When arrow is clicked
		$('body,html').animate({
			scrollTop : 0                       // Scroll to top of body
		}, 500);
	});

}(jQuery) );