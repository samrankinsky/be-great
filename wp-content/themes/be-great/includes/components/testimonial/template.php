<?php
/**
 * Testimonial Component - Template
 *
 * @package     Titan
 * @subpackage  Component/Template
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

$testimonial = get_sub_field( 'testimonial' );
$author      = get_sub_field( 'testimonial_author' );
?>
<section <?php titan_component_id(); ?> <?php titan_component_class(); ?><?php titan_component_style(); ?>>
	<div class="container">
		<div class="testimonial-wrapper">
			<div class="testimonial-content">
				<?php if ( $testimonial ) { ?>
					<blockquote><?php echo $testimonial; ?></blockquote>
				<?php } ?>
				<?php if ( $author ) { ?>
					<footer><?php echo $author; ?></footer>
				<?php } ?>
			</div>
		</div>
	</div>
</section>