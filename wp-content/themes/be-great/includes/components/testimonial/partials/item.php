<?php
/**
 * Testimonial Component - Item - Partial
 *
 * @package     Titan
 * @subpackage  Component/Testimonial/Partial
 * @version     1.0.0
 */

// Vars
$testimonial = get_sub_field( 'testimonial' );
$author      = get_sub_field( 'author' );
?>
<div class="testimonial-item">
	<?php if ( $testimonial ) { ?>
		<blockquote><?php echo $testimonial; ?></blockquote>
	<?php } ?>
	<?php if ( $author ) { ?>
		<footer>-<?php echo $author; ?></footer>
	<?php } ?>
</div>