<?php
/**
 * Hero Image Component - Template
 *
 * @package     Titan
 * @subpackage  Component/Template
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

$image = get_sub_field( 'image' );
$badge = get_sub_field( 'badge_image' );

?>
<section <?php titan_component_id(); ?> <?php titan_component_class(); ?> <?php titan_component_style(); ?>>
	<?php
	if ( $image ) {
		echo wp_get_attachment_image( $image[ 'id' ], 'full', false, array( 'class' => 'hero-image' ) );
	}
	?>
	<?php
	if ( $badge ) {
		echo wp_get_attachment_image( $badge[ 'id' ], 'full', false, array( 'class' => 'hero-badge img-responsive' ) );
	}
	?>
</section>