<?php
/**
 * Content 1 Column w Background Component
 *
 * @package     Titan
 * @subpackage  Component
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

# Remove component layout options for this component
add_filter( "titan/component/{$component->name}/layout", 'titan_component_remove_layout_support' );
