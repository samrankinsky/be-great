<?php
/**
 * Content 1 Column w Background Component - Template
 *
 * @package     Titan
 * @subpackage  Component/Template
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

$image      = get_sub_field( 'background' );
$heading    = get_sub_field( 'heading' );
$text       = get_sub_field( 'text' );
$btn_text   = get_sub_field( 'button_text' );
$btn_link   = get_sub_field( 'button_url' );
$layout     = get_sub_field( 'layout' );
$push       = ( $layout == 'right' ) ? ' col-md-push-7' : '';
?>

<section <?php titan_component_class(); ?> <?php titan_component_style(); ?>>
	<div class="component-bg" style="background-image: url( '<?php echo $image['url']; ?>');">
		<div class="container">
			<div class="row-flex">
				<div class="col-xs-12 col-sm-6 col-md-5<?php echo $push; ?>">
					<div class="content-wrapper">
						<div class="content-inner">
							<?php if( $heading ): ?>
								<h2 class="subheading text-center"><?php echo $heading; ?></h2>
							<?php endif; ?>

							<?php echo $text ?>

							<?php if( $btn_text && $btn_link ): ?>
								<a class="btn btn-primary" href="<?php echo $btn_link; ?>"><?php echo $btn_text; ?></a>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>