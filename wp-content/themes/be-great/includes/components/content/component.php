<?php
/**
 * Content Component
 *
 * @package     Titan
 * @subpackage  Component
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

# Repeater Layouts
add_filter( "titan/component/{$component->name}/repeater/layouts", 'titan_content_repeater_layouts' );

/**
 * Update Conent Component Repeater Layouts
 *
 * @since   1.0.0
 *
 * @param   array $layouts Default layouts
 *
 * @return  array $layouts Adjusted layouts
 */
function titan_content_repeater_layouts( $layouts ) {

	// Remove 4 column
	unset( $layouts[ '4_col' ] );

	$layouts[ '1_col_indented' ] = array(
		'name' => __( 'One Column Indented' ),
		'classes' => array(
			'col-xs-12 col-md-10 col-md-offset-1'
		)
	);

	$layouts[ '1_col_narrow' ] = array(
		'name' => __( 'One Column Narrow' ),
		'classes' => array(
			'col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2'
		)
	);

	$layouts[ '2_col_narrow' ] = array(
		'name' => __( 'Two Column Narrow' ),
		'classes' => array(
			'col-xs-12 col-sm-6 col-md-4 col-md-offset-2',
			'col-xs-12 col-sm-6 col-md-4'
		)
	);

	// Return
	return $layouts;
}