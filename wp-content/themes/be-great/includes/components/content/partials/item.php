<?php
/**
 * Content Component - Item - Partial
 *
 * @package     Titan
 * @subpackage  Component/Content/Partial
 * @version     1.0.0
 */

// Vars
if ( ! $content = get_sub_field( 'content' ) ) {
	return;
}
?>
<div class="content-item">
	<?php echo $content; ?>
</div>