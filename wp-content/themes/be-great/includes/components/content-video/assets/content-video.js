( function( $ ) {
    'use strict';
    $(document).ready(function() {

        // Change the URLs for the videos so they don't autoplay or show related videos
        var magnific = $('.magnific-video');
        magnific.magnificPopup({
            mainClass: 'mfp-fade',
            removalDelay: 160,
            preloader: false,
            fixedContentPos: false,
            type: 'iframe',
            iframe: {
                patterns: {
                    youtube: {
                        index: 'youtube.com/',
                        id: 'v=',
                        src: 'http://www.youtube.com/embed/%id%?rel=0&autoplay=1'
                    },
                    youtubeshare: {
                        index: 'youtu.be/',
                        id: '/',
                        src: 'http://www.youtube.com/embed/%id%?rel=0&autoplay=1'
                    },
                    vimeo: {
                        index: 'vimeo.com/',
                        id: '/',
                        src: '//player.vimeo.com/video/%id%?autoplay=1'
                    }
                }
            }
        });


    });
}(jQuery) );