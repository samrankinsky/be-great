<?php
/**
 * Content Video Component - Template
 *
 * @package     Titan
 * @subpackage  Component/Template
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */


$heading    = get_sub_field( 'heading' );
$video      = get_sub_field( 'video' );
$image      = get_sub_field( 'image' );
$text       = get_sub_field( 'text' );
?>

<section <?php titan_component_id(); ?> <?php titan_component_class(); ?> <?php titan_component_style(); ?>>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
			<?php

			if( $heading ):
				echo '<h4 class="text-center">' . $heading . '</h4>';
			endif;
			?>
				<a href="<?php echo $video; ?>" class="magnific-video" style="background-image:url(<?php echo $image['url']; ?>);">


                    <svg class="play-button" width="85px" height="85px" viewBox="0 0 85 85" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <defs>
                            <circle id="path-1" cx="40.5" cy="40.5" r="40.5"></circle>
                        </defs>
                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <g id="Case-study" transform="translate(-608.000000, -2243.000000)">
                                <g id="3" transform="translate(81.000000, 1529.000000)">
                                    <g id="V" transform="translate(195.000000, 541.000000)">
                                        <g id="Play" transform="translate(334.000000, 175.000000)">
                                            <g id="Oval-2">
                                                <use fill="#FFFFFF" fill-rule="evenodd" xlink:href="#path-1"></use>
                                                <circle stroke="#D1B275" stroke-width="2" cx="40.5" cy="40.5" r="41.5"></circle>
                                            </g>
                                            <polygon id="Triangle" fill="#D1B275" points="63.4601313 41.0698874 29.2162289 58.1918386 29.2162289 23.9479362"></polygon>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </svg>

				</a>

			<?php echo $text; ?>
			</div>
		</div>
	</div>
</section>
