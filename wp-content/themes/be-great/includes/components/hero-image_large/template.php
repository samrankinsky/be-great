<?php
/**
 * Hero Image Large Component - Template
 *
 * @package     Titan
 * @subpackage  Component/Template
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

$heading    = get_sub_field( 'heading' );
$text       = get_sub_field( 'text' );
$image      = get_sub_field( 'image' );
$valign     = get_sub_field( 'vertical_alignment' );
$halign     = get_sub_field( 'horizontal_alignment' );

?>

<section <?php titan_component_class(); ?> <?php titan_component_style(); ?>>
	<div class="hero-image-bg" style="background-image: url('<?php echo $image['url'];?>'); background-position: <?php echo $valign . ' ' . $halign; ?>;">
		<div class="hero-image-caption text-center">
			<?php if( $heading ):
				echo '<h1>' . $heading . '</h1>';
			endif; ?>

			<div class="hero-image-text">
				<?php echo $text; ?>
			</div>
		</div>
		<div class="hero-jump-link"><a href="#dsc-learn-more"><span class="simple-icon-arrow-down"></span></a></div>
	</div>
</section>