<?php
/**
 * Content 2 Column w Image Component - Template
 *
 * @package     Titan
 * @subpackage  Component/Template
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */
// 54.1666667%
$layout     = get_sub_field( 'layout' );
$image      = get_sub_field( 'image' );
$text       = get_sub_field( 'text' );
$text_col   = ( $layout == 'image_left' ) ? ' col-md-5 col-md-push-7' : ' col-md-6';
$image_col  = ( $layout == 'image_left' ) ? ' col-md-7 col-md-pull-5' : ' col-md-5 col-md-offset-1';
?>

<section <?php titan_component_id(); ?> <?php titan_component_class(); ?> <?php titan_component_style(); ?>>
	<div class="container">
		<div class="row-flex">
			<div class="col-xs-12<?php echo $text_col; ?> content-col">
				<div class="flex-inner">
					<?php echo $text; ?>
				</div>
			</div>
			<div class="col-xs-12<?php echo $image_col; ?> image-col">
				<div class="flex-inner">
					<?php echo $image; ?>
				</div>
			</div>
		</div>
	</div>
</section>