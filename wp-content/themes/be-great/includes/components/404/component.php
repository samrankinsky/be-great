<?php
/**
 * 404 Template Component
 *
 * @package     Titan
 * @subpackage  Component
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

# Override the 404 Template Path
add_filter( 'titan/template/404/path', 'titan_404_component_path' );

/**
 * 404 Path
 *
 * @since   1.0.0
 * @access  public
 * @return  string
 */
function titan_404_component_path() {
	return trailingslashit( TITAN_COMPONENTS . '404' );
}
