<?php
/**
 * 404 Template File
 *
 * @package Titan
 * @since   1.0.0
 */
?>

<div class="component component-404">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<h1><?php titan_the_title(); ?></h1>
				<div class="alert alert-warning">
					<?php _e( 'Sorry, but the page you were trying to view does not exist.', 'titan' ); ?>
				</div>
				<?php get_search_form(); ?>
			</div>
		</div>
	</div>
</div>
