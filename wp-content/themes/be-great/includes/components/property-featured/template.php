<?php
/**
 * Featured Properties Component - Template
 *
 * @package     Titan
 * @subpackage  Component/Template
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

// Global
global $post;

// Variables
$layout     = get_sub_field( 'layout' );
$heading    = get_sub_field( 'heading' );
$properties = '';

$classes    = ( $layout == 'small' ) ? 'col-sm-4' : 'col-sm-6';
$thumb_size = 'list_property_' . $layout;

$btn_text   = get_sub_field( 'button_text' );
$btn_link   = get_sub_field( 'button_url' );

$idx        = 1;

if( $layout == 'small' ) {
	$properties = get_sub_field( 'properties_small' );
} else if( $layout == 'large_4' ) {
	$properties = get_sub_field( 'properties_large_4' );
} else if( $layout == 'large_2' ) {
	$properties = get_sub_field( 'properties_large_2' );
}

$prop_count = count( $properties );

// Check
if ( empty( $properties ) ) return;
?>
<section <?php titan_component_id(); ?> <?php titan_component_class(); ?><?php titan_component_style(); ?>>
	<div class="container">
		<?php if( $heading ): ?>
		<div class="row">
			<div class="col-xs-12">
				<h2 class="subheading text-center"><?php echo $heading; ?></h2>
			</div>
		</div>
		<?php endif; ?>
		<div class="row-flex">
			<?php foreach ( $properties as $post ) { setup_postdata( $post );
				$thumb_url          = get_the_post_thumbnail_url( $post, $thumb_size );
				$center_last_odd    = ( $layout == 'large_4' && $prop_count < 4 && $idx == $prop_count ) ? ' center-last-odd' : '';
				?>
				<div class="property<?php echo $center_last_odd; ?> prop-<?php echo $layout; ?>">
					<div class="property-image-effect">
						<figure>
							<a href="<?php the_permalink(); ?>">
								<?php
								if ( $layout == 'small' ){
									$thumbSize = 'list_property_small';
								} else {
									$thumbSize = 'list_property_large';
								}
								?>
								<?php the_post_thumbnail( $thumbSize, array( 'class' => 'img-responsive' ) ); ?>
							</a>
						</figure>
					</div>
					<?php /*<div class="property-image" style="background-image: url('<?php the_post_thumbnail_url( $thumb_size ) ?>');">
						<a href="<?php the_permalink(); ?>" class="border-wrapper">
							<?php the_post_thumbnail( $thumb_size, array( 'class' => 'img-responsive' ) ); ?>
						</a>
						<a href="<?php the_permalink(); ?>" class="image-border"></a>
					</div>*/ ?>
					<p class="text-center"><a href="<?php the_permalink(); ?>" class="btn btn-default"><?php the_title(); ?></a></p>
				</div>
			<?php $idx++; } wp_reset_postdata(); ?>
		</div>
		<?php if( $btn_text && $btn_link ): ?>
		<p class="text-center"><a href="<?php echo $btn_link; ?>" class="btn btn-primary"><?php echo $btn_text; ?></a></p>
		<?php endif; ?>
	</div>
</section>