<?php
/**
 * Blog Component - Single Template
 *
 * @package     Titan
 * @subpackage  Component/Template
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */
the_post();

$news_layout = get_field('news_layout');

if (!$news_layout) {
	$news_layout = 'vertical';
}

?>
<section class="component blog-component blog-single">
	<div class="container">
		<div class="row">
			<div class="col-xs-10 col-xs-offset-1 index">
				<?php if ($news_layout == 'vertical') {
					titan_component_part( 'blog', 'partials/content', 'single' );
				} else if ($news_layout == 'horizontal') {
					titan_component_part( 'blog', 'partials/content', 'single-horizontal' );
                } else if ($news_layout == 'no_featured') {
					titan_component_part( 'blog', 'partials/content', 'single-no-featured' );
                } ?>
			</div>
		</div>
	</div>
</section>

<?php

/// Include blog post builder
titan_builder( 'builder-news-single', 19 );

?>