<?php
/**
 * Created by PhpStorm.
 * User: Dennis
 * Date: 5/30/17
 * Time: 4:57 PM
 */


if ( is_category() ) {
	$categories = get_the_category();
	$current_category = $categories[0]->slug;
}
?>

<section class="component news-filters">
    <div class="container">
        <div class="news-filter-wrap">
            <span class="filter-icon">
                <svg width="33px" height="29px" viewBox="0 0 33 29" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <defs></defs>
                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <g id="Gallery" transform="translate(-133.000000, -456.000000)" stroke-width="2">
                            <g id="filter" transform="translate(134.000000, 446.000000)">
                                <g id="icon" transform="translate(0.000000, 11.000000)">
                                    <path d="M0.5,4 L30.0169443,4" id="Line" stroke="#00374D" stroke-linecap="square"></path>
                                    <path d="M0.5,23 L30.0169443,23" id="Line-Copy-3" stroke="#00374D" stroke-linecap="square"></path>
                                    <path d="M0.5,13 L30.0169443,13" id="Line-Copy-2" stroke="#00374D" stroke-linecap="square"></path>
                                    <circle class="filter-first-circle" id="Oval" stroke="#D1B275" fill="#FFFFFF" cx="22" cy="4" r="4"></circle>
                                    <circle class="filter-second-circle" id="Oval-Copy" stroke="#D1B275" fill="#FFFFFF" cx="9" cy="13" r="4"></circle>
                                    <circle class="filter-third-circle" id="Oval-Copy-2" stroke="#D1B275" fill="#FFFFFF" cx="22" cy="23" r="4"></circle>
                                </g>
                            </g>
                        </g>
                    </g>
                </svg>
            </span>
            <span class="filter-label"><?php _e( 'Filter by', 'dsc' ); ?></span>
			<?php echo dsc_get_category_filter( $current_category ); ?>
			<a href="/news/" class="clear-filters <?php echo $clear_class; ?>"><?php _e( 'x clear filters', 'dsc' ); ?></a>
		</div>
	</div>
</section>