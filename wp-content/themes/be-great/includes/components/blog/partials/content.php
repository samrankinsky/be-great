<?php
/**
 * Blog Component - Template Partial - Content
 *
 * @package     Titan
 * @subpackage  Component/Template/Partial
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */


$featured_image = get_the_post_thumbnail_url($post, 'full');

?>

<article <?php post_class(); ?>>

    <?php if ( $featured_image ) { ?>
        <a href="<?php the_permalink(); ?>" class="post-featured" style="background-image: url(<?php echo $featured_image; ?>);"></a>
	<?php } else { ?>
        <a href="<?php the_permalink(); ?>" class="post-featured" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/dsc-placeholder.jpg);"></a>
	<?php } ?>

    <div class="entry-summary">
        <h3 class="title"><?php the_title(); ?></h3>
        <?php the_excerpt(); ?>
    </div>
</article>