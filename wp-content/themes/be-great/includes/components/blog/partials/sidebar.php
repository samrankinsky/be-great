<?php
/**
 * Blog Component - Template Partial - Sidebar
 *
 * @package     Titan
 * @subpackage  Component/Template/Partial
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

# Blog Sidebar
dynamic_sidebar( 'blog-sidebar' ); ?>