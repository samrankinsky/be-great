<?php
/**
 * Blog Component - Template Partial - Comments
 *
 * @package     Titan
 * @subpackage  Component/Template/Partial
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

// Password Protected
if ( post_password_required() ) return;
?>
<section id="comments" class="comments">

	<?php // Comments Closed ?>
	<?php if ( ! comments_open() && get_comments_number() != '0' && post_type_supports( get_post_type(), 'comments' ) ) { ?>
		<div class="alert alert-warning">
			<?php _e( 'Comments are closed.', 'titan' ); ?>
		</div>
	<?php } ?>

	<?php // Comments Open ?>
	<?php if ( have_comments() ) { ?>
		<h2>
			<?php
			printf(
				_nx( 'One response to &ldquo;%2$s&rdquo;', '%1$s responses to &ldquo;%2$s&rdquo;', get_comments_number(), 'comments title', 'titan' ),
				number_format_i18n( get_comments_number() ),
				'<span>' . get_the_title() . '</span>'
			);
			?>
		</h2>
		<ol class="comment-list">
			<?php wp_list_comments( [ 'style' => 'ol', 'short_ping' => true ] ); ?>
		</ol>
		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : ?>
			<nav>
				<ul class="pager">
					<?php if ( get_previous_comments_link() ) : ?>
						<li class="previous"><?php previous_comments_link( __( '&larr; Older comments', 'titan' ) ); ?></li>
					<?php endif; ?>
					<?php if ( get_next_comments_link() ) : ?>
						<li class="next"><?php next_comments_link( __( 'Newer comments &rarr;', 'titan' ) ); ?></li>
					<?php endif; ?>
				</ul>
			</nav>
		<?php endif; ?>
	<?php } ?>

	<?php // Comment Form ?>
	<?php comment_form(); ?>
</section>
