<?php
/**
 * Blog Component - Template Partial - Content Single
 *
 * @package     Titan
 * @subpackage  Component/Template/Partial
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

$share_label = 'Share this news';

?>
<article <?php post_class(); ?>>
	<header>
		<h1 class="entry-title text-center h2"><?php titan_the_title(); ?></h1>
		<?php titan_component_part( 'blog', 'partials/meta' ); ?>
        <div class="share"><?php echo $share_label; ?> <span class='st_sharethis_large' st_title='<?php the_title(); ?>' st_url='<?php echo get_permalink(); ?>'></span></div>
	</header>
	<div class="entry-content">
        <?php the_post_thumbnail('full', array('class' => 'featured-horizontal') ); ?>
		<?php the_content(); ?>
	</div>
    <div class="footer-share">
        <div class="share"><?php echo $share_label; ?> <span class='st_sharethis_large' st_title='<?php the_title(); ?>' st_url='<?php echo get_permalink(); ?>'></span></div>
        <a href="/news/">See All News &gt;</a>
    </div>
</article>
