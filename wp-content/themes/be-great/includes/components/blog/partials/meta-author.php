<?php
/**
 * Blog Component - Template Partial - Meta + Category - Default
 *
 * @package     Titan
 * @subpackage  Component/Template/Partial
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */
?>
<div class="meta meta-author">
	<time class="updated" datetime="<?php echo get_post_time( 'c', true ); ?>"><?php echo get_the_date( 'F j, Y'); ?></time>
	<span class="sep">&nbsp;|&nbsp;</span>
	<span class="byline author vcard">
		<?php _e( 'By', 'sage' ); ?>
		<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>" rel="author" class="fn"><?php echo get_the_author(); ?></a>
	</span>
	<span class="sep">&nbsp;|&nbsp;</span>
	<span class="categories"><?php echo get_the_category_list( __( ', ', 'titan' ) ); ?></span>
</div>
