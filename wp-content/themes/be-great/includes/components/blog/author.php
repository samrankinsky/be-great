<?php
/**
 * Blog Component - Author
 *
 * @package     Titan
 * @subpackage  Component/Template
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */
?>
<section class="component blog-component blog-archive blog-archive-author">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 title">
				<h1><?php titan_the_title(); ?></h1>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-8 index">
				<?php while ( have_posts() ) { the_post(); ?>
					<?php titan_component_part( 'blog', 'partials/content' ); ?>
				<?php } ?>
				<?php titan_component_part( 'blog', 'partials/pagination' ); ?>
			</div>
			<div class="col-xs-4 sidebar">
				<?php titan_component_part( 'blog', 'partials/sidebar' ); ?>
			</div>
		</div>
	</div>
</section>
