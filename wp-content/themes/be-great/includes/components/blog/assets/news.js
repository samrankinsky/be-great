( function( $ ) {
    'use strict';
    $(document).ready(function() {

        var $select = $('.news-filter');
        $select.select2({
            minimumResultsForSearch: Infinity // hide the search box
        });

        $select.on( 'select2:select', function(e) {
            window.location = $(this).val();
        });


        /// Add hover opacity to all un-hovered news articles on blog page
        /*var news_column = $(".news-grid .flex-col");

        news_column.mouseenter(function(){
            $('.flex-col').addClass('hover-fade');
            $(this).removeClass('hover-fade');
            console.log('hover it');
        });
        news_column.mouseleave(function(){
            $('.flex-col').removeClass('hover-fade');
            console.log('leave it');
        });*/
    });

}(jQuery) );
