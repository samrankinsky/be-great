<?php
/**
 * Blog Component - Category Template
 *
 * @package     Titan
 * @subpackage  Component/Template
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

?>
<section class="component blog-component blog-archive blog-archive-category">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 title">
                <h1 class="h2 text-center"><?php titan_the_title(); ?></h1>
            </div>
        </div>

	    <?php titan_component_part( 'blog', 'partials/news-filter' ); ?>

        <div class="row">
            <div class="col-xs-12 index">
                <div class="row-flex">
					<?php while ( have_posts() ) { the_post(); ?>
                        <div class="col-xs-12 col-sm-6 flex-col">
							<?php titan_component_part( 'blog', 'partials/content' ); ?>
                        </div>
					<?php } ?>
                </div>
				<?php titan_component_part( 'blog', 'partials/pagination' ); ?>
            </div>
        </div>
    </div>
</section>