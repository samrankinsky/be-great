<?php
/**
 * Blog Component - Index/Default Template
 *
 * @package     Titan
 * @subpackage  Component/Template
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */




?>
<section class="component blog-component blog-index">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 title intro-text">
				<h1 class="h2 text-center"><?php titan_the_title(); ?></h1>
			</div>
		</div>

        <div class="row">
            <div class="col-xs-12">
                <?php titan_builder( 'builder-news', 19 ); /// Include builder ?>
            </div>
        </div>

		<?php titan_component_part( 'blog', 'partials/news-filter' ); ?>

		<div class="row">
			<div class="col-xs-12 index news-grid">
                <div class="row-flex">
                    <?php while ( have_posts() ) { the_post(); ?>
                        <div class="col-xs-12 col-sm-6 flex-col">
                            <?php titan_component_part( 'blog', 'partials/content' ); ?>
                        </div>
                    <?php } ?>
                </div>
				<?php titan_component_part( 'blog', 'partials/pagination' ); ?>
			</div>
		</div>
	</div>
</section>
