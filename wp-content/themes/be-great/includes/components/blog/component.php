<?php
/**
 * Content Component
 *
 * @package     Titan
 * @subpackage  Component
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

# Filter all blog templates path.
add_filter( 'titan/template/index/path', 'titan_blog_component_path' );
add_filter( 'titan/template/archive/path', 'titan_blog_component_path' );
add_filter( 'titan/template/category/path', 'titan_blog_component_path' );
add_filter( 'titan/template/taxonomy/path', 'titan_blog_component_path' );
add_filter( 'titan/template/author/path', 'titan_blog_component_path' );
add_filter( 'titan/template/post/path', 'titan_blog_component_path' );

# Register Blog Sidebar
add_action( 'widgets_init', 'titan_blog_register_widgets' );

# Excerpt Length
add_filter( 'excerpt_length', 'titan_blog_excerpt_length' );

# Excerpt More
add_filter( 'excerpt_more', 'titan_blog_excerpt_more' );

/**
 * Blog Path
 *
 * @since   1.0.0
 * @access  public
 * @return  string
 */
function titan_blog_component_path() {
	return trailingslashit( TITAN_COMPONENTS . 'blog' );
}

/**
 * Register Blog Widgets
 *
 * @since   1.0.0
 * @access  public
 * @return  void
 */
function titan_blog_register_widgets() {
	register_sidebar( array(
		'name'          => __( 'Blog Widgets', 'titan' ),
		'id'            => 'blog-sidebar',
		'before_widget' => '<section class="widget %1$s %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>'
	) );
}

/**
 * Blog Excerpt Length
 *
 * @since   1.0.0
 * @access  public
 * @param   $length
 * @return  mixed
 */
function titan_blog_excerpt_length( $length ) {
	return 10;
}

/**
 * Blog Excerpt More
 *
 * @since   1.0.0
 * @param   string  $more
 * @return  string
 */
function titan_blog_excerpt_more( $more ) {
	return sprintf( '<p class="no-margin read-more"><a class="read-more btn btn-primary" href="%s">%s</a></p>', get_permalink(), __( 'Read More', 'titan' ) );
}




if ( ! function_exists( 'dsc_get_category_filter' ) ) {

	function dsc_get_category_filter( $current_category ) {

		$category_select  = '';
		$categories         = get_terms( 'category', array(
				'hide_empty' => true
			)
		);

		$category_select .= '<select id="filter-category" class="news-filter">';
		$category_select .= '<option value="/news/">Category</option>';

		foreach ( $categories as $category ) {
			$category_select .= '<option';
			if ( $category->slug == $current_category ) {
				$category_select .= ' selected="selected"';
			}
			$category_select .= ' value="/category/' . $category->slug . '/">' . $category->name . '</option>';
		}

		$category_select .= '</select>';

		return $category_select;
	}
}