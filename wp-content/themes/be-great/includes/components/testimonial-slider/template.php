<?php
/**
 * Testimonial Slider Component - Template
 *
 * @package     Titan
 * @subpackage  Component/Template
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

// Variables
$title = get_sub_field( 'title' );

// Check
if ( ! have_rows( 'testimonials' ) ) return;
?>
<section <?php titan_component_class(); ?><?php titan_component_style(); ?>>
	<div class="container">
		<div class="row">
			<?php if ( $title ) { ?>
				<h6><?php echo $title; ?></h6>
			<?php } ?>
			<div class="testimonials-slider">
				<?php while ( have_rows( 'testimonials' ) ) {
					the_row();
					// Variables
					$author      = get_sub_field( 'author' );
					$testimonial = get_sub_field( 'testimonial' );

					// Check
					if ( ! $testimonial ) {
						continue;
					}
					?>
					<div class="item">
						<?php if ( $testimonial ) { ?>
							<blockquote><?php echo $testimonial; ?></blockquote>
						<?php } ?>
						<?php if ( $author ) { ?>
							<footer>-<?php echo $author; ?></footer>
						<?php } ?>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
</section>