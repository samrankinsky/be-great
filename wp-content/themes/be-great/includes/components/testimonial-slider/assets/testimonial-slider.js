( function( $ ) {
	'use strict';
	$(document).ready(function() {

		var testimonials = $('.testimonials-slider');
		testimonials.owlCarousel({
			loop           : true,
			responsiveClass: true,
			items          : 1, // slides to show
			autoplay       : true,
			autoplayTimeout: 5000, // timeout between slides
			smartSpeed     : 1000, // transition speed
			nav            : false, // prev/next navigation
			dots           : false // dot navigation
		});

	})
}(jQuery) );