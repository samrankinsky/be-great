<?php
/**
 * Hero Image Small Component - Template
 *
 * @package     Titan
 * @subpackage  Component/Template
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

// Variables
$vertical_alignment = get_sub_field( 'va' );
?>
<section <?php titan_component_id(); ?> <?php titan_component_class( 'va-' . $vertical_alignment ); ?> <?php titan_component_style(); ?>>
	<?php
	$image = get_sub_field( 'image' );
	if ( $image ) {
		echo wp_get_attachment_image( $image[ 'id' ], 'full' );
	}
	?>
</section>