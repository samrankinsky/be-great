<?php
/**
 * Custom Builder Template File
 *
 * This custom template is used for builder modules.
 *
 * @package Titan_Theme
 * @since   1.0.0
 *
 * Template Name: Builder
 */

the_post();

//printf( '<h1 class="title">%s</h1>', titan_get_the_title() );

# Output Builder
titan_builder( 'hero-builder' );

# Output Builder
titan_builder( 'builder' );
