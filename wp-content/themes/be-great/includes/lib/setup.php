<?php
/**
 * Titan Theme Setup
 *
 * @package     Titan_Theme
 * @subpackage  Functions
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

/**
 * Titan Theme Setup
 *
 * This is basically like hooking into 'after_setup_theme'
 * minues some things happen before this hook is fired.
 *
 * Put all setup data in this function like you would
 * 'after_setup_theme'.
 *
 * We do multiple things below like registering
 * supported components and builders.
 *
 * @since  1.0.0
 * @access public
 * @return void
 */
if ( ! function_exists( 'titan_theme_setup' ) ) {
	function titan_theme_setup() {
		// Theme Components.
		add_theme_support( 'theme-components', array(
			'settings',
			'header',
			'footer',
			'form',
			'blog',
			'footer',
			'404',
			'search',
			'property',
			'gallery',
			'gallery-taxonomies'
		) );

		// Theme Builder
		add_theme_support( 'theme-builder', array(
			'hero-builder'    => array(
				'title'          => __( 'Hero', 'titan_theme' ),
				'components'     => array(
					'hero-image_large',
					'hero-image',
				),
				'locations'      => array(
					'page_template' => array(
						titan_path_to_page_template( 'builder.php' ),
						titan_path_to_page_template( 'builder-sidebar.php' ),
					),
				),
				'min'            => '',
				'max'            => 1,
				'button_c_label' => __( 'Add Hero', 'titan_theme' ),
			),
			'builder'         => array(
				'title'      => __( 'Page Builder', 'titan_theme' ),
				'components' => array(
					'page-intro',
					'content',
					'content-1col-bg',
					'content-2col-image',
					'content-video',
					'image-slider',
					'testimonial',
					'testimonial-grid',
					'property-featured',
					'logo-grid',
					'team-grid',
					'recent-posts',
					'cta',
				),
				'locations'  => array(
					'page_template' => array(
						titan_path_to_page_template( 'builder.php' ),
						//titan_path_to_page_template( 'builder-sidebar.php' ),
					),
				),
			),
			'builder-property'         => array(
				'title'      => __( 'Page Builder', 'titan_theme' ),
				'components' => array(
					'page-intro',
					'image-slider',
					'property-details',
					'content',
					'content-2col-image',
					'content-video',
					'testimonial',
					'property-credits',
					'cta',
				),
				'locations'  => array(
					'post_type' => array(
						'property'
					),
				),
			),
			'builder-news'         => array(
				'title'      => __( 'News Builder', 'titan_theme' ),
				'components' => array(
					'page-intro',
					'content',
					'content-1col-bg',
					'content-2col-image',
					'content-video',
					'image-slider',
					'testimonial',
					'testimonial-grid',
					'property-featured',
					'logo-grid',
					'team-grid',
					'recent-posts',
					'cta',
				),
				'locations'  => array(
					'page_type' => array(
						'posts_page'
					),
				),
			),

			'builder-news-single'         => array(
				'title'      => __( 'News Builder - Single Post Footer', 'titan_theme' ),
				'components' => array(
					'page-intro',
					'content',
					'content-1col-bg',
					'content-2col-image',
					'content-video',
					'image-slider',
					'testimonial',
					'testimonial-grid',
					'property-featured',
					'logo-grid',
					'team-grid',
					'recent-posts',
					'cta',
				),
				'locations'  => array(
					'page_type' => array(
						'posts_page'
					),
				),
			),
			//'sidebar-builder' => array(
			//	'title'      => __( 'Sidebar Builder', 'titan_theme' ),
			//	'components' => array(
			//		'content',
			//	),
			//	'locations'  => array(
			//		'page_template' => array(
			//			titan_path_to_page_template( 'builder-sidebar.php' ),
			//		),
			//	),
			//),
			'single-team-builder'         => array(
				'title'      => __( 'Team Component Builder', 'titan_theme' ),
				'components' => array(
					'page-intro',
					'content',
					'content-1col-bg',
					'content-2col-image',
					'content-video',
					'image-slider',
					'testimonial',
					'testimonial-grid',
					'property-featured',
					'logo-grid',
					'team-grid',
					'recent-posts',
					'cta',
				),
				'locations'  => array(
					'post_type' => array(
						'teammember',
					),
				),
			),
		) );
	}

	add_action( 'titan/setup', 'titan_theme_setup' );
}
