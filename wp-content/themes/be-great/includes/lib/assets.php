<?php
/**
 * Titan Theme Assets
 *
 * This can include any custom Javascript or CSS Files that need to be included.
 *
 * @package     Titan_Theme
 * @subpackage  Functions
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

/**
 * Enqueue Scripts
 *
 * This will hook into the `titan/enqueue/scripts`
 * action that will fire right after the main app
 * javascript.
 *
 * @since 1.0.0
 */
if ( ! function_exists( 'titan_theme_enqueue_scripts' ) ) {
	function titan_theme_enqueue_scripts() {
		// Enqueue additional scripts here.
	}

	add_action( 'titan/enqueue/scripts', 'titan_theme_enqueue_scripts' );
}

/**
 * Enqueue Styles
 *
 * This will hook into the `titan/enqueue/scripts`
 * action that will fire right after the main app
 * javascript.
 *
 * @since 1.0.0
 */
if ( ! function_exists( 'titan_theme_enqueue_styles' ) ) {
	function titan_theme_enqueue_styles() {
		// Enqueue addtional styles here.
	}

	add_action( 'titan/enqueue/styles', 'titan_theme_enqueue_styles' );
}
