<?php
/**
 * Titan Theme Custom Functions
 *
 * Use this to add any custom functionality needed
 * to the theme.
 *
 * @package     Titan_Theme
 * @subpackage  Functions
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */


/**
 * Gravity Forms Label Options
 *
 * Adds a Field Label Visibility dropdown
 * under the Appearance tab in your Gravity
 * Form field
 *
 * @link  https://gravitywiz.com/how-to-hide-gravity-form-field-labels-when-using-placeholders/
 *
 * @since 1.0.0
 */
add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

/**
 * DSC Image Sizes
 *
 * Custom image sizes for DSC 2017.
 *
 * @since 1.0.0
 */

add_action( 'init', 'dsc_add_image_sizes' );

if ( ! function_exists( 'dsc_add_image_sizes' ) ) {
	function dsc_add_image_sizes() {

		add_image_size( 'list_property_large', 519, 403, true );
		add_image_size( 'list_property_small', 360, 300, true ); // also for featured news
		add_image_size( 'gallery_thumb', 260, 260, true );

		/// Test custom size for digimark
		add_image_size( 'content-column', 581, 99999, false ); // 768 pixels wide by unlimited pixels tall, resize mode

		//add_image_size( 'news-banner', 910, 230, true );
		//add_image_size( 'page-side', 350, 9999, false );
		//add_image_size( 'page-link', 150, 150, true );
		//add_image_size( 'top-banner', 910, 9999, false );
	}
}


add_filter( 'image_size_names_choose', 'dsc_show_image_sizes' );

if ( ! function_exists( 'dsc_show_image_sizes' ) ) {

	function dsc_show_image_sizes( $sizes ) {
//		$sizes['property-icon'] = __( 'Property Icon', 'dsc' );
//		$sizes['news-banner']   = __( 'News Banner', 'dsc' );
//		$sizes['page-link']     = __( 'Page Link', 'dsc' );
		$sizes['gallery_thumb'] = __( 'Gallery Thumb', 'dsc' );

		return $sizes;
	}

}

/**
 * Move Yoast SEO
 *
 * Automatically moves Yoast to the bottom of the page.
 *
 * @since 1.0.0
 */
add_filter( 'wpseo_metabox_prio', function () {
	return 'low';
} );


/**
 * Header Color Scheme
 *
 * Set post meta value based on existence of hero or selected option.
 *
 * @since 1.0.0
 */

// On post ACF save, set meta to navbar-default (blue).
// check for a hero large or hero component, if it exists check the header color option, set post meta to navbar-inverse (white) else, set to navbar-default (blue)
add_action('acf/save_post', 'dsc_update_header_color', 20); // 20 = after ACF data has been saved

if ( ! function_exists( 'dsc_update_header_color' ) ) {

	function dsc_update_header_color( $post_id ) {
		
		// Check for flags that would exclude the page from requiring this function

		// If this is just a revision, exit
		if ( wp_is_post_revision( $post_id ) && wp_is_post_autosave( $post_id ) ) {
			return;
		}

		$meta_value = 'navbar-default'; // blue

		// check if the flexible content field has rows of data
		if( have_rows( 'b_hero-builder', $post_id ) ):

			// loop through the rows of data
			while ( have_rows( 'b_hero-builder', $post_id ) ) : the_row();

				if( get_row_layout() == 'c_hero-image_large' || get_row_layout() == 'c_hero-image' ):

					$meta_value = 'navbar-inverse';
					$override = get_sub_field( 'navigation_text_color' );


					if( $override ) {
						$meta_value = $override;
					}

					if (get_row_layout() == 'c_hero-image') {
						$meta_value .= " navbar-mobile-default";
                    }

					break;

				endif;

			endwhile;

		endif;

		update_post_meta( $post_id, '_header_color_scheme', $meta_value );
		
	}
}

/**
 * Formats Menu
 *
 * Set styling options for custom formats menu in the editor.
 * Callback function to insert 'styleselect' into the $buttons array
 *
 * @since 1.0.0
 *
 * @param array $buttons
 *
 * @return array $buttons
 */

add_filter( 'mce_buttons_2', 'dsc_mce_buttons' );

function dsc_mce_buttons( $buttons ) {
	array_unshift( $buttons, 'fontsizeselect' );
	array_unshift( $buttons, 'styleselect' );
	$buttons[] = 'superscript'; // display hidden superscript button

	return $buttons;
}

/**
 * Insert the styles into TinyMCE
 *
 * @since 1.0.0
 *
 * @param array $init_array
 *
 * @return mixed
 */

add_filter( 'tiny_mce_before_init', 'dsc_mce_before_init_insert_formats' );

function dsc_mce_before_init_insert_formats( $init_array ) {
	// custom style formats
	$style_formats = array(
		array(
			'title'   => 'Lead Paragraph',
			'classes' => 'lead',
			'block'   => 'p',
		),
		array(
			'title'   => 'Drop Cap Paragraph',
			'classes' => 'lead dropcap',
			'block'   => 'p',
		),
		array(
			'title'    => 'Page Title Style',
			'classes'  => 'heading',
			'selector' => 'h1, h2, h3, h4, h5, h6',
		),
		array(
			'title'    => 'Heading Style',
			'classes'  => 'subheading',
			'selector' => 'h1, h2, h3, h4, h5, h6',
		),
		array(
			'title'    => 'Heading - Centered',
			'classes'  => 'text-center',
			'selector' => 'h1, h2, h3, h4, h5, h6',
		),
		array(
			'title'    => 'Video Popup Link',
			'classes'  => 'magnific-video',
			'selector' => 'a',
		),
		array(
			'title'   => 'Uppercase',
			'classes' => 'text-uppercase',
			'inline'  => 'span',
			'wrapper' => true,
		),
		array(
			'title'   => 'Font Weight: Regular',
			'classes' => 'font-weight-regular',
			'inline'  => 'span',
			'wrapper' => true,
		),
		array(
			'title'   => 'Font-Weight: Semi-Bold',
			'classes' => 'font-weight-semibold',
			'inline'  => 'span',
			'wrapper' => true,
		),
		array(
			'title'   => 'Font Weight: Bold',
			'classes' => 'font-weight-bold',
			'inline'  => 'span',
			'wrapper' => true,
		),
		array(
			'title'    => 'Button/Link: Blue',
			'classes'  => 'btn btn-primary',
			'selector' => 'a',
		),
		array(
			'title'    => 'Button/Link: Gray',
			'classes'  => 'btn btn-default',
			'selector' => 'a',
		),
	);

	$init_array['style_formats']          = json_encode( $style_formats );
	$init_array['style_formats_autohide'] = true;

	// custom font sizes
	$init_array['fontsize_formats'] = "12px 14px 15px 17px 19px 22px 24px 61px";

	return $init_array;
}

// Social Media Shortcode
add_shortcode( 'social_links', 'dsc_output_social_links' );

if ( ! function_exists( 'dsc_output_social_links' ) ) {
	function dsc_output_social_links( $atts ){

		ob_start();

		$facebook  = get_field( 'facebook', 'options' );
		$instagram = get_field( 'instagram', 'options' );
		$linkedin  = get_field( 'linkedin', 'options' );
		?>
		<div class="social">
			<?php if ( $facebook ) : ?><a href="<?php echo $facebook; ?>" target="_blank"><span class="fa fa-facebook"></span></a><?php endif; ?>
			<?php if ( $instagram ) : ?><a href="<?php echo $instagram; ?>" target="_blank"><span class="fa-stack"><i class="fa fa-square fa-stack-1x"></i><i class="fa fa-instagram fa-stack-1x fa-inverse"></i></span></a><?php endif; ?>
			<?php if ( $linkedin ) : ?><a href="<?php echo $linkedin; ?>" target="_blank"><span class="fa fa-linkedin"></span></a><?php endif; ?>
		</div>
		<?php
		return ob_get_clean();
	}
}


/*-----------------------------------------------------------------------------------*/
/*  Shortcode for Team DSC Login form
/*-----------------------------------------------------------------------------------*/
add_shortcode('dscteamform', 'team_dsc_form_shortcode');
function team_dsc_form_shortcode($atts){
	ob_start();
	?>
    <a href="https://teamdsc.com/_forms/TeamDSCLogin.aspx" class="btn btn-primary team-dsc-btn">Login to the Team DSC<sup>®</sup> Portal</a>
	<?php
	$content = ob_get_contents();
	ob_end_clean();
	return $content;
}



/*-----------------------------------------------------------------------------------*/
/* listallpages shortcode
/*-----------------------------------------------------------------------------------*/
add_shortcode('listallpages', 'dsc_listpages');
function dsc_listpages(){
	// ob_start();
	$args = array(
		'depth' => 0,
		'sort_column' => 'menu_order',
		'title_li' => '<h4 class="heading">Pages</h4>',
		'echo' => false
	);
	$content = wp_list_pages($args);
	// $content = ob_get_contents();
	// ob_end_clean();
	return $content;
}


/*-----------------------------------------------------------------------------------*/
/* listproperties shortcode
/*-----------------------------------------------------------------------------------*/
add_shortcode('listproperties', 'dsc_listproperties');
function dsc_listproperties(){
	// ob_start();
	$args = array(
		'depth' => 0,
		'sort_column' => 'menu_order',
		'title_li' => '<h4 class="heading"><a href="/custom-home-builder/">Custom Home Builder</a></h4>',
		'echo' => false,
		'post_type' => 'property'
	);
	$content = wp_list_pages($args);
	// $content = ob_get_contents();
	// ob_end_clean();
	return $content;
}


/*-----------------------------------------------------------------------------------*/
/* listteam shortcode
/*-----------------------------------------------------------------------------------*/
add_shortcode('listteam', 'dsc_listteam');
function dsc_listteam(){
	// ob_start();
	$args = array(
		'depth' => 0,
		'sort_column' => 'menu_order',
		'title_li' => '<h4 class="heading"><a href="/our-team/">Team DSC<sup>®</sup> Bios</a></h4>',
		'echo' => false,
		'post_type' => 'teammember'
	);
	$content = wp_list_pages($args);
	// $content = ob_get_contents();
	// ob_end_clean();
	return $content;
}

/*-----------------------------------------------------------------------------------*/
/* listpposts shortcode
/*-----------------------------------------------------------------------------------*/
add_shortcode('listallposts', 'dsc_listposts');
function dsc_listposts(){
	ob_start();

	echo '<h4 class="heading"><a href="/news/">News</a></h4>';

	$cats = get_categories();
	foreach ($cats as $cat){
		echo '<h5><a href="'.get_category_link($cat->term_id).'">'.$cat->name.'</a></h5>';
		$args = array( 'post_type' => 'post', 'orderby' => 'post_date', 'cat' => $cat->term_id, 'showposts' => -1 );
		$post_query = new WP_Query($args);
		echo '<ul>';
		while ( $post_query->have_posts() ) : $post_query->the_post(); ?>
            <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
			<?php
		endwhile;
		echo '</ul>';
		wp_reset_postdata();
	}

	$content = ob_get_contents();
	ob_end_clean();
	return $content;
}
