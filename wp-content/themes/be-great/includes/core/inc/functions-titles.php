<?php
/**
 * Title Functions
 *
 * @package     Titan
 * @subpackage  Includes
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

/**
 * Get the Title
 *
 * @since   1.0.0
 * @access  public
 * @return  string
 */
function titan_get_the_title() {
	if ( is_home() ) {
		if ( get_option( 'page_for_posts', true ) ) {
			return get_the_title( get_option( 'page_for_posts', true ) );
		} else {
			return __( 'Latest Posts', 'titan' );
		}
	} elseif ( is_archive() ) {
		return get_the_archive_title();
	} elseif ( is_search() ) {
		return sprintf( __( 'Search Results for "%s"', 'titan' ), get_search_query() );
	} elseif ( is_404() ) {
		return __( '404 - Page Not Found', 'titan' );
	} elseif ( is_front_page() ) {
		return __( 'Home', 'titan' );
	} else {
		return get_the_title();
	}
}

/**
 * The Title
 *
 * @since   1.0.0
 * @access  public
 * @return  string
 */
function titan_the_title() {
	echo titan_get_the_title();
}
