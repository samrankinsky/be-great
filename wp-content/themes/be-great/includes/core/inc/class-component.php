<?php
/**
 * Component class. This class is for creating new component objects. Component registration is handled via
 * the `Titan_Component_Factory` class in `inc/class-component-factory.php`. Theme authors should utilize
 * the API functions in `inc/functions-components.php`.
 *
 * @package     Titan
 * @subpackage  Includes
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

/**
 * Creates new component object.
 *
 * @since   1.0.0
 * @access  public
 */
class Titan_Component {

	/**
	 * Arguments for creating the component object.
	 *
	 * @since   1.0.0
	 * @access  public
	 * @var     array
	 */
	public $args = array();

	/**
	 * Array of Component fields
	 *
	 * @since   1.0.0
	 * @access  public
	 * @var     array
	 */
	public $fields = array();

	/**
	 * Array of Advanced Fields
	 *
	 * @since   1.0.0
	 * @access  public
	 * @var     array
	 */
	public $advanced_fields = array();

	/**
	 * Advanced Fields
	 *
	 * @since   1.0.0
	 * @access  public
	 * @var     array
	 */
	public $advanced = array();

	/**
	 * Builder Component
	 *
	 * @since   1.0.0
	 * @access  public
	 * @var     array
	 */
	public $is_builder_component = false;

	/**
	 * Counter
	 *
	 * @since   1.0.0
	 * @access  public
	 * @var     int
	 */
	public $counter = 0;

	/**
	 * Register a new component object
	 *
	 * @since   1.0.0
	 * @access  public
	 *
	 * @param   string $name
	 * @param   array  $args {
	 *
	 * @type string    $label
	 *          }
	 * @return void
	 */
	public function __construct( $name, $args = array() ) {
		// Name
		$name = sanitize_key( $name );

		// Defaults
		$defaults = apply_filters( "titan/component/{$name}/defaults", array(
			'name'     => $name,
			'label'    => $name,
			'path'     => trailingslashit( TITAN_COMPONENTS . $name ),
			'template' => 'template.php',
			'file'     => 'component.php',
			'cjson'    => 'acf.json',
			'ajson'    => 'advanced.json',
		) );

		// Set Components
		$this->args = wp_parse_args( $args, $defaults );

		// Path
		$this->path = apply_filters( "titan/component/{$this->name}/path", $this->path );

		// Template
		$this->template = apply_filters( "titan/component/{$this->name}/template", $this->path . $this->template );

		// File
		$this->file = apply_filters( "titan/component/{$this->name}/file", $this->path . $this->file );

		// Load Component
		$this->load();

		// Add Local Fields
		add_action( 'acf/include_fields', array( $this, 'register_fields' ), 10, 5 );
		add_action( 'acf/include_fields', array( $this, 'register_advanced_fields' ), 10, 5 );
	}

	/**
	 * Load Component
	 *
	 * @since   1.0.0
	 * @access  protected
	 * @return  void
	 */
	protected function load() {
		global $component;
		if ( file_exists( $this->file ) ) {
			$component = $this;
			include_once $this->file;

			// Hook
			do_action( "titan/component/{$this->name}/loaded", $this );
		}
	}

	/**
	 * Register Corresponding Fields
	 *
	 * @since  1.0.0
	 * @access public
	 * @return void
	 */
	public function register_fields() {
		$this->fields = $this->get_acf_component_fields();
		if ( function_exists( 'acf_add_local_field_group' ) && ! empty( $this->fields ) ) {
			acf_add_local_field_group( $this->fields );
		}
	}

	/**
	 * Get Component Fields
	 *
	 * @since   1.0.0
	 * @access  public
	 * @return  array
	 */
	public function get_acf_component_fields() {
		// Get Fields
		$fields = $this->get_field_json();

		// Validate
		if ( empty( $fields ) || ! $fields ) {
			return array();
		}

		// Filter Location
		$location           = acf_extract_var( $fields, 'location' );
		$fields['location'] = ( ! empty( $location ) ) ? $location : $this->default_location();
		$fields['location'] = apply_filters( "titan/component/{$this->name}/acf/location", $fields['location'] );

		// Local
		$fields['local'] = 'json';

		// Hook
		do_action( "titan/component/{$this->name}/acf/fields", $fields );

		// Return
		return apply_filters( "titan/component/{$this->name}/acf/fields", $fields );
	}

	/**
	 * Register Advanced Fields
	 *
	 * @since  1.0.0
	 * @access public
	 * @return void
	 */
	public function register_advanced_fields() {
		$this->advanced_fields = $this->get_acf_advanced_fields();
		if ( function_exists( 'acf_add_local_field_group' ) && ! empty( $this->advanced_fields ) ) {
			acf_add_local_field_group( $this->advanced_fields );
		}
	}

	/**
	 * Get Advanced ACF Fields
	 *
	 * @since  1.0.0
	 * @access public
	 * @return array
	 */
	public function get_acf_advanced_fields() {
		// Get Fields
		$advanced_fields = $this->get_advanced_field_json();

		// Validate
		if ( empty( $advanced_fields ) || ! $advanced_fields ) {
			return array();
		}

		// Filter Title
		$title = acf_extract_var( $advanced_fields, 'title' );
		$title = sprintf( '%s - %s', $this->get_name_formatted_by_param( $this->name ), __( 'Advanced', 'titan' ) );

		// Set Title in fields
		$advanced_fields['title'] = apply_filters( "titan/component/{$this->name}/acf/advanced/title", $title );

		// Get location
		$location = acf_extract_var( $advanced_fields, 'location' );
		$location = ( ! empty( $location ) ) ? $location : $this->default_location();

		// Set Location in fields
		$advanced_fields['location'] = apply_filters( "titan/component/{$this->name}/acf/advanced/location", $location );

		// Local
		$advanced_fields['local'] = 'json';

		// Hook
		do_action( "titan/component/{$this->name}/acf/advanced/fields", $advanced_fields );

		// Return
		return apply_filters( "titan/component/{$this->name}/acf/advanced/fields", $advanced_fields );
	}

	/**
	 * Get Default Location
	 *
	 * @since   1.0.0
	 * @access  public
	 * @return  array
	 */
	public function default_location() {
		return apply_filters( "titan/component/{$this->name}/acf/default_location", array(
			array(
				array(
					'param'    => 'post_type',
					'operator' => '==',
					'value'    => 'titan-nothing',
				),
			),
		) );
	}

	/**
	 * Load Advanced Fields
	 *
	 * @since  1.0.0
	 * @access public
	 * @return array    $advanced
	 */
	public function load_advanced() {

		// Default Holder
		$fields = array();

		// Default Advanced
		$default_advanced_fields_json = $this->get_advanced_default_field_json();
		$default_advanced_fields      = titan_extract_var( $default_advanced_fields_json, 'fields' );

		// Added Advanced
		$advanced_fields_json = $this->get_advanced_field_json();
		$advanced_fields      = titan_extract_var( $advanced_fields_json, 'fields' );

		if ( ! empty( $default_advanced_fields ) ) {
			foreach ( $default_advanced_fields as $field ) {
				if ( isset( $field['name'] ) ) {
					$name            = str_replace( '_', '-', $field['name'] );
					$fields[ $name ] = get_sub_field( $field['name'] );
				}
			}
		}

		if ( ! empty( $advanced_fields ) ) {
			foreach ( $advanced_fields as $field ) {
				if ( isset( $field['name'] ) ) {
					$name            = str_replace( '_', '-', $field['name'] );
					$fields[ $name ] = get_sub_field( $field['name'] );
				}
			}
		}

		// Add Class
		$fields['class'] = get_sub_field( 'class' );
		$fields['id']    = get_sub_field( 'id' );

		// Filter
		$this->advanced = apply_filters( "titan/component/{$this->name}/acf/advanced/fields/load", $fields );
	}

	/* ====== ACF json ====== */

	/**
	 * Get Content JSON
	 *
	 * @since   1.0.0
	 * @access  protected
	 * @return  array
	 */
	public function get_field_json() {
		$json = trailingslashit( TITAN_COMPONENTS . $this->args['name'] ) . $this->args['cjson'];

		return ( file_exists( $json ) ) ? $this->get_fields_from_file( $json ) : array();
	}

	/**
	 * Get Advanced Tab JSON
	 *
	 * @since   1.0.0
	 * @access  protected
	 * @return  array
	 */
	public function get_advanced_field_json() {
		$json = trailingslashit( TITAN_COMPONENTS . $this->args['name'] ) . $this->args['ajson'];

		return ( file_exists( $json ) ) ? $this->get_fields_from_file( $json ) : array();
	}

	/**
	 * Get Default Advanced Tab JSON
	 *
	 * @since   1.0.0
	 * @access  protected
	 * @return  array
	 */
	public function get_advanced_default_field_json() {
		$json = apply_filters( 'titan/component/acf/advanced/json_file', TITAN_JSON . 'default-advanced.json' );

		return ( file_exists( $json ) ) ? $this->get_fields_from_file( $json ) : array();
	}

	/* ====== Getters ====== */

	/**
	 * Get Template Path
	 *
	 * @since   1.0.0
	 * @access  public
	 * @return  string
	 */
	public function get_template() {
		return $this->template;
	}

	/**
	 * Get Args
	 *
	 * @since   1.0.0
	 * @access  public
	 * @return  array   $args
	 */
	public function get_args() {
		return $this->args;
	}

	/**
	 * Get Fields
	 *
	 * @since   1.0.0
	 * @access  public
	 * @return  array   $advanced
	 */
	public function get_component_fields() {
		return $this->fields;
	}

	/**
	 * Get Advanced Fields
	 *
	 * @since   1.0.0
	 * @access  public
	 * @return  array   $advanced
	 */
	public function get_advanced_fields() {
		return $this->advanced_fields;
	}

	/**
	 * Get Advanced
	 *
	 * @since   1.0.0
	 * @access  public
	 * @return  array   $advanced
	 */
	public function get_advanced() {
		return $this->advanced;
	}

	/**
	 * Get Fields from JSON file
	 *
	 * @since   1.0.0
	 * @access  public
	 *
	 * @param   string $file
	 *
	 * @return  void|array
	 */
	public function get_fields_from_file( $file = '' ) {
		// bail early
		if ( ! $file ) {
			return;
		}

		// read json and get fields
		$fields = file_get_contents( $file );

		// validate
		if ( empty ( $fields ) ) {
			return;
		}

		// decode
		return json_decode( $fields, true );
	}

	/**
	 * Get Name Formatted
	 *
	 * @since   1.0.0
	 * @access  public
	 * @return  string
	 */
	public function get_name_formatted() {
		$this->name = str_replace( '-', ' ', $this->name );
		$this->name = str_replace( '_', ' ', $this->name );
		$this->name = ucwords( $this->name );

		return $this->name;
	}

	/**
	 * Get Name Formatted
	 *
	 * @since   1.0.0
	 * @access  public
	 *
	 * @param   string $name
	 *
	 * @return  string $name
	 */
	public function get_name_formatted_by_param( $name ) {
		if ( empty( $name ) ) {
			return $name;
		}

		$name = str_replace( '-', ' ', $name );
		$name = str_replace( '_', ' ', $name );
		$name = ucwords( $name );

		return $name;
	}

	/**
	 * Get Is Builder Component
	 *
	 * @since  1.0.0
	 * @access public
	 * @return bool
	 */
	public function get_is_builder_component() {
		return $this->is_builder_component;
	}

	/**
	 * Get Counter
	 *
	 * @since  1.0.0
	 * @access public
	 * @return int
	 */
	public function get_counter() {
		return $this->counter;
	}

	/* ====== Setter ====== */

	/**
	 * Set Is Builder Component
	 *
	 * @since 1.0.0
	 *
	 * @param bool $set Set the status of the builder component.
	 *
	 * @return  void
	 */
	public function set_is_builder_component( $set = true ) {
		$this->is_builder_component = $set;
	}

	/**
	 * Set Counter
	 *
	 * @since  1.0.0
	 * @access public
	 *
	 * @param int $counter
	 *
	 * @return void
	 */
	public function set_counter( $counter = 0 ) {
		$this->counter = $counter;
	}

	/* ====== Magic Methods ====== */

	/**
	 * Magic method for getting layout object properties.
	 *
	 * @since   1.0.0
	 * @access  public
	 *
	 * @param   string $property
	 *
	 * @return  mixed
	 */
	public function __get( $property ) {
		return isset( $this->$property ) ? $this->args[ $property ] : null;
	}

	/**
	 * Magic method for setting layout object properties.
	 *
	 * @since   1.0.0
	 * @access  public
	 *
	 * @param   string $property
	 * @param   mixed  $value
	 *
	 * @return  void
	 */
	public function __set( $property, $value ) {
		if ( isset( $this->$property ) ) {
			$this->args[ $property ] = $value;
		}
	}

	/**
	 * Magic method for checking if a layout property is set.
	 *
	 * @since   1.0.0
	 * @access  public
	 *
	 * @param   string $property
	 *
	 * @return  bool
	 */
	public function __isset( $property ) {
		return isset( $this->args[ $property ] );
	}

	/**
	 * Don't allow properties to be unset.
	 *
	 * @since   1.0.0
	 * @access  public
	 *
	 * @param   string $property
	 *
	 * @return  void
	 */
	public function __unset( $property ) {
	}

	/**
	 * Magic method to use in case someone tries to output the layout object as a string.
	 * We'll just return the layout name.
	 *
	 * @since   1.0.0
	 * @access  public
	 * @return  string
	 */
	public function __toString() {
		return $this->name;
	}
}
