<?php
/**
 * Template Functions
 *
 * Includes functions to filte the template heirarchy.
 *
 * @package     Titan
 * @subpackage  Includes
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

# Single Template
add_filter( 'single_template', 'titan_template_singular', 5 );

# Singular Template
add_filter( 'singular_template', 'titan_template_singular', 5 );

# Blog Template
add_filter( 'home_template', 'titan_template_index', 5 );

# Archive Template
add_filter( 'archive_template', 'titan_template_archive', 5 );

# Category Template
add_filter( 'category_template', 'titan_template_category', 5 );

# Tag Template
add_filter( 'tag_template', 'titan_template_tag', 5 );

# Author Template
add_filter( 'author_template', 'titan_template_author', 5 );

# Taxonomy Template
add_filter( 'taxonomy_template', 'titan_template_taxonomy', 5 );

# 404 Template
add_filter( '404_template', 'titan_template_404', 5 );

# Search Template
add_filter( 'search_template', 'titan_template_search', 5 );

# Add Page Templates Directory
add_filter( 'theme_page_templates', 'titan_custom_page_templates', 10, 3 );

/**
 * Locate Template
 *
 * @since   1.0.0
 *
 * @param   array $template_names
 *
 * @return  string
 */
function titan_locate_template( $template_names, $post_type, $type, $name = '' ) {

	// Located var
	$located = '';

	// Template Path
	$template_path = apply_filters( 'titan/template/path', trailingslashit( TITAN_DIR . 'templates' ) );

	// Type
	if ( $type ) {
		$template_path = apply_filters( "titan/template/{$type}/path", $template_path );
	}

	// Type && Name
	if ( $type && $name ) {
		$template_path = apply_filters( "titan/template/{$type}/{$name}/path", $template_path );
	}

	// Post Type Defined - more specific
	if ( $post_type ) {
		$template_path = apply_filters( "titan/template/{$post_type}/path", $template_path );
	}

	// Type & Post Type
	if ( $post_type && $type ) {
		$template_path = apply_filters( "titan/template/{$post_type}/{$type}/path", $template_path );
	}

	// Define Paths
	foreach ( (array) $template_names as $template_name ) {
		if ( ! $template_name ) {
			continue;
		}
		if ( file_exists( $template_path . $template_name ) ) {
			$located = $template_path . $template_name;
			break;
		}
		if ( file_exists( STYLESHEETPATH . '/' . $template_name ) ) {
			$located = STYLESHEETPATH . '/' . $template_name;
			break;
		} elseif ( file_exists( TEMPLATEPATH . '/' . $template_name ) ) {
			$located = TEMPLATEPATH . '/' . $template_name;
			break;
		}
	}

	// Return if located
	return $located;
}

/**
 * Overrides the default single (singular post) template.
 * Post templates can be loaded using a custom
 * post template, by slug, or by ID.
 *
 * @since  1.0.0
 *
 * @param  string $template The default WordPress post template.
 *
 * @return string $template The theme post template after all templates have been checked for.
 */
function titan_template_singular( $template ) {
	// Templates Array
	$templates = array();

	// Get the queried post.
	$post = get_queried_object();

	// Add a post name (slug) template.
	$templates[] = "{$post->post_type}-{$post->post_name}.php";

	// Add a post ID template.
	$templates[] = "{$post->post_type}-{$post->ID}.php";

	// Add a template based off the post type name.
	$templates[] = "{$post->post_type}.php";

	// Allow for WP standard 'single' templates for compatibility.
	$templates[] = "single-{$post->post_type}.php";
	$templates[] = 'single.php';

	// Add a general template of singular.php.
	$templates[] = "singular.php";

	// Locate Template
	return titan_locate_template( $templates, $post->post_type, 'singular' );
}

/**
 * Overrides Archive
 *
 * @since  1.0.0
 *
 * @param  string $template The default WordPress post template.
 *
 * @return string $template The theme post template after all templates have been checked for.
 */
function titan_template_archive( $template ) {

	// Define the specifics
	$post_types = array_filter( (array) get_query_var( 'post_type' ) );
	$post_type  = '';
	$templates  = array();

	// Match a more specific one.
	if ( count( $post_types ) == 1 ) {
		$post_type   = reset( $post_types );
		$templates[] = "archive-{$post_type}.php";
	}

	// Match the generic template
	$templates[] = 'archive.php';

	// If no other ones match, find the default.
	$templates[] = 'index.php';

	// Locate Template
	return titan_locate_template( $templates, $post_type, 'archive' );
}

/**
 * Overrides Category
 *
 * @since  1.0.0
 *
 * @param  string $template The default WordPress post template.
 *
 * @return string $template The theme post template after all templates have been checked for.
 */
function titan_template_category( $template ) {

	// Define the specifics
	$category  = get_queried_object();
	$templates = array();

	// Match a more specific template
	if ( ! empty( $category->slug ) ) {
		$templates[] = "category-{$category->slug}.php";
		$templates[] = "category-{$category->term_id}.php";
	}

	// Match the generic template
	$templates[] = 'category.php';

	// Match the archive template
	$templates[] = 'archive.php';

	// If no other ones match, find the default.
	$templates[] = 'index.php';

	// Locate Template
	return titan_locate_template( $templates, '', 'category' );
}

/**
 * Overrides Tag
 *
 * @since  1.0.0
 *
 * @param  string $template The default WordPress post template.
 *
 * @return string $template The theme post template after all templates have been checked for.
 */
function titan_template_tag( $template ) {

	// Define the specifics
	$tag       = get_queried_object();
	$templates = array();

	// Match a more specific template
	if ( ! empty( $tag->slug ) ) {
		$templates[] = "tag-{$tag->slug}.php";
		$templates[] = "tag-{$tag->term_id}.php";
	}

	// Match the generic template
	$templates[] = 'tag.php';

	// Match the archive template
	$templates[] = 'archive.php';

	// If no other ones match, find the default.
	$templates[] = 'index.php';

	// Locate Template
	return titan_locate_template( $templates, '', 'tag' );
}

/**
 * Overrides Author
 *
 * @since  1.0.0
 *
 * @param  string $template The default WordPress post template.
 *
 * @return string $template The theme post template after all templates have been checked for.
 */
function titan_template_author( $template ) {

	// Define the specifics
	$author    = get_queried_object();
	$templates = array();

	// Match a more specific template
	if ( $author instanceof WP_User ) {
		$templates[] = "author-{$author->user_nicename}.php";
		$templates[] = "author-{$author->ID}.php";
	}

	// Match the generic template
	$templates[] = 'author.php';

	// Match the archive template
	$templates[] = 'archive.php';

	// If no other ones match, find the default.
	$templates[] = 'index.php';

	// Locate Template
	return titan_locate_template( $templates, '', 'author' );
}

/**
 * Overrides Attachment Template
 *
 * @since  1.0.0
 *
 * @param  string $template The default WordPress post template.
 *
 * @return string $template The theme post template after all templates have been checked for.
 */
function titan_template_attachment( $template ) {

	$attachment = get_queried_object();

	$templates = array();

	if ( $attachment ) {
		if ( false !== strpos( $attachment->post_mime_type, '/' ) ) {
			list( $type, $subtype ) = explode( '/', $attachment->post_mime_type );
		} else {
			list( $type, $subtype ) = array( $attachment->post_mime_type, '' );
		}

		if ( ! empty( $subtype ) ) {
			$templates[] = "{$type}-{$subtype}.php";
			$templates[] = "{$subtype}.php";
		}
		$templates[] = "{$type}.php";
	}

	// Match the generic template
	$templates[] = 'attachment.php';

	// Add the single template
	$templates[] = 'single.php';

	// Add a general template of singular.php.
	$templates[] = "singular.php";

	// Locate Template
	return titan_locate_template( $templates, '', 'attachment' );
}

/**
 * Overrides Taxonomy
 *
 * @since  1.0.0
 *
 * @param  string $template The default WordPress post template.
 *
 * @return string $template The theme post template after all templates have been checked for.
 */
function titan_template_taxonomy( $template ) {

	// Get details
	$term      = get_queried_object();
	$taxonomy  = '';
	$templates = array();

	// Match more specifically
	if ( ! empty( $term->slug ) ) {
		$taxonomy    = $term->taxonomy;
		$templates[] = "taxonomy-$taxonomy-{$term->slug}.php";
		$templates[] = "taxonomy-$taxonomy.php";
	}

	// Match the generic template
	$templates[] = 'taxonomy.php';

	// March the archive template
	$templates[] = 'archive.php';

	// Match the default template.
	$templates[] = 'index.php';

	// Locate Template
	return titan_locate_template( $templates, '', 'taxonomy', $taxonomy );
}

/**
 * Overrides the default index.php or home.php template inside the component folder.
 *
 * @since  1.0.0
 *
 * @param  string $template The default WordPress post template.
 *
 * @return string $template The theme post template after all templates have been checked for.
 */
function titan_template_index() {
	return titan_locate_template( array( 'home.php', 'index.php' ), '', 'index' );
}

/**
 * Overrides the default 404.php or a component folder.
 *
 * @since  1.0.0
 *
 * @param  string $template The default WordPress post template.
 *
 * @return string $template The theme post template after all templates have been checked for.
 */
function titan_template_404() {
	return titan_locate_template( array( '404.php' ), '', '404' );
}

/**
 * Overrides the default search.php or a component folder.
 *
 * @since  1.0.0
 *
 * @param  string $template The default WordPress post template.
 *
 * @return string $template The theme post template after all templates have been checked for.
 */
function titan_template_search() {
	return titan_locate_template( array( 'search.php' ), '', 'search' );
}

/**
 * Find files in the cores page-templates directory.
 *
 * @since   1.0.0
 *
 * @return  array   Array of files, keyed by the path to the file relative to the theme's directory, with the values
 *                  being absolute paths.
 */
function titan_find_page_templates() {

	// Path
	$path = TITAN_DIR . titan_get_page_templates_path();

	// Scan directory
	$results = (array) scandir( $path );

	// remove . and .. files
	$results = array_diff( $results, array( '..', '.' ) );

	// loop through files
	foreach ( $results as $result ) {
		// find only php files
		if ( preg_match( '~\.(php)$~', $result ) ) {
			$files[ $result ] = $path . $result;
		}
	}

	return $files;
}

/**
 * Return files in the cores page-templates directory.
 *
 * @since   1.0.0
 *
 * @param   array        $page_templates
 * @param   WP_Theme     $this The theme object.
 * @param   WP_Post|null $post Optional. The post being edited, provided for context.
 *
 * @return array Array of page templates, keyed by filename, with the value of the translated header name.
 */
function titan_custom_page_templates( $page_templates = array(), $theme, $post ) {

	// get core page templates
	$files = titan_find_page_templates();

	// Go through page templates
	foreach ( $files as $file => $full_path ) {

		// find the template name
		if ( ! preg_match( '|Template Name:(.*)$|mi', file_get_contents( $full_path ), $header ) ) {
			continue;
		}

		// get the template path
		$file_path = str_replace( TITAN_DIR, '', $full_path );

		// set page template
		$page_templates[ $file_path ] = _cleanup_header_comment( $header[ 1 ] );

	}

	return $page_templates;
}

/**
 * Get the page template path to the file
 *
 * @since   1.0.0
 *
 * @return  string
 */
function titan_get_page_templates_path() {
	return apply_filters( 'titan/templates/path', trailingslashit( 'includes/page-templates' ) );
}

/**
 * Get the full page template path
 *
 * @since   1.0.0
 *
 * @param   string $file
 *
 * @return  string  $file
 */
function titan_path_to_page_template( $file ) {
	return titan_get_page_templates_path() . $file;
}
