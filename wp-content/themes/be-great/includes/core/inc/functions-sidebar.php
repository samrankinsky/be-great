<?php
/**
 * Helper functions for working with the WordPress sidebar system. Currently, the framework creates a
 * simple function for registering HTML5-ready sidebars instead of the default WordPress unordered lists.
 *
 * @package     Titan
 * @subpackage  Includes
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

# Compatibility for when a theme doesn't register any sidebars.
add_action( 'widgets_init', '__return_false', 95 );

/**
 * Wrapper function for WordPress' register_sidebar() function. This function exists so that theme authors
 * can more quickly register sidebars with an HTML5 structure instead of having to write the same code
 * over and over. Theme authors are also expected to pass in the ID, name, and description of the sidebar.
 * This function can handle the rest at that point.
 *
 * @since   1.0.0
 * @access  public
 * @param   array   $args
 * @return  string  Sidebar ID.
 */
function titan_register_sidebar( $args ) {

	// Set up some default sidebar arguments.
	$defaults = array(
		'id'            => '',
		'name'          => '',
		'description'   => '',
		'before_widget' => '<section class="widget %1$s %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>'
	);

	// Parse the arguments.
	$args = wp_parse_args( $args, apply_filters( 'titan_sidebar_defaults', $defaults ) );

	// Remove action.
	remove_action( 'widgets_init', '__return_false', 95 );

	// Register the sidebar.
	return register_sidebar( apply_filters( 'titan_sidebar_args', $args ) );
}

/**
 * Function for grabbing a dynamic sidebar name.
 *
 * @since   1.0.0
 * @access  public
 * @global  array   $wp_registered_sidebars
 * @param   string  $sidebar_id
 * @return  string
 */
function titan_get_sidebar_name( $sidebar_id ) {
	global $wp_registered_sidebars;
	return isset( $wp_registered_sidebars[ $sidebar_id ] ) ? $wp_registered_sidebars[ $sidebar_id ][ 'name' ] : '';
}