<?php
/**
 * Component factory class. This is a singleton factory class for handling the registering and
 * storing of `Titan_Component` objects. Theme authors should utilize the API functions found
 * in `inc/functions-components.php`.
 *
 * @package     Titan
 * @subpackage  Includes
 * @version     1.0.0
 * @author      Skyhook Interactive <support@skyhookinteractive.com>
 * @copyright   Copyright (c) 2016, Skyhook Interactive
 * @link        https://bitbucket.org/skyhook/titan
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

/**
 * Engine Core Component Factory class. This is the backbone of the Components API. Theme authors should
 * utilize the appropriate functions for accessing the `Titan_Component_Factory` object.
 *
 * @since   1.0.0
 * @access  public
 */
class Titan_Component_Factory {

	/**
	 * Array of component objects.
	 *
	 * @since   1.0.0
	 * @access  public
	 * @var     array
	 */
	public $components = array();

	/**
	 * Constructor method.
	 *
	 * @since   1.0.0
	 * @access  private
	 * @return  void
	 */
	private function __construct() {}

	/**
	 * Register a new component object
	 *
	 * @see     Titan_Component::__construct()
	 * @since   1.0.0
	 * @access  public
	 *
	 * @param   string  $name
	 * @param   array   $args
	 *
	 * @return  void
	 */
	public function register_component( $name, $args = array() ) {
		if ( ! $this->component_exists( $name ) ) {
			$component = new Titan_Component( $name, $args );
			$this->components[ $component->name ] = $component;
		}

		return $this->get_component( $name );
	}

	/**
	 * Unregisters a component object.
	 *
	 * @since   1.0.0
	 * @access  public
	 *
	 * @param   string $name
	 *
	 * @return  void
	 */
	public function unregister_component( $name ) {
		if ( $this->component_exists( $name ) ) {
			unset( $this->components[ $name ] );
		}
	}

	/**
	 * Checks if a component exists.
	 *
	 * @since   1.0.0
	 * @access  public
	 *
	 * @param   string  $name
	 *
	 * @return  bool
	 */
	public function component_exists( $name ) {
		return isset( $this->components[ $name ] );
	}

	/**
	 * Gets a component object.
	 *
	 * @since   1.0.0
	 * @access  public
	 *
	 * @param   string  $name
	 *
	 * @return  object|bool
	 */
	public function get_component( $name ) {
		return $this->component_exists( $name ) ? $this->components[ $name ] : false;
	}

	/**
	 * Returns the instance.
	 *
	 * @since   1.0.0
	 * @access  public
	 * @return  object
	 */
	public static function get_instance() {
		static $instance = null;
		if ( is_null( $instance ) ) {
			$instance = new Titan_Component_Factory();
		}
		return $instance;
	}
}
