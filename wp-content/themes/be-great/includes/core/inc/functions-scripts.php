<?php
/**
 * Functions for handling JavaScript in the framework. Themes can add support for the
 * 'titan-core-scripts' feature to allow the framework to handle loading the stylesheets into
 * the theme header or footer at an appropriate time.
 *
 * @package     Titan
 * @subpackage  Includes
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

# Register Titan Core Scripts.
add_action( 'wp_enqueue_scripts', 'titan_register_scripts', 5 );

# Load Titan Core Scripts.
add_action( 'wp_enqueue_scripts', 'titan_enqueue_scripts', 5 );

/**
 * Registers JavaScript files for the framework. This function merely registers scripts with WordPress using
 * the wp_register_script() function. It does not load any script files on the site. If a theme wants to register
 * its own custom scripts, it should do so on the 'wp_enqueue_scripts' hook.
 *
 * @since   1.0.0
 * @access  public
 * @return  void
 */
function titan_register_scripts() {
	// Register Scripts Before
	do_action( 'titan/register/scripts/before' );

	wp_register_script( 'titan/vendor/js', titan_asset( 'vendor', 'js' ), array( 'jquery' ),                    TITAN_VERSION, true );
	wp_register_script( 'titan/app/js',    titan_asset( 'app',    'js' ), array( 'jquery', 'titan/vendor/js' ), TITAN_VERSION, true );

	// Register More Scripts
	do_action( 'titan/register/scripts' );
}

/**
 * Tells WordPress to load the scripts needed for the framework using the wp_enqueue_script() function.
 *
 * @since   1.0.0
 * @access  public
 * @return  void
 */
function titan_enqueue_scripts() {
	// Enqueue Scripts Before
	do_action( 'titan/enqueue/scripts/before' );

	// Enqueue the comment reply script on singular posts with open comments if threaded comments are supported.
	if ( is_singular() && get_option( 'thread_comments' ) && comments_open() ) {
		wp_enqueue_script( 'comment-reply' );
	}

	// Enqueue Core Javascript
	wp_enqueue_script( 'titan/vendor/js' );
	wp_enqueue_script( 'titan/app/js'    );

	// Enqueue More Scripts
	do_action( 'titan/enqueue/scripts' );
}
