<?php
/**
 * Custom Nav Walker Functions
 *
 * @package     Titan
 * @subpackage  Includes
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

# Clean up nav menu args
add_filter( 'wp_nav_menu_args', 'titan_nav_menu_args' );
add_filter( 'nav_menu_item_id', '__return_null' );

/**
 * Clean up wp_nav_menu_args for Titan_Nav_Walker
 *
 * Remove the container
 * Remove the id="" on nav menu items
 *
 * @see     Titan_Nav_Walker
 * @since   1.0.0
 * @access  public
 * @param   string $args
 * @return  void
 */
function titan_nav_menu_args( $args = '' ) {
	$nav_menu_args                = [ ];
	$nav_menu_args[ 'container' ] = false;
	if ( ! $args[ 'items_wrap' ] ) {
		$nav_menu_args[ 'items_wrap' ] = '<ul class="%2$s">%3$s</ul>';
	}
	if ( ! $args[ 'walker' ] ) {
		$nav_menu_args[ 'walker' ] = new Titan_Nav_Walker();
	}
	return array_merge( $args, $nav_menu_args );
}