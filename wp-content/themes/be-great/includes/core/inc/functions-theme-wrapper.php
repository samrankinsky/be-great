<?php
/**
 * Theme Wrapper Functions
 *
 * @package     Titan
 * @subpackage  Includes
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

# Add Theme Wrapper to take over template loading
add_filter( 'template_include', 'titan_template_include', 100 );

/**
 * Returns the instance of the `Titan_Wrapper` object. Use this function to access the object.
 *
 * @see     Titan_Wrapper
 * @since   1.0.0
 * @access  public
 * @return  object
 */
function titan_template_include( $template ) {
	return Titan_Wrapper::wrap( $template );
}

/**
 * Returns the main template path.
 *
 * @see     Titan_Wrapper
 * @since   1.0.0
 * @access  public
 * @return  string
 */
function titan_get_main_template() {
	return Titan_Wrapper::$main_template;
}

/**
 * Returns the main sidebar template path.
 *
 * @see     Titan_Wrapper
 * @since   1.0.0
 * @access  public
 * @return  string
 */
function titan_get_main_sidebar() {
	return new Titan_Wrapper( 'templates/sidebar.php' );
}