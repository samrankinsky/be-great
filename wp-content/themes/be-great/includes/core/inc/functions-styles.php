<?php
/**
 * Functions for handling styles in the framework.
 *
 * @package     Titan
 * @subpackage  Includes
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

# Add Editor Styles
add_action( 'titan/setup', 'titan_add_editor_styles' );

# Register Titan Core styles.
add_action( 'wp_enqueue_scripts', 'titan_register_styles', 5 );

# Add Titan Core Styles
add_action( 'wp_enqueue_scripts', 'titan_enqueue_styles', 5 );

/**
 * Engueue styles for front end.
 *
 * @uses    wp_register_style()
 * @since   1.0.0
 * @access  public
 * @retun   void
 */
function titan_register_styles() {
	// Register Styles Before
	do_action( 'titan/register/styles/before' );

	// Main style.
	wp_register_style( 'titan/app/css', titan_asset( 'app', 'css' ), false, TITAN_VERSION );

	// Register More Styles
	do_action( 'titan/register/styles' );
}

/**
 * Engueue styles for front end.
 *
 * @uses    wp_register_style()
 * @since   1.0.0
 * @access  public
 * @retun   void
 */
function titan_enqueue_styles() {
	// Enqueue Styles Before.
	do_action( 'titan/enqueue/styles/before' );

	// Enqueue main style.
	wp_enqueue_style( 'titan/app/css' );

	// Enqueue Styles.
	do_action( 'titan/enqueue/styles' );
}

/**
 * Add the editor styles so the backend wysywig can look similar to the frontend.
 *
 * @uses    add_editor_style()
 * @since   1.0.0
 * @access  public
 * @return  void
 */
function titan_add_editor_styles() {
	add_editor_style( titan_asset( 'app', 'css', 'path' ) );

	// Add More Styles
	do_action( 'titan/add/editor/styles' );
}
