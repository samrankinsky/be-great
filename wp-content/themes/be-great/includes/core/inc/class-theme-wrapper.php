<?php
/**
 * Theme Wrapper Class.
 *
 * @package     Titan
 * @subpackage  Includes
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

/**
 * Creates new Titan Wrapper object.
 *
 * @since   1.0.0
 * @access  public
 */
class Titan_Wrapper {

	/**
	 * Stores the full path to the main template file
	 *
	 * @since   1.0.0
	 * @access  public
	 * @static
	 * @var     string
	 */
	public static $main_template;

	/**
	 * Basename of template file
	 *
	 * @since   1.0.0
	 * @access  public
	 * @var     string
	 */
	public $slug;

	/**
	 * Stores the array of templates
	 *
	 * @since   1.0.0
	 * @access  public
	 * @static
	 * @var     array
	 */
	public $templates;

	/**
	 * Stores the base name of the template file; e.g. 'page' for 'page.php' etc
	 *
	 * @since   1.0.0
	 * @access  public
	 * @static
	 * @var     string
	 */
	public static $base;

	/**
	 * Register a new Titan_Wrapper object
	 *
	 * @since   1.0.0
	 * @access  public
	 * @param   string $template
	 */
	public function __construct( $template = 'base.php' ) {
		$this->slug      = basename( $template, '.php' );
		$this->templates = [ $template ];
		if ( self::$base ) {
			$str = substr( $template, 0, - 4 );
			array_unshift( $this->templates, sprintf( $str . '-%s.php', self::$base ) );
		}
	}

	/**
	 * To String Method
	 *
	 * @since   1.0.0
	 * @access  public
	 * @return  string
	 */
	public function __toString() {
		$this->templates = apply_filters( 'titan/wrap_' . $this->slug, $this->templates );
		return locate_template( $this->templates );
	}

	/**
	 * Returns a new instance of the Titan_Wrapper object/
	 *
	 * @since   1.0.0
	 * @access  public
	 * @param   $main
	 * @return  object  new Titan_Wrapper
	 */
	public static function wrap( $main ) {
		if ( ! is_string( $main ) ) {
			return $main;
		}
		self::$main_template = $main;
		self::$base          = basename( self::$main_template, '.php' );
		if ( self::$base === 'index' ) {
			self::$base = false;
		}
		return new Titan_Wrapper();
	}
}