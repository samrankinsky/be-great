<?php
/**
 * Builder class. This class is for creating new `Titan_Builder` objects. Builder registration is handled via
 * the `Titan_Builder_Factory` class in `inc/class-builder-factory.php`. Theme authors should utilize
 * the API functions in `inc/functions-builder.php`.
 *
 * @package     Titan
 * @subpackage  Includes
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

/**
 * Titan_Builder Class
 *
 * @since   1.0.0
 * @access  public
 */
class Titan_Builder {

	/**
	 * Arguments for creating the builder object.
	 *
	 * @since   1.0.0
	 * @access  protected
	 * @var     array
	 */
	protected $args = array();

	/**
	 * Array of component objects.
	 *
	 * @since   1.0.0
	 * @access  public
	 * @var     array
	 */
	public $components = array();

	/**
	 * Array of fields
	 *
	 * @since   1.0.0
	 * @access  public
	 * @var     array
	 */
	public $fields = array();

	/**
	 * Register a new `Titan_Builder` object
	 *
	 * @since   1.0.0
	 * @access  public
	 *
	 * @param   string $name
	 * @param   array  $args {
	 *
	 * @type    string $name
	 * @type    string $label
	 * @type    array  $components
	 * @type    array  $display
	 *          }
	 * @return void
	 */
	public function __construct( $name, $args = array() ) {
		// Name
		$name = sanitize_key( $name );

		// Defaults
		$defaults = apply_filters( "titan/builder/{$name}/defaults", array(
			'name'           => $name,
			'title'          => $name,
			'components'     => array(),
			'locations'      => array(),
			'menu_order'     => 0,
			'button_c_label' => __( '<i class="fa fa-cubes"></i> Add Component', 'titan' ),
			'button_label'   => __( '+ Add', 'titan' ),
		) );

		// Set args
		$this->args = wp_parse_args( $args, $defaults );

		// Register Components
		$this->register_components();

		// Add Local Fields
		add_action( 'acf/include_fields', array( $this, 'register_acf_fields' ), 10, 5 );
	}

	/* ====== Component Methods ====== */

	/**
	 * Register Supported Components
	 *
	 * @see     Titan_Component
	 * @since   1.0.0
	 * @access  protected
	 * @return  void
	 */
	protected function register_components() {
		// Define
		$components = ( isset( $this->args[ 'components' ] ) && ! empty ( $this->args[ 'components' ] ) ) ? $this->args[ 'components' ] : false;

		// Check
		if ( ! $components ) {
			return;
		}

		// Add components
		foreach ( $components as $component_key => $component ) {
			$component_name = ( is_array( $component ) ) ? $component_key : $component;
			$component_args = ( is_array( $component ) ) ? $component : array();
			if ( ! $this->component_exists( $component_name ) ) {
				$component                            = titan_register_component( $component_name, $component_args );
				$this->components[ $component->name ] = $component;
			}
		}
	}

	/**
	 * Check if a `Titan_Component` object exists by `name`
	 *
	 * @see     Titan_Component
	 * @since   1.0.0
	 * @access  protected
	 *
	 * @param   string $name Name of the component.
	 *
	 * @return  bool
	 */
	protected function component_exists( $name ) {
		return isset( $this->components[ $name ] );
	}

	/**
	 * Get Builder Components
	 *
	 * @see     Titan_Component
	 * @since   1.0.0
	 * @access  public
	 *
	 * @return  array An array of 'Titan_Component' objects
	 */
	public function get_components() {
		return ( ! empty( $this->components ) ) ? $this->components : array();
	}

	/**
	 * Get a `Titan_Component` object by `name`
	 *
	 * @see     Titan_Component
	 * @since   1.0.0
	 * @access  public
	 *
	 * @param   string $name Name of the component.
	 *
	 * @return  object|bool     `Titan_Component` object. False otherwise.
	 */
	public function get_component( $name ) {
		return $this->component_exists( $name ) ? $this->components[ $name ] : false;
	}

	/* ====== ACF Builder JSON Methods ====== */

	/**
	 * Register Builder Fields
	 *
	 * @since   1.0.0
	 * @access  public
	 * @return  void
	 */
	public function register_acf_fields() {
		$this->fields = apply_filters( "titan/builder/{$this->name}/fields", array(
			'key'                   => 'group_569f263870eb8' . '_' . $this->name,
			'title'                 => $this->title,
			'fields'                => $this->get_builder_fields(),
			'location'              => $this->get_builder_locations(),
			'menu_order'            => $this->menu_order,
			'position'              => 'acf_after_title',
			'style'                 => apply_filters( "titan/builder/{$this->name}/style", 'seamless' ),
			'label_placement'       => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen'        => apply_filters( "titan/builder/{$this->name}/hide_on_screen", array(
				0  => 'the_content',
				1  => 'excerpt',
				2  => 'custom_fields',
				3  => 'discussion',
				4  => 'comments',
				5  => 'revisions',
				6  => 'slug',
				7  => 'author',
				8  => 'format',
				9  => 'categories',
				10 => 'tags',
				11 => 'send-trackbacks',
				12 => 'featured_image',
			) ),
			'active'                => 1,
			'description'           => '',
			'private'               => true,
			'local'                 => 'json'
		) );
		if ( function_exists( 'acf_add_local_field_group' ) && ! empty( $this->fields ) ) {
			acf_add_local_field_group( $this->fields );
		}
	}

	/**
	 * Get Builder Fields
	 *
	 * @since   1.0.0
	 * @access  public
	 * @return  array
	 */
	public function get_builder_fields() {
		return array(
			array(
				'key'               => 'field_569f264761037' . '_' . $this->name,
				'label'             => isset( $this->title ) ? $this->title : __( 'Builder', 'titan' ),
				'name'              => 'b' . '_' . $this->name,
				'type'              => 'flexible_content',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => 'hide-label',
					'id'    => '',
				),
				'button_label'      => $this->button_c_label,
				'min'               => isset( $this->min ) ? $this->min : '',
				'max'               => isset( $this->max ) ? $this->max : '',
				'layouts'           => $this->get_component_fields()
			)
		);
	}

	/**
	 * Get Builder Component Fields
	 *
	 * @since   1.0.0
	 *
	 * @access  public
	 * @return  array
	 */
	public function get_component_fields() {

		// components
		$component_counter = 0;
		$component_fields  = array();

		// validate
		if ( empty( $this->components ) ) {
			return $component_fields;
		}

		// go through components
		foreach ( $this->components as $component ) {

			// Fields
			$fields          = array();
			$comp_fields     = $component->get_acf_component_fields();
			$advanced_fields = $component->get_acf_advanced_fields();

			// Default Advanced Fields.
			$default_advanced_fields = $component->get_advanced_default_field_json();

			// Extract Fields.
			$name                    = titan_extract_var( $comp_fields, 'title' );
			$comp_fields             = titan_extract_var( $comp_fields, 'fields' );
			$advanced_fields         = titan_extract_var( $advanced_fields, 'fields' );
			$default_advanced_fields = titan_extract_var( $default_advanced_fields, 'fields' );

			// Determine if to combine
			$advanced_fields_default_enable = apply_filters( "titan/component/advanced/default/enable", true, $this );
			$advanced_fields_default_enable = apply_filters( "titan/component/{$component->name}/advanced/default/enable", $advanced_fields_default_enable, $this );

			// Combine
			if ( $advanced_fields_default_enable ) {
				if ( ! empty( $advanced_fields ) ) {
					$advanced_fields = array_merge_recursive( $advanced_fields, $default_advanced_fields );
				} else {
					$advanced_fields = $default_advanced_fields;
				}
			}

			// validate
			if ( ! $comp_fields || ! is_array( $comp_fields ) ) {
				continue;
			}

			// add `tab` for the component fields to live below
			$fields[] = array(
				'key'               => sprintf( 'field_569fc2bc6fa3d_%s_%s-contenttab', $this->name, $component_counter ),
				'label'             => 'Content',
				'name'              => '',
				'type'              => 'tab',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => 'component-content-tab',
					'id'    => '',
				),
				'placement'         => 'top',
				'endpoint'          => 0,
			);

			// add `cfields` to the subfields array
			foreach ( $comp_fields as $field ) {
				$field['key'] = sprintf( '%s_%s_%s', $field['key'], $this->name, $component_counter );
				$fields[]     = $field;
			}

			// add `advanced` tab to the componend field
			$fields[] = array(
				'key'               => sprintf( 'field_56a1b0a1a5664_%s_%s-advancedtab', $this->name, $component_counter ),
				'label'             => __( 'Advanced', 'titan' ),
				'name'              => '',
				'type'              => 'tab',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'placement'         => 'top',
				'endpoint'          => 0,
			);

			// Get layouts
			$comp_layouts = titan_component_get_repeater_layouts( $component->name );
			$comp_layouts = titan_component_get_repeater_layout_choices( $comp_layouts );

			// Component Layout
			$cl_args = apply_filters( "titan/component/{$component->name}/layout", array(
				'component_layouts' => true,
				'choices'           => $comp_layouts,
				'instructions'      => '',
				'label'             => __( 'Column Layout', 'titan' ),
			) );

			// make sure we want the repeater
			if ( $cl_args[ 'component_layouts' ] ) {
				$fields[] = array(
					'key'               => sprintf( 'field_57228a5f551f2_%s_%s-component_layout', $this->name, $component_counter ),
					'label'             => $cl_args[ 'label' ],
					'name'              => 'component_layout',
					'type'              => 'select',
					'instructions'      => $cl_args[ 'instructions' ],
					'required'          => 0,
					'conditional_logic' => 0,
					'wrapper'           => array(
						'width' => '',
						'class' => '',
						'id'    => '',
					),
					'choices'           => $cl_args[ 'choices' ],
					'default_value'     => array(),
					'allow_null'        => 0,
					'multiple'          => 0,
					'ui'                => 0,
					'ajax'              => 0,
					'placeholder'       => '',
					'disabled'          => 0,
					'readonly'          => 0,
				);
			}

			// add advanced fields
			if ( $advanced_fields && ! empty( $advanced_fields ) ) {
				foreach ( $advanced_fields as $cafield_key => $cafield ) {
					$cafield[ 'key' ] = sprintf( '%s_%s_%s', $cafield[ 'key' ], $this->name, $component_counter );
					$fields[]         = $cafield;
				}
			}

			// Always add class field.
			$fields[] = array(
				'key'               => sprintf( 'field_56a1b0aaa5665_%s_%s', $this->name, $component_counter ),
				'label'             => 'Class',
				'name'              => 'class',
				'type'              => 'text',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => '',
				'placeholder'       => '',
				'prepend'           => '',
				'append'            => '',
				'maxlength'         => '',
				'readonly'          => 0,
				'disabled'          => 0,
			);

			// Always add class field.
			$fields[] = array(
				'key'               => sprintf( 'field_56a1b0aaa56651_%s_%s', $this->name, $component_counter ),
				'label'             => 'ID',
				'name'              => 'id',
				'type'              => 'text',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => '',
				'placeholder'       => '',
				'prepend'           => '',
				'append'            => '',
				'maxlength'         => '',
				'readonly'          => 0,
				'disabled'          => 0,
			);

			// Actions.
			do_action( "titan/builder/{$this->name}/acf/advanced/fields", $fields, $this );
			do_action( "titan/builder/component/{$component->name}/acf/advanced/fields", $fields, $this );
			do_action( "titan/builder/{$this->name}/component/{$component->name}/acf/advanced/fields", $fields, $this );

			// Fix conditionals
			foreach ( $fields as $key => $field ) {
				if ( ! isset( $field[ 'conditional_logic' ] ) || ! is_array( $field[ 'conditional_logic' ] ) ) {
					continue;
				}
				foreach ( $field[ 'conditional_logic' ] as $ckey => $conditional ) {
					if ( is_array( $conditional ) ) {
						foreach ( $conditional as $cckey => $subconditional ) {
							$field[ 'conditional_logic' ][ $ckey ][ $cckey ][ 'field' ] = sprintf( '%s_%s_%s', $field[ 'conditional_logic' ][ $ckey ][ $cckey ][ 'field' ], $this->name, $component_counter );
						}
					}
				}
				$fields[ $key ][ 'conditional_logic' ] = $field[ 'conditional_logic' ];
			}

			// add component
			$component_fields[] = array(
				'key'        => sprintf( '57b33085ac96c126_%s_%s', $component->name, $component_counter ),
				'name'       => sprintf( 'c_%s', $component->name ),
				'label'      => ( isset( $name ) ) ? $name : $component->name,
				'display'    => 'block',
				'sub_fields' => $fields,
				'min'        => '',
				'max'        => ''
			);

			// increment
			$component_counter ++;
		}

		// return
		return $component_fields;
	}

	/**
	 * Builder Locations
	 *
	 * @since   1.0.0
	 * @access  public
	 * @return  array
	 */
	public function get_builder_locations() {

		// Location holder
		$location = array();

		// No Location defined
		if ( ! $this->locations ) {
			$location[] = $this->get_builder_default_location();
		}

		// Go through locations array
		foreach ( $this->locations as $loc_type => $loc ) {
			// locations are set for titan
			if ( ! is_numeric( $loc_type ) ) {
				foreach ( $loc as $a_loc ) {
					$location[][] = array(
						'param'    => $loc_type,
						'operator' => '==',
						'value'    => $a_loc
					);
				}
			} else {
				// use ACF default location format
				$location = $this->locations;
			}
		}

		// Return location
		return $location;
	}

	/**
	 * Get Default Location
	 *
	 * @since   1.0.0
	 * @access  public
	 * @return  array
	 */
	public function get_builder_default_location() {
		return apply_filters( "titan/builder/{$this->name}/default_location", array(
			array(
				'param'    => 'page_template',
				'operator' => '==',
				'value'    => 'template-custom.php'
			)
		) );
	}

	/* ====== Utility Methods ====== */

	/**
	 * Get Fields from JSON file
	 *
	 * @since   1.0.0
	 *
	 * @param   string $file
	 *
	 * @return  array|void
	 */
	public function get_fields_from_file( $file = '' ) {
		// bail early
		if ( ! $file ) {
			return;
		}

		// read json and get fields
		$fields = file_get_contents( $file );

		// validate
		if ( empty ( $fields ) ) {
			return;
		}

		// decode
		return json_decode( $fields, true );
	}

	/* ====== Magic Methods ====== */

	/**
	 * Magic method for getting layout object properties.
	 *
	 * @since   1.0.0
	 * @access  public
	 *
	 * @param   string $property
	 *
	 * @return  mixed
	 */
	public function __get( $property ) {
		return isset( $this->$property ) ? $this->args[ $property ] : null;
	}

	/**
	 * Magic method for setting layout object properties.
	 *
	 * @since   1.0.0
	 * @access  public
	 *
	 * @param   string $property
	 * @param   mixed  $value
	 *
	 * @return  void
	 */
	public function __set( $property, $value ) {
		if ( isset( $this->$property ) ) {
			$this->args[ $property ] = $value;
		}
	}

	/**
	 * Magic method for checking if a layout property is set.
	 *
	 * @since   1.0.0
	 * @access  public
	 *
	 * @param   string $property
	 *
	 * @return  bool
	 */
	public function __isset( $property ) {
		return isset( $this->args[ $property ] );
	}

	/**
	 * Don't allow properties to be unset.
	 *
	 * @since   1.0.0
	 * @access  public
	 *
	 * @param   string $property
	 *
	 * @return  void
	 */
	public function __unset( $property ) {}

	/**
	 * Magic method to use in case someone tries to output the layout object as a string.
	 * We'll just return the layout name.
	 *
	 * @since   1.0.0
	 * @access  public
	 * @return  string
	 */
	public function __toString() {
		return $this->name;
	}
}
