<?php
/**
 * Builder Factory class. This is a singleton factory class for handling the registering and
 * storing of `Titan_Builder` objects. Theme authors should utilize the API functions found
 * in `inc/functions-builder.php`.
 *
 * @package     Titan
 * @subpackage  Includes
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

/**
 * Builder Factory class. This is the backbone of the Builder API. Theme authors should
 * utilize the appropriate functions for accessing the `Titan_Builder_Factory` object.
 *
 * @since   1.0.0
 * @access  public
 */
class Titan_Builder_Factory {
	/**
	 * Builder Objects
	 *
	 * @since   1.0.0
	 * @access  public
	 * @var     array
	 */
	public $builders = array();

	/**
	 * Constructor method.
	 *
	 * @since   1.0.0
	 * @access  private
	 * @return  void
	 */
	private function __construct() {}

	/**
	 * Register a new builder object
	 *
	 * @see     Titan_Builder::__construct()
	 * @since   1.0.0
	 * @access  public
	 * @param   string  $name   Builder name.
	 * @param   array   $args   Builder arguments.
	 * @return  void
	 */
	public function register_builder( $name, $args = array() ) {
		if ( ! $this->builder_exists( $name ) ) {
			$builder = new Titan_Builder( $name, $args );
			$this->builders[ $builder->name ] = $builder;
		}
	}

	/**
	 * Unregisters a builder object.
	 *
	 * @since   1.0.0
	 * @access  public
	 * @param   string  $name   Name of the builder.
	 * @return  void
	 */
	public function unregister_builder( $name ) {
		if ( $this->builder_exists( $name ) ) {
			unset( $this->builders[ $name ] );
		}
	}

	/**
	 * Checks if a builder exists.
	 *
	 * @since   1.0.0
	 * @access  public
	 * @param   string  $name   Builder name.
	 * @return  bool
	 */
	public function builder_exists( $name ) {
		return isset( $this->builders[ $name ] );
	}

	/**
	 * Gets a builder object.
	 *
	 * @since   1.0.0
	 * @access  public
	 * @param   string  $name   Builder name.
	 * @return  object|bool
	 */
	public function get_builder( $name ) {
		return $this->builder_exists( $name ) ? $this->builders[ $name ] : false;
	}

	/**
	 * Returns the instance.
	 *
	 * @since   1.0.0
	 * @access  public
	 * @return  object
	 */
	public static function get_instance() {
		static $instance = null;
		if ( is_null( $instance ) ) {
			$instance = new Titan_Builder_Factory();
		}
		return $instance;
	}
}
