<?php
/**
 * Function to rewrite many urls to be relative.
 *
 * @package     Titan
 * @subpackage  Includes
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

# Add url re-writes
add_filter( 'bloginfo_url',          'titan_url_rewrite', 10 );
add_filter( 'the_permalink',         'titan_url_rewrite', 10 );
add_filter( 'wp_list_pages',         'titan_url_rewrite', 10 );
add_filter( 'wp_list_categories',    'titan_url_rewrite', 10 );
add_filter( 'wp_get_attachment_url', 'titan_url_rewrite', 10 );
add_filter( 'the_content_more_link', 'titan_url_rewrite', 10 );
add_filter( 'the_tags',              'titan_url_rewrite', 10 );
add_filter( 'get_pagenum_link',      'titan_url_rewrite', 10 );
add_filter( 'get_comment_link',      'titan_url_rewrite', 10 );
add_filter( 'month_link',            'titan_url_rewrite', 10 );
add_filter( 'day_link',              'titan_url_rewrite', 10 );
add_filter( 'year_link',             'titan_url_rewrite', 10 );
add_filter( 'term_link',             'titan_url_rewrite', 10 );
add_filter( 'the_author_posts_link', 'titan_url_rewrite', 10 );
add_filter( 'script_loader_src',     'titan_url_rewrite', 10 );
add_filter( 'style_loader_src',      'titan_url_rewrite', 10 );

/**
 * Relative URL Re-write
 *
 * @since   1.0.0
 * @access  public
 * @param   string  $input
 * @return  string
 */
function titan_url_rewrite( $input ) {
	$url = parse_url( $input );
	if ( ! isset( $url[ 'host' ] ) || ! isset( $url[ 'path' ] ) ) {
		return $input;
	}
	$site_url = parse_url( network_site_url() );
	if ( ! isset( $url[ 'scheme' ] ) ) {
		$url[ 'scheme' ] = $site_url[ 'scheme' ];
	}
	$hosts_match   = $site_url[ 'host' ] === $url[ 'host' ];
	$schemes_match = $site_url[ 'scheme' ] === $url[ 'scheme' ];
	$ports_exist   = isset( $site_url[ 'port' ] ) && isset( $url[ 'port' ] );
	$ports_match   = ( $ports_exist ) ? $site_url[ 'port' ] === $url[ 'port' ] : true;
	if ( $hosts_match && $schemes_match && $ports_match ) {
		return wp_make_link_relative( $input );
	}
	return $input;
}