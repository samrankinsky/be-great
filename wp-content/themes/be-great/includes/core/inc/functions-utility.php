<?php
/**
 * Additional helper functions that the framework or themes may use. The functions in this file are functions
 * that don't really have a home within any other parts of the framework.
 *
 * @package     Titan
 * @subpackage  Includes
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

/**
 * Function for setting the content width of a theme. This does not check if a content width has been set; it
 * simply overwrites whatever the content width is.
 *
 * @since   1.0.0
 * @access  public
 *
 * @param   int $width
 *
 * @return  void
 */
function titan_set_content_width( $width = '' ) {
	$GLOBALS[ 'content_width' ] = apply_filters( 'titan/content_width', absint( $width ) );
}

/**
 * Function for getting the theme's content width.
 *
 * @since   1.0.0
 * @access  public
 * @return  int
 */
function titan_get_content_width() {
	return absint( $GLOBALS[ 'content_width' ] );
}

/**
 * Retrieves the file with the highest priority that exists. The function searches both the stylesheet
 * and template directories. This function is similar to the locate_template() function in WordPress
 * but returns the file name with the URI path instead of the directory path.
 *
 * @since   1.0.0
 * @access  public
 * @link    http://core.trac.wordpress.org/ticket/18302
 *
 * @param   array $file_names The files to search for.
 *
 * @return  string
 */
function titan_locate_theme_file( $file_names ) {
	$located = '';
	// Loops through each of the given file names.
	foreach ( (array) $file_names as $file ) {
		// If the file exists in the stylesheet (child theme) directory.
		if ( is_child_theme() && file_exists( TITAN_CHILD_DIR . $file ) ) {
			$located = TITAN_CHILD_URI . $file;
			break;
		} // If the file exists in the template (parent theme) directory.
		elseif ( file_exists( TITAN_DIR . $file ) ) {
			$located = TITAN_URI . $file;
			break;
		}
	}

	return $located;
}

/**
 * Converts a hex color to RGB.  Returns the RGB values as an array.
 *
 * @since   1.0.0
 * @access  public
 *
 * @param   string $hex
 *
 * @return  array
 */
function titan_hex_to_rgb( $hex ) {
	// Remove "#" if it was added.
	$color = trim( $hex, '#' );

	// If the color is three characters, convert it to six.
	if ( 3 === strlen( $color ) ) {
		$color = $color[ 0 ] . $color[ 0 ] . $color[ 1 ] . $color[ 1 ] . $color[ 2 ] . $color[ 2 ];
	}

	// Get the red, green, and blue values.
	$red   = hexdec( $color[ 0 ] . $color[ 1 ] );
	$green = hexdec( $color[ 2 ] . $color[ 3 ] );
	$blue  = hexdec( $color[ 4 ] . $color[ 5 ] );

	// Return the RGB colors as an array.
	return array( 'r' => $red, 'g' => $green, 'b' => $blue );
}

/**
 * Helper function for getting the script/style `.min` suffix for minified files.
 *
 * @since   1.0.0
 * @access  public
 * @return  string
 */
function titan_get_min_suffix() {
	return defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';
}

/**
 * Helper function to a core assets url.
 *
 * @since   1.0.0
 * @access  public
 *
 * @param   string $file        Name of the file.
 * @param   string $type        Options include ( css, js, img )
 * @param   string $path_or_url Options include ( url, path )
 *
 * @retun   void
 */
function titan_asset( $file = '', $type = 'css', $path_or_url = 'url' ) {
	// script debug check
	$min = '.';

	// check dist directory
	$dist_path = apply_filters( 'titan/assets/dist/dir', TITAN_ASSETS_DIR );
	$dist_url  = apply_filters( 'titan/assets/dist/url', TITAN_ASSETS_URI );

	// check
	if ( ! file_exists( $dist_path . $file . $min . $type ) || ( defined( 'WP_ENV' ) && WP_ENV == 'dev' ) || ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ) {
		switch ( $type ) {
			case 'css' :
				$dist_url = TITAN_CSS;
				break;
			case 'js' :
				$dist_url = TITAN_JS;
				break;
			default :
				$dist_url = TITAN_CSS;
				break;
		}
	}

	if ( ! defined( 'WP_ENV' ) || ( defined( 'WP_ENV' ) && WP_ENV !== 'dev' ) ) {
		$min = '.min.';
	}

	// check img
	if ( $type == 'img' ) {
		return TITAN_IMG . $file;
	}

	return $dist_url . $file . $min . $type;
}

/**
 * Utility function for including a file if a theme feature is supported and the file exists.  Note
 * that this should not be used in place of the core `require_if_theme_supports()` function.  We need
 * this particular function for checking if the file exists first, which the core function does not
 * handle at the moment.
 *
 * @since   1.0.0
 * @access  public
 *
 * @param   string $feature
 * @param   string $file
 *
 * @return  void
 */
function titan_require_if_theme_supports( $feature, $file ) {
	if ( current_theme_supports( $feature ) && file_exists( $file ) ) {
		require_once( $file );
	}
}

/**
 * Check for the blog page.
 *
 * @since   1.0.0
 * @access  public
 * @return  bool
 */
function titan_is_blog() {
	global $post;
	$posttype = get_post_type( $post );

	return ( ( ( is_archive() ) || ( is_author() ) || ( is_category() ) || ( is_home() ) || ( is_single() ) || ( is_tag() ) ) && ( $posttype == 'post' ) ) ? true : false;
}

/**
 * Check if you are on a custom post type page.
 *
 * @since   1.0.0
 * @access  public
 *
 * @param   string $slug
 *
 * @return  bool
 */
function titan_is_cpt_page( $slug = '' ) {
	return ( ( is_post_type_archive( $slug ) || is_singular( $slug ) ) ) ? true : false;
}

/**
 * Display error alerts in admin panel
 *
 * @since   1.0.0
 * @access  public
 *
 * @param   mixed  $errors
 * @param   string $capability
 *
 * @return  void
 */
function titan_alerts( $errors, $capability = 'activate_plugins' ) {
	if ( ! did_action( 'init' ) ) {
		return add_action( 'init', function () use ( $errors, $capability ) {
			alerts( $errors, $capability );
		} );
	}
	$alert = function ( $message ) {
		echo '<div class="error"><p>' . $message . '</p></div>';
	};
	if ( call_user_func_array( 'current_user_can', (array) $capability ) ) {
		add_action( 'admin_notices', function () use ( $alert, $errors ) {
			array_map( $alert, (array) $errors );
		} );
	}
}

/**
 * Extract Var
 *
 * This function will remove the var from the array, and return the var
 *
 * @since   1.0.0
 * @access  public
 *
 * @param   mixed  $array
 * @param   string $key
 * @param   null   $default
 *
 * @return null|Titan_Component
 */
function titan_extract_var( &$array, $key, $default = null ) {
	if ( is_array( $array ) && array_key_exists( $key, $array ) ) {
		$v = $array[ $key ];

		return $v;
	}

	return $default;
}

/**
 * Diff a multi-dimensional array
 *
 * @param $array1
 * @param $array2
 *
 * @return array
 */
function titan_array_diff_multi( $array1, $array2 ) {
	$result = array();
	foreach ( $array1 as $key => $val ) {
		if ( isset( $array2[ $key ] ) ) {
			if ( is_array( $val ) && $array2[ $key ] ) {
				$result[ $key ] = titan_array_diff_multi( $val, $array2[ $key ] );
			}
		} else {
			$result[ $key ] = $val;
		}
	}

	return $result;
}

/**
 * Blog Pagination
 *
 * @since   1.0.0
 * @access  public
 *
 * @param   string $pages
 * @param   int    $range
 *
 * @return  void
 */
function titan_pagination( $pages = '', $range = 2 ) {
	$showitems = ( $range * 2 ) + 1;
	global $paged;
	if ( empty( $paged ) ) {
		$paged = 1;
	}
	if ( $pages == '' ) {
		global $wp_query;
		$pages = $wp_query->max_num_pages;
		if ( ! $pages ) {
			$pages = 1;
		}
	}
	if ( 1 != $pages ) {
		echo "<ul class='pagination pagination-lg'>";
		if ( $paged > 2 && $paged > $range + 1 && $showitems < $pages ) {
			echo "<li><a href='" . get_pagenum_link( 1 ) . "'>&laquo;</a></li>";
		}
		if ( $paged > 1 && $showitems < $pages ) {
			echo "<li><a href='" . get_pagenum_link( $paged - 1 ) . "'>&lsaquo;</a></li>";
		}
		for ( $i = 1; $i <= $pages; $i ++ ) {
			if ( 1 != $pages && ( ! ( $i >= $paged + $range + 1 || $i <= $paged - $range - 1 ) || $pages <= $showitems ) ) {
				echo ( $paged == $i ) ? "<li class='active'><span>" . $i . "<span class='sr-only'>(current)</span></span></li>" : "<li><a href='" . get_pagenum_link( $i ) . "' class='inactive' >" . $i . "</a></li>";
			}
		}
		if ( $paged < $pages && $showitems < $pages ) {
			echo "<li><a href='" . get_pagenum_link( $paged + 1 ) . "'>&rsaquo;</a></li>";
		}
		if ( $paged < $pages - 1 && $paged + $range - 1 < $pages && $showitems < $pages ) {
			echo "<li><a href='" . get_pagenum_link( $pages ) . "'>&raquo;</a></li>";
		}
		echo "</ul>\n";
	}
}

/**
 * Debug
 *
 * @uses    error_log()
 * @since   1.0.0
 * @access  public
 *
 * @param   mixed $message
 *
 * @return  void
 */
function titan_debug( $message ) {
	if ( WP_DEBUG === true ) {
		if ( is_array( $message ) || is_object( $message ) ) {
			error_log( print_r( $message, true ) );
		} else {
			error_log( $message );
		}
	}
}
