<?php
/**
 * Functions for handling search in the framework. Themes can add support for the
 * 'titan-search' feature to allow the framework to handle search rewrites.
 *
 * @package     Titan
 * @subpackage  Includes
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

add_action( 'template_redirect',        'titan_search_redirect'      );
add_filter( 'wpseo_json_ld_search_url', 'titan_search_wpseo_rewrite' );

/**
 * Redirects search results from /?s=query to /search/query/,
 * converts %20 to +
 *
 * @since   1.0.0
 * @access  public
 * @link    http://txfx.net/wordpress-plugins/nice-search/
 * @return  void
 */
function titan_search_redirect() {
	global $wp_rewrite;
	if ( ! isset( $wp_rewrite ) || ! is_object( $wp_rewrite ) || ! $wp_rewrite->get_search_permastruct() ) {
		return;
	}
	$search_base = $wp_rewrite->search_base;
	if ( is_search() && ! is_admin() && strpos( $_SERVER[ 'REQUEST_URI' ], "/{$search_base}/" ) === false && strpos( $_SERVER[ 'REQUEST_URI' ], '&' ) === false ) {
		wp_redirect( get_search_link() );
		exit();
	}
}

/**
 * WPSEO: Rewrite
 *
 * @since   1.0.0
 * @access  public
 * @param   string  $url
 * @return  mixed
 */
function titan_search_wpseo_rewrite( $url ) {
	return str_replace( '/?s=', '/search/', $url );
}