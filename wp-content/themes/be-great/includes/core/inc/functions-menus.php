<?php
/**
 * Menu Functions for defining and registering WordPress navigation menus. Additional helper
 * functions for dealing with menus.
 *
 * @package     Titan
 * @subpackage  Includes
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

# Add Titan Core Menus
add_action( 'titan/loaded', 'titan_register_menus', 1 );

/**
 * Register the default menus.
 *
 * @uses    register_nav_menus()
 * @since   1.0.0
 * @access  public
 * @return  void
 */
function titan_register_menus() {
	register_nav_menus( apply_filters( 'titan/menus', array(
		'primary_navigation' => __( 'Primary Navigation', 'titan' )
	) ) );
}

/**
 * Function for grabbing a WP nav menu theme location name.
 *
 * @since   1.0.0
 * @access  public
 *
 * @param   string $location
 *
 * @return  string
 */
function titan_get_menu_location_name( $location ) {
	$locations = get_registered_nav_menus();
	return isset( $locations[ $location ] ) ? $locations[ $location ] : '';
}

/**
 * Function for grabbing a WP Nav Menu name based on theme location.
 *
 * @since   1.0.0
 * @access  public
 *
 * @param   string $location
 *
 * @return  string
 */
function titan_get_menu_name( $location ) {
	$locations = get_nav_menu_locations();
	return isset( $locations[ $location ] ) ? wp_get_nav_menu_object( $locations[ $location ] )->name : '';
}
