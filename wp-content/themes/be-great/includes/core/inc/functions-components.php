<?php
/**
 * Components API - An API for themes to build components.
 *
 * Theme Components was created to allow theme developers to easily create themes with dynamic layout
 * structures. This file merely contains the API function calls at theme developers' disposal.
 * See `inc/class-component.php` and `inc/class-components-factory.php` for the muscle behind the API.
 *
 * @package     Titan
 * @subpackage  Includes
 * @version     1.0.0
 * @author      Skyhook Interactive <support@skyhookinteractive.com>
 * @copyright   Copyright (c) 2016, Skyhook Interactive
 * @link        https://bitbucket.org/skyhook/titan
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

# Registers components
add_action( 'titan/loaded', 'titan_register_components', 0 );

/**
 * Returns the instance of the `Titan_Component_Factory` object. Use this function to access the object.
 *
 * @see     Titan_Component_Factory
 *
 * @since   1.0.0
 *
 * @return  object
 */
function titan_components() {
	return Titan_Component_Factory::get_instance();
}

/**
 * Function for registering a component.
 *
 * @see     Titan_Component_Factory::register_component()
 *
 * @since   1.0.0
 *
 * @param   string $name
 * @param   array  $args
 *
 * @return  void
 */
function titan_register_component( $name, $args = array() ) {
	return titan_components()->register_component( $name, $args );
}

/**
 * Unregisters a component.
 *
 * @see     Titan_Component_Factory::unregister_component()
 *
 * @since   1.0.0
 *
 * @param   string $name
 *
 * @return  void
 */
function titan_unregister_component( $name ) {
	titan_components()->unregister_component( $name );
}

/**
 * Checks if a component exists.
 *
 * @see     Titan_Component_Factory::component_exists()
 *
 * @since   1.0.0
 *
 * @param   string $name
 *
 * @return  bool
 */
function titan_component_exists( $name ) {
	return titan_components()->component_exists( $name );
}

/**
 * Returns an array of registered component objects.
 *
 * @see     Titan_Component_Factory::components
 *
 * @since   1.0.0
 *
 * @return  array
 */
function titan_get_components() {
	return titan_components()->components;
}

/**
 * Returns a component object if it exists. Otherwise, `FALSE`.
 *
 * @see     Titan_Component_Factory::get_component()
 * @see     Titan_Component
 *
 * @since   1.0.0
 *
 * @param   string $name
 *
 * @return  objtitant|bool
 */
function titan_get_component_by_name( $name ) {
	return titan_components()->get_component( $name );
}

/**
 * Function for getting a component by name.
 *
 * @since   1.0.0
 *
 * @param   string $name The component name.
 *
 * @return  string
 */
function titan_get_component( $name ) {

	// Initial check.
	if ( empty( $name ) || ! class_exists( 'acf' ) ) {
		return;
	}

	// Init component.
	global $component;
	$component = titan_get_component_by_name( $name );

	// Include template.
	if ( file_exists( $component->template ) ) {
		include $component->template;
	}
}

/**
 * Returns default theme supported components.
 *
 * @since   1.0.0
 *
 * @return  string
 */
function titan_get_default_components() {
	$support = get_theme_support( 'theme-components' );
	return isset( $support[ 0 ] ) ? $support[ 0 ] : '';
}

/**
 * Register Component Layotus
 *
 * @since   1.0.0
 *
 * @return  void
 */
function titan_register_components() {

	// Get default components.
	$components = apply_filters( 'titan/components/default', titan_get_default_components() );

	// Register default components.
	if ( ! empty( $components ) ) {
		foreach ( $components as $component_name => $component_args ) {
			if ( is_numeric( $component_name ) ) {
				$component_name = $component_args;
				$component_args = array();
			}
			titan_register_component( $component_name, $component_args );
		}
	}

	// Hook to register additional theme components.
	do_action( 'titan/components', $components );
}

/**
 * Component Location Helper
 *
 * @since   1.0.0
 *
 * @param   string  $condition_type
 * @param   string  $condition_location
 *
 * @return  array
 */
function titan_component_location_helper( $condition_type = 'post_type', $condition_location = 'titan-nothing' ) {
	return array(
		array(
			array(
				'param'    => $condition_type,
				'operator' => '==',
				'value'    => $condition_location
			)
		)
	);
}

/**
 * Component Part
 *
 * @since   1.0.0
 *
 * @param   string $component The name of the component
 * @param   string $slug      The slug of the template -- all lowercase.
 * @param   string $name      The name of the template -- all lowercase.
 *
 * @return  void
 */
function titan_component_part( $component_name, $slug, $name = '' ) {

	// Check
	if ( ! $component_name ) {
		return;
	}

	// Template Path
	$path = titan_component_get_path( $component_name );

	// Template + Name -- More specific first
	if ( $name ) {
		$templates[] = "{$slug}-{$name}.php";
	}

	// Template - Name
	$templates[] = "{$slug}.php";

	// Include
	foreach ( $templates as $template ) {
		$template = $path . $template;
		if ( file_exists( $template ) ) {
			include $template;
			break;
		}
	}
}

/**
 * Component Path
 *
 * @since   1.0.0
 *
 * @param   string $component_name The name of the component
 *
 * @return  string
 */
function titan_component_get_path( $component_name ) {
	$component = titan_get_component_by_name( $component_name );
	return ( $component ) ? $component->path : apply_filters( "titan/component/{$component_name}/path", trailingslashit( TITAN_COMPONENTS . $component_name ) );
}

/**
 * Get Component Advanced by Key
 *
 * @since   1.0.0
 *
 * @param   string $key
 *
 * @return  bool|string|array
 */
function titan_component_get_advanced_by_key( $key ) {

	// Check Key
	if ( empty( $key ) ) {
		return false;
	}

	// Load Component
	global $component;
	$advanced = $component->get_advanced();

	// Check Key.
	if ( is_array( $advanced ) && array_key_exists( $key, $advanced ) ) {
		return $advanced[$key];
	}

	// Default.
	return false;
}

/**
 * Remove Component layout support
 *
 * @since   1.0.0
 *
 * @return  array
 */
function titan_component_remove_layout_support() {
	return array( 'component_layouts' => false );
}

/**
 * Component Class
 *
 * @since   1.0.0
 *
 * @param   string  $class
 *
 * @return  string
 */
function titan_component_class( $class = '' ) {
	global $component;

	// Vars.
	$classes = apply_filters( 'titan/component/classes/default', array( 'component' ) );

	// Check Builder Component
	if ( $component->get_is_builder_component() ) {
		$classes[] = 'component-builder';
		$classes[] = sprintf( 'component-builder-%s', $component->get_counter() );
	}

	// Add name.
	if ( $component->name ) {
		$classes[] = $component->name;
	}

	// Add additional classes.
	if ( ! empty( $class ) ) {
		if ( ! is_array( $class ) ) {
			$class = preg_split( '#\s+#', $class );
		}
		$classes = array_merge( $classes, $class );
	}

	// Get advanced fields.
	$advanced = $component->get_advanced();

	// Add the class from advanced.
	if ( isset( $advanced['class'] ) && '' !== $advanced['class'] ) {
		$classes[] = $advanced[ 'class' ];
	}

	// Security.
	$classes = array_map( 'esc_attr', $classes );

	// Filters.
	$classes = apply_filters( 'titan/component/classes', $classes, $component );
	$classes = apply_filters( "titan/component/{$component->name}/classes", $classes, $component );

	// Unique values.
	$classes = array_unique( $classes );

	// Output.
	echo 'class="' . join( ' ', $classes ) . '"';
}

/**
 * Component Id
 *
 * @since   1.0.0
 *
 * @param   string  $id
 *
 * @return  string
 */
function titan_component_id( $id = '' ) {

	global $component;

	// Define Ids'
	$ides = array();

	// Add additional classes.
	if ( ! empty( $id ) ) {
		if ( ! is_array( $id ) ) {
			$id = preg_split( '#\s+#', $id );
		}
		$ides = array_merge( $ides, $id );
	}

	// Get advanced fields.
	$advanced = $component->get_advanced();

	// Add the class from advanced.
	if ( isset( $advanced['id'] ) && '' !== $advanced['id'] ) {
		$ides[] = $advanced['id'];
	}

	// Security.
	$ides = array_map( 'esc_attr', $ides );

	// Filters.
	$ides = apply_filters( 'titan/component/id', $ides, $id );
	$ides = apply_filters( "titan/component/{$component->name}/id", $ides, $id );

	// Unique values.
	$ides = array_unique( $ides );

	// If Empty
	if ( empty( $ides ) ) {
		return;
	}

	// Output.
	echo 'id="' . join( ' ', $ides ) . '"';
}

/**
 * Component Styles
 *
 * @since   1.0.0
 *
 * @return  string $styles
 */
function titan_component_style() {
	global $component;

	// Vars.
	$styles    = '';
	$advanced = $component->get_advanced();

	// Check
	if ( empty( $advanced ) || ! is_array( $advanced ) ) {
		return $styles;
	}

	// Margin/Padding styles.
	$advanced = titan_component_get_margin_padding_styles( $advanced );

	// Image Styles.
	$advanced = titan_component_get_background_styles( $advanced );

	// Check blacklisted properties.
	if ( array_key_exists( 'width', $advanced ) ) {
		unset( $advanced['width'] );
	}

	// Filter.
	$advanced = apply_filters( "titan/component/{$component->name}/styles", $advanced );

	// Populate the $styles variable.
	if ( is_array( $advanced ) ) {
		foreach ( $advanced as $key => $val ) {
			if ( is_array( $val ) || $val == '' || $key == 'class' || $key == 'id' ) {
				continue;
			}
			$styles .= $key . ':' . str_replace( '"', "'", $val ) . ';';
		}
	}

	if ( ! $styles ) {
		return $styles;
	}

	// Style
	$styles = 'style="' . str_replace( '"', "'", $styles ) . '"';

	// Filter
	$styles = apply_filters( "titan/component/{$component->name}/styles/output", $styles, $advanced );

	// Output.
	echo $styles;
}

/**
 * Get margin padding styles
 *
 * @since   1.0.0
 *
 * @param   array $advanced
 *
 * @return  array $advanced
 */
function titan_component_get_margin_padding_styles( $advanced ) {

	// global
	global $component;

	// setup key checks
	$properties = array(
		'margin-top',
		'margin-bottom',
		'padding-top',
		'padding-bottom'
	);

	// loop through each property to set when entered in CMS
	foreach ( $properties as $property ) {
		if ( array_key_exists( $property, $advanced ) && '' != $advanced[ $property ] ) {
			$advanced[ $property ] = $advanced[ $property ] . 'px';
		}
	}

	// Filter
	$advanced = apply_filters( "titan/component/{$component->name}/styles/margin_padding", $advanced );

	// Return
	return $advanced;
}

/**
 * Get the advanced image styles
 *
 * @since   1.0.0
 *
 * @param   array $advanced
 *
 * @return  array $advanced
 */
function titan_component_get_background_styles( $advanced ) {
	global $component;

	// setup the image styles from ACF fields
	if ( isset( $advanced['bg'] ) && 'color' === $advanced['bg'] ) {
		$advanced['background'] = $advanced['color'];
	} else if ( isset( $advanced['bg'] ) && 'image' === $advanced['bg'] && isset( $advanced['image']['url'] ) ) {
		$advanced[ 'background' ]      = 'url("' . $advanced[ 'image' ][ 'url' ] . '") no-repeat center center fixed';
		$advanced[ 'background-size' ] = 'cover';
	}

	// Filter
	$advanced = apply_filters( "titan/component/{$component->name}/styles/background", $advanced );

	// Unset these variables, because they are not used in the final styles.
	unset( $advanced[ 'bg' ] );
	unset( $advanced[ 'image' ] );
	unset( $advanced[ 'color' ] );

	// Return
	return $advanced;
}

/**
 * Titan Component Repeater
 *
 * This creates the layout for a repeater based on
 * optional arguements.
 *
 * @since   1.0.0
 *
 * @param   string  $repeater_name
 * @param   array   $args
 *
 * @return  string
 */
function titan_component_repeater( $repeater, $args = array() ) {
	global $component;

	// setup the default
	$defaults = array(
		'echo'                  => true,                                   // echo or return
		'file'                  => 'partials/item.php',                    // repeater content
		'layout'                => get_sub_field( 'component_layout' ),    // layout OR column class
		'section_class'         => 'repeater-' . $component->name,         // section class
		'section_id'            => false,                                  // section ID
		'container'             => false,                                  // include a container?
		'container_class'       => 'container',                            // container class
		'row_class'             => 'row',                                  // row class
		'column_class'          => '',                                     // additional column classes (other than layout)
	);

	// merge site defaults with defaults
	$defaults = apply_filters( 'titan/component/repeater/defaults', $defaults );

	// merge defaults with items set
	$args = wp_parse_args( $args, $defaults );

	// add one more filter
	$args = apply_filters( "titan/component/{$component->name}/repeater/args", $args );

	// extract args into variables
	extract( $args );

	// Check 'echo'
	if ( ! $echo ) {
		ob_start();
	}

	// check if the repeater field has rows of data
	if ( have_rows( $repeater ) ) { ?>
		<div <?php echo ( $section_id ) ? 'id="' . $section_id . '"' : ''; ?> class="<?php echo $section_class; ?>">
			<?php if ( $container ){ ?>
			<div class="<?php echo $container_class; ?>">
				<?php } ?>
				<div class="<?php echo $row_class; ?>">
					<?php $component->counter = 0; while ( have_rows( $repeater ) ) { the_row(); ?>
						<div class="<?php echo titan_component_repater_get_column_classes( $layout ); ?><?php echo ( $column_class ) ? ' ' . $column_class : ''; ?>">
							<?php if ( file_exists( $component->path . $file ) ) { ?>
								<?php include $component->path . $file ?>
							<?php } ?>
						</div>
						<?php $component->counter++; } ?>
				</div>
				<?php if ( $container ) { ?>
			</div>
		<?php } ?>
		</div>
	<?php }

	// Check 'echo'
	if ( ! $echo ) {
		$html = ob_get_contents();
		ob_end_clean();
		return $html;
	}
}

/**
 * Titan Get Component Repeater Column Classes
 *
 * Returns the class for the layout that should be used.
 *
 * @since   1.0.0
 *
 * @param   string $layout which layout to use (1_col, 2_col, etc), otherwise classes to be used
 *
 * @return  string  Classes for layout
 */
function titan_component_repater_get_column_classes( $layout = 'col-xs-12' ) {
	global $component;

	// Get repeater layouts.
	$layouts = titan_component_get_repeater_layouts( $component->name );

	// check if layout column key/slug was passed and isset.
	if ( ! array_key_exists( $layout, $layouts ) ) {
		return $layout;
	}

	// get classes for that layout.
	foreach ( $layouts as $key => $value ) {
		if ( $key === $layout ) {
			if ( is_array( $value[ 'classes' ] ) ) {
				if ( $component->counter == count( $value[ 'classes' ] ) ) {
					$component->counter = 0;
				}
				return $value[ 'classes' ][ $component->counter ];
			}
			return $value[ 'classes' ];
		}
	}

	// Return.
	return $layout;
}

/**
 * Set the default layouts
 *
 * @since   1.0.0
 *
 * @param   bool    $component_name
 *
 * @return  array   $layouts
 */
function titan_component_get_repeater_layouts( $component_name = '' ) {

	// get default layouts
	$layouts = titan_component_get_default_repeater_layouts();

	// Check component name
	if ( $component_name ){
		$layouts = apply_filters( "titan/component/{$component_name}/repeater/layouts", $layouts );
	}

	// return layouts
	return $layouts;
}

/**
 * Get the default layouts for repeaters
 *
 * @since   1.0.0
 *
 * @return  array
 */
function titan_component_get_default_repeater_layouts() {
	return apply_filters( 'titan/component/repeater/layouts', array(
		'1_col' => array(
			'name'    => __( 'One Column', 'titan' ),
			'classes' => 'col-xs-12',
		),
		'2_col' => array(
			'name'    => __( 'Two Column', 'titan' ),
			'classes' => 'col-xs-12 col-sm-6',
		),
		'3_col' => array(
			'name'    => __( 'Three Column', 'titan' ),
			'classes' => 'col-xs-12 col-sm-4',
		),
		'4_col' => array(
			'name'    => __( 'Four Column', 'titan' ),
			'classes' => 'col-xs-12 col-sm-3',
		),
		'33_66' => array(
			'name' => __( 'One Third / Two Third' ),
			'classes' => array(
				'col-xs-12 col-sm-4',
				'col-xs-12 col-sm-8'
			)
		),
		'66_33' => array(
			'name' => __( 'Two Third / One Third' ),
			'classes' => array(
				'col-xs-12 col-sm-8',
				'col-xs-12 col-sm-4'
			)
		)
	) );
}

/**
 * Get layout choices
 *
 * @since   1.0.0
 *
 * @param   array $layouts The available layouts.
 *
 * @return  array $layouts The available layouts restructered to choices.
 */
function titan_component_get_repeater_layout_choices( $layouts ) {
	foreach ( $layouts as $key => $val ) {
		$layouts[ $key ] = $layouts[ $key ][ 'name' ];
	}
	return $layouts;
}
