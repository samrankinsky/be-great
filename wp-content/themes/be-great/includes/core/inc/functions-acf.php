<?php
/**
 * Advanced custom field functions.
 *
 * @package     Titan
 * @subpackage  Includes
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

# Include ACF Manually
add_filter( 'acf/settings/path', 'titan_acf_settings_path' );
add_filter( 'acf/settings/dir', 'titan_acf_settings_dir' );

# ACF Component Rule Types
add_filter( 'acf/location/rule_types', 'titan_acf_rule_types', 0 );
add_filter( 'acf/location/rule_values/titan_component', 'titan_acf_titan_component_rule_values', 0 );
add_filter( 'acf/location/rule_values/titan_acomponent', 'titan_acf_titan_component_rule_values', 0 );

# ACF Save & Load JSON paths
add_filter( 'acf/settings/save_json', 'titan_acf_json_save' );
add_filter( 'acf/settings/load_json', 'titan_acf_json_load' );

# Save Component Fields
add_action( 'acf/update_field_group', 'titan_acf_update_component_fields', 10, 5 );
add_action( 'acf/duplicate_field_group', 'titan_acf_update_component_fields', 10, 5 );
add_action( 'acf/untrash_field_group', 'titan_acf_update_component_fields', 10, 5 );

# Delete Component Fields
// add_action( 'acf/trash_field_group', 'titan_acf_delete_component_fields', 10, 5 );
// add_action( 'acf/delete_field_group', 'titan_acf_delete_component_fields', 10, 5 );

/**
 * ACF Settings Path
 *
 * @since  1.0.0
 * @access public
 *
 * @param $path
 *
 * @return string
 */
function titan_acf_settings_path( $path ) {
	$path = apply_filters( 'titan/acf/settings_path', TITAN_ACF );

	return $path;
}

/**
 * ACF Settings Directory
 *
 * @since  1.0.0
 * @access public
 *
 * @param $path
 *
 * @return string
 */
function titan_acf_settings_dir( $dir ) {
	$dir = apply_filters( 'tittan/acf/settings_uri', TITAN_ACF_URI );

	return $dir;
}

/**
 * ACF Rule Types
 *
 * @since   1.0.0
 * @access  public
 *
 * @param   array   $choices
 *
 * @return  array   $choices
 */
function titan_acf_rule_types( $choices ) {

	// Add Titan Choice
	$choices[ __( 'Titan', 'titan' ) ]['titan_component']  = __( 'Component - Fields', 'titan' );
	$choices[ __( 'Titan', 'titan' ) ]['titan_acomponent'] = __( 'Component - Advanced Fields', 'titan' );

	return $choices;
}

/**
 * Populate the Values of the Rule Type 'titan_component'
 *
 * @since   1.0.0
 * @access  public
 *
 * @param   array   $choices
 *
 * @return  array   $choices
 */
function titan_acf_titan_component_rule_values( $choices ) {

	// Get Regular Components.
	$components = titan_get_components();
	if ( $components ) {
		foreach ( $components as $component ) {
			$choices[ $component->name ] = $component->get_name_formatted();
		}
	}

	// Sort them by Key
	ksort( $choices );

	// Return Choices
	return $choices;
}

/**
 * Save ACF JSON
 *
 * @since   1.0.0
 * @access  public
 *
 * @param   $path
 *
 * @return  mixed
 */
function titan_acf_json_save( $path ) {
	$path = apply_filters( 'titan/acf/save_path', TITAN_ACF_JSON );

	return $path;
}

/**
 * Load ACF JSON
 *
 * @since   1.0.0
 * @access  public
 *
 * @param   $path
 *
 * @return  mixed
 */
function titan_acf_json_load( $paths ) {
	unset( $paths[0] );
	$paths[] = apply_filters( 'titan/acf/load_path', TITAN_ACF_JSON );

	return apply_filters( 'titan/acf/load_paths', $paths );
}

/**
 * Update ACF Component Fields
 *
 * @since   1.0.0
 * @access  public
 *
 * @param   array $field_group
 *
 * @return void
 */
function titan_acf_update_component_fields( $field_group ) {
	// Get Location
	$locations = isset( $field_group['location'] ) ? $field_group['location'] : false;

	// Check
	if ( ! $locations ) {
		return;
	}

	// Define Holder
	$location = array();

	// Go through each location and determin if the component matches
	foreach ( $locations as $location_option ) {

		// Check.
		if ( ! isset( $location_option[0] ) ) {
			continue;
		}

		// Check.
		if ( ! isset( $location_option[0]['param'] ) ) {
			continue;
		}

		// Check.
		if ( ! in_array( $location_option[0]['param'], array( 'titan_acomponent', 'titan_component' ) ) ) {
			continue;
		}

		// Make Location
		$location = $location_option[0];
	}

	// Check
	if ( empty( $location ) || ! isset( $location['value'] ) ) {
		return false;
	}

	// Get Component Name
	$component_name = isset( $location['value'] ) ? $location['value'] : '';

	// Check
	if ( empty( $component_name ) ) {
		return false;
	}

	// Get Component by Name
	$component = titan_get_component_by_name( $component_name );

	// Check
	if ( ! is_object( $component ) ) {
		return false;
	}

	// ACF Component Path
	$path = isset( $component->path ) ? untrailingslashit( $component->path ) : '';
	$file = ( isset( $location['param'] ) && 'titan_acomponent' === $location['param'] ) ? 'advanced.json' : 'acf.json';

	// Check if is writable and bail early if it does not.
	if ( ! $path || ! is_writable( $path ) ) {
		return false;
	}

	// Get Fields
	$field_group['fields'] = acf_get_fields( $field_group );

	// Prepare for export
	$id = acf_extract_var( $field_group, 'ID' );
	$field_group = acf_prepare_field_group_for_export( $field_group );

	// Add Modified Time
	$field_group['modified'] = get_post_modified_time('U', true, $id, true);

	// Write File
	$f = fopen( "{$path}/{$file}", 'w' );
	fwrite( $f, acf_json_encode( $field_group ) );
	fclose( $f );

	// Delete original file
	$original_path = acf_get_setting( 'save_json' );
	$original_path = untrailingslashit( $original_path );
	$original_file = $field_group['key'] . '.json';

	// Is Readable.
	if ( is_readable( "{$original_path}/{$original_file}" ) ) {

		// Remove
		unlink("{$original_path}/{$original_file}");
	}

	// Return
	return true;
}

/**
 * Delete ACF Component Fields
 *
 * @since   1.0.0
 * @access  public
 *
 * @param   array $field_group
 *
 * @return void
 */
function titan_acf_delete_component_fields( $field_group ) {

	// Get Location
	$location = isset( $field_group['location'][0][0] ) ? $field_group['location'][0][0] : false;

	// Check
	if ( ! $location || ( isset( $location['param'] ) && ! in_array( $location['param'], array( 'titan_acomponent', 'titan_component' ) ) ) ) {
		return false;
	}

	// Get Component Name
	$component_name = isset( $location['value'] ) ? $location['value'] : '';

	// Check
	if ( empty( $component_name ) ) {
		return false;
	}

	// Get Component by Name
	$component = titan_get_component_by_name( $component_name );

	// Check
	if ( ! is_object( $component ) ) {
		return false;
	}

	// ACF Component Path
	$path = isset( $component->path ) ? untrailingslashit( $component->path ) : '';
	$file = ( isset( $location['param'] ) && 'titan_acomponent' === $location['param'] ) ? 'advanced.json' : 'acf.json';

	// Check if is writable and bail early if it does not.
	if ( ! $path || ! is_readable( "{$path}/{$file}" ) ) {
		return false;
	}

	// Delete
	unlink("{$path}/{$file}");

	// Return
	return true;
}
