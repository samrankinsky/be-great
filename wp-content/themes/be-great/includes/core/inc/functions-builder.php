<?php
/**
 * Builder API - An API for themes to register and handle page builders.
 *
 * Theme Builder was created to allow theme developers to easily create themes with dynamic layout
 * structures. This file merely contains the API function calls at theme developers' disposal.
 * See `inc/class-builder.php` and `inc/class-builder-factory.php` for the muscle behind the API.
 *
 * @package     Titan
 * @subpackage  Includes
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

# Registers default builders
add_action( 'titan/loaded', 'titan_register_builders', 1 );

/**
 * Returns the instance of the `Titan_Builder_Factory` object. Use this function to access the object.
 *
 * @see     Titan_Builder_Factory
 * @since   1.0.0
 * @access  public
 * @return  object
 */
function titan_builder_instance() {
	return Titan_Builder_Factory::get_instance();
}

/**
 * Function for registering a builder.
 *
 * @see     Titan_Builder_Factory::register_builder()
 * @since   1.0.0
 * @access  public
 *
 * @param   string $name
 * @param   array  $args
 *
 * @return  void
 */
function titan_register_builder( $name, $args = array() ) {
	titan_builder_instance()->register_builder( $name, $args );
}

/**
 * Unregisters a builder.
 *
 * @see     Titan_Builder_Factory::unregister_builder()
 * @since   1.0.0
 * @access  public
 *
 * @param   string $name
 *
 * @return  void
 */
function titan_unregister_builder( $name ) {
	titan_builder_instance()->unregister_builder( $name );
}

/**
 * Checks if a builder exists.
 *
 * @see     Titan_Builder_Factory::builder_exists()
 * @since   1.0.0
 * @access  public
 *
 * @param   string $name
 *
 * @return  bool
 */
function titan_builder_exists( $name ) {
	return titan_builder_instance()->builder_exists( $name );
}

/**
 * Returns an array of registered builder objects.
 *
 * @see     Titan_Builder_Factory::builders
 * @since   1.0.0
 * @access  public
 * @return  Titan_Builder
 */
function titan_get_builders() {
	return titan_builder_instance()->builders;
}

/**
 * Returns a builder object if it exists. Otherwise, `FALSE`.
 *
 * @see     Titan_Builder_Factory::get_builder()
 * @see     Titan_Builder
 * @since   1.0.0
 * @access  public
 *
 * @param   string $name
 *
 * @return  Titan_Builder
 */
function titan_get_builder( $name ) {
	return titan_builder_instance()->get_builder( $name );
}

/**
 * Returns an array of registered component objects by builder name if it exists. Otherwise, `FALSE`.
 *
 * @see     Titan_Builder_Factory::get_builder()
 * @see     Titan_Builder
 * @since   1.0.0
 * @access  public
 *
 * @param   string $name
 *
 * @return  object|bool
 */
function titan_get_components_by_builder( $name ) {
	return titan_builder_instance()->get_builder( $name )->components;
}

/**
 * Returns a registered component object if it exists. Otherwise, `FALSE`.
 *
 * @see     Titan_Builder_Factory::get_builder()
 * @see     Titan_Builder::get_component()
 * @see     Titan_Builder
 * @since   1.0.0
 * @access  public
 *
 * @param   string $name
 * @param   string $component
 *
 * @return  object|bool
 */
function titan_get_builder_component( $name, $component ) {
	return titan_builder_instance()->get_builder( $name )->get_component( $component );
}

/**
 * Get the default builder added in `add_theme_support`
 * inside functions.php file.
 *
 * @since   1.0.0
 * @access  public
 * @return  string
 */
function titan_get_default_builders() {
	$builders = get_theme_support( 'theme-builder' );
	return isset( $builders[0] ) ? $builders[0] : '';
}

/**
 * Register Builders
 *
 * This function registers the default builders defined
 * in the `titan/setup` hook. It also allows other developers
 * to register other builders via the `titan/builders` hook.
 *
 * @since   1.0.0
 * @access  public
 * @return  void
 */
function titan_register_builders() {

	// Get default builders.
	$builders = apply_filters( 'titan/builders/default', titan_get_default_builders() );

	// Register default builders.
	if ( ! empty ( $builders ) ) {
		foreach ( $builders as $builder_name => $builder_args ) {
			if ( is_numeric( $builder_name ) ) {
				$component_name = $builder_args;
				$builder_args   = array();
			}
			titan_register_builder( $builder_name, $builder_args );
		}
	}

	// Action to register additional builders.
	do_action( 'titan/builders', $builders );
}

/**
 * Titan Builder - Get Component
 *
 * This function is used to get the correct
 * component file by component name.
 *
 * @see     Titan_Builder_Factory::get_builder()
 * @see     Titan_Builder::builders
 * @see     Titan_Builder
 * @since   1.0.0
 * @access  public
 *
 * @param   string $name The name of the builder layout field.
 *
 * @return  void
 */
function titan_builder_get_component( $builder_name = '', $name = '', $counter = 1 ) {
	global $component;
	$component = titan_get_builder_component( $builder_name, str_replace( 'c_', '', $name ) );
	$component->set_is_builder_component();
	$component->load_advanced();
	$component->set_counter( $counter );
	if ( file_exists( $component->template ) ) {
		include $component->template;
	}
}

/**
 * Titan Builder
 *
 * The main builder function. You should use this function in
 * the main builder template file.
 *
 * @see     Titan_Builder_Factory::get_builder()
 * @see     Titan_Builder::builders
 * @see     Titan_Builder
 * @since   1.0.0
 * @access  public
 *
 * @param   int $id The ID of the post.
 *
 * @return  void
 */
function titan_builder( $builder = '', $id = '' ) {

	// get the builder(s)
	if ( $builder == '' ) {
		$builder = titan_get_builders();
	}

	// get ID of post
	$id = ( $id ) ? $id : get_the_ID();

	if ( ! is_array( $builder ) ) {
		// just get one builder and output
		titan_single_builder( $builder, $id );
	} else {
		// loop through all builders and output
		foreach ( $builder as $builder_name => $build ) {
			titan_single_builder( $builder_name, $id );
		}
	}
}

/***
 * Titan single builder
 *
 * @since   1.0.0
 * @access  public
 *
 * @param   string $builder_name The builder name
 * @param   int    $id           The ID of the post.
 */
function titan_single_builder( $builder_name, $id = '' ) {
	$id = ( $id ) ? $id : get_the_ID();
	$counter = 1;
	if ( have_rows( 'b' . '_' . $builder_name, $id ) ) {
		while ( have_rows( 'b' . '_' . $builder_name, $id ) ) {
			the_row();
			titan_builder_get_component( $builder_name, get_row_layout(), $counter );
			$counter++;
		}
	}
}
