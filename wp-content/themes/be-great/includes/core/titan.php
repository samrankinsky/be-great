<?php
/**
 * Titan - A WordPress theme development framework.
 *
 * Titan is a framework for developing WordPress themes. The framework allows theme developers
 * to quickly build themes without having to handle all of the "logic" behind the theme or having to
 * code complex functionality for features that are often needed in themes. The framework does these
 * things for developers to allow them to get back to what matters the most: developing and designing
 * themes. Themes handle all the markup, style, and scripts while the framework handles the logic.
 *
 * @package     Titan
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */
if ( ! class_exists( 'Titan' ) ) {
	/**
	 * The Titan class launches the framework. It's the organizational structure behind the
	 * entire framework. This class should be loaded and initialized before anything else within
	 * the theme is called to properly use the framework.
	 *
	 * After parent themes call the Titan class, they should perform a theme setup function on
	 * the `titan/setup` hook with a priority no later than 10. This allows the class to
	 * load theme-supported features at the appropriate time, which is on the `after_setup_theme`
	 * hook with a priority of 5.
	 *
	 * @since   1.0.0
	 * @access  public
	 */
	class Titan {
		/**
		 * Constructor method for the Titan class. This method adds other methods of the
		 * class to specific hooks within WordPress. It controls the load order of the
		 * required files for running the framework.
		 *
		 * @since   1.0.0
		 * @access  public
		 * @return  void
		 */
		public function __construct() {

			// Set up an empty object to work with.
			$GLOBALS[ 'titan' ] = new stdClass;

			// Setup constants
			$this->constants();

			// Utility
			require_once( TITAN_INC . 'functions-utility.php'  );

			// Set up the load order.
			add_action( 'after_setup_theme', array( $this, 'core'          ), 0 );
			add_action( 'after_setup_theme', array( $this, 'setup'         ), 1 );
			add_action( 'after_setup_theme', array( $this, 'includes'      ), 2 );
			add_action( 'after_setup_theme', array( $this, 'extensions'    ), 3 );
			add_action( 'after_setup_theme', array( $this, 'admin'         ), 4 );
			add_action( 'after_setup_theme', array( $this, 'loaded'        ), 5 );
		}

		/**
		 * Defines the constant paths for use within the core framework,
		 * parent theme, and child theme.
		 *
		 * @since   1.0.0
		 * @access  public
		 * @return  void
		 */
		public function constants() {

			// Sets the framework version number.
			define( 'TITAN_VERSION', '1.0.0' );

			// Sets the domain.
			define( 'TITAN_DOMAIN', 'titan' );

			// Theme directory paths.
			define( 'TITAN_DIR',       trailingslashit( get_template_directory()   ) );
			define( 'TITAN_CHILD_DIR', trailingslashit( get_stylesheet_directory() ) );

			// Theme directory URIs.
			define( 'TITAN_URI',       trailingslashit( get_template_directory_uri()   ) );
			define( 'TITAN_CHILD_URI', trailingslashit( get_stylesheet_directory_uri() ) );

			// Sets the path to the includes directory.
			if ( ! defined( 'TITAN_INC_DIR' ) ) {
				define( 'TITAN_INC_DIR', trailingslashit( TITAN_DIR . 'includes' ) );
			}

			// Sets the path to the includes directory URI.
			if ( ! defined( 'TITAN_INC_URI' ) ) {
				define( 'TITAN_INC_URI', trailingslashit( TITAN_URI . 'includes' ) );
			}

			// Sets the path to the core framework directory.
			if ( ! defined( 'TITAN_CORE_DIR' ) ) {
				define( 'TITAN_CORE_DIR', trailingslashit( TITAN_INC_DIR . 'core' ) );
			}

			// Sets the path to the core framework directory URI.
			if ( ! defined( 'TITAN_CORE_URI' ) ) {
				define( 'TITAN_CORE_URI', trailingslashit( TITAN_INC_URI . 'core' ) );
			}

			// Core framework directory paths.
			define( 'TITAN_INC',   trailingslashit( TITAN_CORE_DIR . 'inc'   ) );
			define( 'TITAN_EXT',   trailingslashit( TITAN_CORE_DIR . 'ext'   ) );
			define( 'TITAN_JSON',  trailingslashit( TITAN_CORE_DIR . 'json'  ) );

			// Core Admin directory paths.
			define( 'TITAN_ADMIN', trailingslashit( TITAN_CORE_DIR . 'admin' ) );
			define( 'TITAN_ADMIN_URI', trailingslashit( TITAN_CORE_URI . 'admin' ) );

			// Core paths to layouts and components.
			define( 'TITAN_LAYOUTS',    trailingslashit( TITAN_INC_DIR . 'layouts'    ) );
			define( 'TITAN_COMPONENTS', trailingslashit( TITAN_INC_DIR . 'components' ) );

			// Core assets directory url.
			define( 'TITAN_ASSETS_URI', trailingslashit( TITAN_URI . 'assets' ) );
			define( 'TITAN_ASSETS_DIR', trailingslashit( TITAN_DIR . 'assets' ) );

			// Specific core assets directory urls.
			define( 'TITAN_FONTS',  trailingslashit( TITAN_ASSETS_URI . 'fonts' ) );
			define( 'TITAN_IMG',    trailingslashit( TITAN_ASSETS_URI . 'img'   ) );
			define( 'TITAN_CSS',    trailingslashit( TITAN_ASSETS_URI . 'css'   ) );
			define( 'TITAN_JS',     trailingslashit( TITAN_ASSETS_URI . 'js'    ) );

			// Dist directory.
			define( 'TITAN_ASSETS_DIST_DIR', trailingslashit( TITAN_ASSETS_DIR . 'dist' ) );
			define( 'TITAN_ASSETS_DIST_URI', trailingslashit( TITAN_ASSETS_URI . 'dist' ) );

			// ACF JSON PATH.
			define( 'TITAN_ACF', trailingslashit( TITAN_CORE_DIR . 'acf' ) );
			define( 'TITAN_ACF_URI', trailingslashit( TITAN_CORE_URI . 'acf' ) );
			define( 'TITAN_ACF_JSON', trailingslashit( TITAN_INC_DIR . 'acf-fields' ) );
		}

		/**
		 * Loads the core framework files.
		 *
		 * @since   1.0.0
		 * @access  public
		 * @return  void
		 */
		public function core() {

			// Include core classes.
			require_once( TITAN_INC . 'class-cpt.php' );

			// Include core functions.
			require_once( TITAN_INC . 'functions-i18n.php'     );
			require_once( TITAN_INC . 'functions-menus.php'    );
			require_once( TITAN_INC . 'functions-cleanup.php'  );
			require_once( TITAN_INC . 'functions-scripts.php'  );
			require_once( TITAN_INC . 'functions-styles.php'   );
			require_once( TITAN_INC . 'functions-titles.php'   );
			require_once( TITAN_INC . 'functions-sidebar.php'  );
			require_once( TITAN_INC . 'functions-template.php' );

			// Hook.
			do_action( 'titan/core' );
		}

		/**
		 * Setup Features using add_theme_support
		 *
		 * @since   1.0.0
		 * @access  public
		 * @return  void
		 */
		public function setup() {

			// Automatically add <title> to head.
			add_theme_support( 'title-tag' );

			// Add core WordPress thumbnails support
			add_theme_support( 'post-thumbnails' );

			// Adds core WordPress HTML5 support.
			add_theme_support( 'html5', array( 'caption', 'comment-form', 'comment-list', 'gallery', 'search-form' ) );

			// Adds support for the Nav Walker
			add_theme_support( 'theme-nav-walker' );

			// Theme Wrapper.
			add_theme_support( 'theme-wrapper' );

			// Theme Relative Urls.
			add_theme_support( 'theme-relative-urls' );

			// Theme ACF.
			add_theme_support( 'theme-acf' );

			// Theme Components
			add_theme_support( 'theme-components', array() );

			// Theme Builders
			add_theme_support( 'theme-builder', array() );

			// Shorfcode Helper
			add_theme_support( 'theme-shortcode-helper' );

			// Hook to handle additional setup definitions
			do_action( 'titan/setup' );
		}

		/**
		 * Loads the framework files supported by themes. Functionality in these files should
		 * not be expected within the theme setup function.
		 *
		 * @since  1.0.0
		 * @access public
		 * @return void
		 */
		public function includes() {

			// Load Theme Relative URLS if `theme-relative-urls` is supported.
			titan_require_if_theme_supports( 'theme-relative-urls', TITAN_INC . 'functions-relative-urls.php' );

			// Load the Theme Wrapper if `theme-wrapper` is supported.
			titan_require_if_theme_supports( 'theme-wrapper', TITAN_INC . 'class-theme-wrapper.php'     );
			titan_require_if_theme_supports( 'theme-wrapper', TITAN_INC . 'functions-theme-wrapper.php' );

			// Load the Theme Wrapper if `theme-wrapper` is supported.
			titan_require_if_theme_supports( 'theme-nav-walker', TITAN_INC . 'class-nav-walker.php'     );
			titan_require_if_theme_supports( 'theme-nav-walker', TITAN_INC . 'functions-nav-walker.php' );

			// Load the ACF functions if `theme-acf` is supported.
			titan_require_if_theme_supports( 'theme-acf', TITAN_ACF . 'acf.php' );
			titan_require_if_theme_supports( 'theme-acf', TITAN_INC . 'functions-acf.php' );
			titan_require_if_theme_supports( 'theme-acf-save-local', TITAN_ACF . 'acf.php' );
			titan_require_if_theme_supports( 'theme-acf-save-local', TITAN_INC . 'functions-acf.php' );

			// Load the Components Modules if `theme-components` is supported.
			titan_require_if_theme_supports( 'theme-components', TITAN_INC . 'class-component.php' );
			titan_require_if_theme_supports( 'theme-components', TITAN_INC . 'class-component-factory.php' );
			titan_require_if_theme_supports( 'theme-components', TITAN_INC . 'functions-components.php' );

			// Load the Theme Builder if `theme-builder` supported.
			titan_require_if_theme_supports( 'theme-builder', TITAN_INC . 'class-component.php'       );
			titan_require_if_theme_supports( 'theme-builder', TITAN_INC . 'class-builder.php'         );
			titan_require_if_theme_supports( 'theme-builder', TITAN_INC . 'class-builder-factory.php' );
			titan_require_if_theme_supports( 'theme-builder', TITAN_INC . 'functions-builder.php'     );

			// Hook.
			do_action( 'titan/includes' );
		}

		/**
		 * Load extensions (external projects). Extensions are projects that are included
		 * within the framework but are not a part of it. They are external projects
		 * developed outside of the framework. Themes must use `add_theme_support( $extension )`
		 * to use a specific extension within the theme.  This should be declared on
		 * `after_setup_theme` no later than a priority of 11.
		 *
		 * @since   1.0.0
		 * @access  public
		 * @return  void
		 */
		public function extensions() {

			// Hook to load additional extensions.
			do_action( 'titan/extensions' );
		}

		/**
		 * Titan Admin
		 *
		 * This loads all the files for the admin of the
		 * titan framework.
		 *
		 * @since   1.0.0
		 * @access  public
		 * @return  void
		 */
		public function admin() {
			if ( is_admin() ) {
				// Admin.
				require_once( TITAN_ADMIN . 'functions-admin.php' );

				// Shortcode Helper
				titan_require_if_theme_supports( 'theme-shortcode-helper', TITAN_ADMIN . 'class-shortcode-helper.php' );

				// Hook.
				do_action( 'titan/admin' );
			}
		}

		/**
		 * Titan Loaded
		 *
		 * At this point in the execution process, all setup items have
		 * been defined and all files included. At this point you may
		 * start executing core functions.
		 *
		 * @since   1.0.0
		 * @access  public
		 * @return  void
		 */
		public function loaded() {

			// Hooks.
			do_action( 'titan/loaded' );
			do_action( 'titan/init'   );
		}
	}
}

/** Initalize the Framework */
new Titan();
