<?php
/**
 * Shortcode Helper Class.
 *
 * @package     Titan
 * @subpackage  Includes
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

/* Titan Shortcake Class */
if ( ! class_exists( 'Titan_Shortcode_Helper' ) ) {
	/**
	 * Class Titan_Shortcode_Helper
	 *
	 * @since 1.0.0
	 */
	class Titan_Shortcode_Helper {
		/**
		 * Singleton instance
		 *
		 * @since 1.0.0
		 * @var Titan_Shortcode_Helper
		 */
		protected static $instance = null;

		/**
		 * Shortcodes
		 *
		 * @since   1.0.0
		 * @var     array
		 */
		private $shortcodes = array();

		/**
		 * Content Access Instance
		 *
		 * Ensures only one instance of Titan_Shortcode_Helper
		 * is loaded or can be loaded.
		 *
		 * @since   1.0.0
		 *
		 * @static
		 *
		 * @return  Titan_Shortcode_Helper - Main Instance
		 */
		public static function instance() {
			if ( is_null( self::$instance ) ) {
				self::$instance = new self();
			}

			return self::$instance;
		}

		/**
		 * Constructor
		 *
		 * @since 1.0.0
		 */
		public function __construct() {

			/* Shortcake UI Dependency */
			if ( ! class_exists( 'Shortcode_UI' ) ) {
				return;
			}

			/* Helper Media Button */
			remove_action( 'media_buttons', array( Shortcode_UI::get_instance(), 'action_media_buttons' ) );
			add_action( 'media_buttons', array( $this, 'register_media_button' ) );

			/* Helper Enqueue Scripts */
			add_action( 'enqueue_shortcode_ui', array( $this, 'register_scripts_styles' ) );

			/* Add Shortcodes */
			add_action( 'register_shortcode_ui', array( $this, 'register_shortcodes' ) );
		}

		/**
		 * Register New Media Button
		 *
		 * @since 1.0.0
		 *
		 * @param int $editor_id
		 *
		 * @return void
		 */
		public function register_media_button( $editor_id ) {
			echo apply_filters( 'titan/admin/shortcode_helper/media_button_html', sprintf(
				'<button type="button" class="button shortcake-add-post-element" data-editor="%s">' .
					'<span class="wp-media-buttons-icon dashicons dashicons-screenoptions"></span> %s' .
				'</button>',
				esc_attr( $editor_id ),
				esc_html__( 'Add Content', 'titan' )
			) );
		}

		/**
		 * Register Scripts Styles
		 *
		 * @since 1.0.0
		 *
		 * @return void
		 */
		public function register_scripts_styles() {
			/* Get Current Screen */
			$screen = get_current_screen();

			/* Helper CSS */
			wp_register_style( 'titan-shortcode-helper-css', TITAN_ADMIN_URI . 'assets/css/titan-shortcode-helper.css', array(), TITAN_VERSION, 'all' );
			wp_register_script( 'titan-shortcode-helper-js', TITAN_ADMIN_URI . 'assets/js/titan-shortcode-helper.js', array( 'jquery', 'shortcode-ui', 'underscore', 'backbone' ), TITAN_VERSION, false );

			/* Only Enqueue on Post or Page Screens */
			if ( in_array( $screen->id, apply_filters( 'titan/admin/shortcode_helper/screens', array( 'post', 'page' ) ) ) ) {
				wp_enqueue_style( 'titan-shortcode-helper-css' );
				wp_enqueue_script( 'titan-shortcode-helper-js' );
				wp_localize_script( 'titan-shortcode-helper-js', ' shortcodeHelperData', array(
					'strings' => apply_filters( 'titan/admin/shortcode_helper/ui_strings', array(
						'media_frame_title'                 => __( 'Insert Content', 'titan' ),
						'media_frame_menu_insert_label'     => __( 'Insert Content', 'titan' ),
						'media_frame_menu_update_label'     => __( '%s Content Details', 'titan' ),
						'media_frame_toolbar_insert_label'  => __( 'Insert Content', 'titan' ),
						'media_frame_toolbar_update_label'  => __( 'Update Content', 'titan' ),
						'media_frame_no_attributes_message' => __( 'There are no attributes to configure for this content. Please click the "Insert Content Element" button below.', 'titan' ),
						'mce_view_error'                    => __( 'Failed to load content preview', 'titan' ),
						'search_placeholder'                => __( 'Search Content', 'titan' ),
						'insert_content_label'              => __( 'Insert Content', 'titan' ),
					) ),
				) );
			}
		}

		/**
		 * Register Shortcodes
		 *
		 * @since 1.0.0
		 * @return void
		 */
		public function register_shortcodes() {
			$this->shortcodes = apply_filters( 'titan/shortcode_helper/shortcodes', $this->shortcodes );
			foreach ( $this->shortcodes as $ui_shortcode => $ui_shortcode_args ) {
				if ( ! empty( $ui_shortcode_args ) && function_exists( 'shortcode_ui_register_for_shortcode' ) ) {
					shortcode_ui_register_for_shortcode( $ui_shortcode, $ui_shortcode_args );
				}
			}
		}
	}
}

/* Initialize */
Titan_Shortcode_Helper::instance();
