( function ( $, wp, Backbone ) {

	// Change labels when adding using the 'Add Media' button
	$( document ).ready( function () {
		if ( shortcodeUIData && shortcodeHelperData ) {
			shortcodeUIData.strings = shortcodeHelperData.strings;
		}
	} );

	// Add Popup Shortocde
	var popupShortcodeEditor = function ( shortcode ) {
		// Check
		if ( !shortcodeUIData || !shortcodeHelperData || !shortcode ) {
			return;
		}

		var shortcode = _.findWhere( shortcodeUIData.shortcodes, {
			shortcode_tag: shortcode
		} );

		var wpshortcode = new wp.shortcode( {
			tag  : shortcode.shortcode_tag,
			attrs: shortcode.attrs
		} );

		var shortcodeModel = window.Shortcode_UI.utils.shortcodeViewConstructor.getShortcodeModel( wpshortcode );

		// Define media frame
		var wp_media_frame = wp.media.frames.wp_media_frame = wp.media( {
			frame           : "post",
			state           : 'shortcode-ui',
			currentShortcode: shortcodeModel
		} );

		// Open
		wp_media_frame.open();

		// Focus media search
		$( '.media-button' ).text( 'Insert Shortcode' );
	};

}( jQuery, wp, Backbone ) );
