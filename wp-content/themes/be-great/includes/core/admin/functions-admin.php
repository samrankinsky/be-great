<?php
/**
 * Theme administration functions used with other components of the framework admin. This file is for
 * setting up any basic features and holding additional admin helper functions.
 *
 * @package     Titan
 * @subpackage  Admin
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

# Enqueue Assets needed in admin
add_action( 'admin_enqueue_scripts', 'titan_admin_enqueue_assets', 100 );

/**
 * Enqueue Admin Scripts and Styles
 *
 * @since   1.0.0
 * @access  public
 * @return  void
 */
function titan_admin_enqueue_assets() {

	// Font Awesome
	if ( true === apply_filters( 'titan_admin_font_awesome', true ) ) {
		wp_enqueue_style( 'titan/admin/fontawesome', TITAN_ADMIN_URI . 'assets/css/font-awesome.min.css', array(), TITAN_VERSION, 'all' );
	}

	// Admin Styles
	wp_enqueue_style( 'titan/admin', TITAN_ADMIN_URI . 'assets/css/titan-admin.css', array( 'titan/admin/fontawesome' ), TITAN_VERSION, 'all' );

	// Hook.
	do_action( 'titan/admin/assets' );
}
