<?php
/**
 * Front Page Template File
 *
 * This template is used on the home page
 * of the site when set in settings.
 *
 * @package Titan_Theme
 * @since   1.0.0
 */
the_post();

# Output Builder
titan_builder( 'hero-builder' );

# Output Builder
titan_builder( 'builder' );
