<?php
/**
 * Page Template File
 *
 * This template is used on most pages when a more
 * specific template does not exist.
 *
 * @package Titan_Theme
 * @since   1.0.0
 */

the_post();
get_template_part( 'templates/title' );
get_template_part( 'templates/page' );
