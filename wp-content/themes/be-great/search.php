<?php
/**
 * Search Template
 *
 * @package Titan_Theme
 * @since   1.0.0
 */


get_template_part( 'templates/title' );
get_template_part( 'templates/no-results' );

while ( have_posts() ) { the_post();
	get_template_part( 'templates/search' );
}
