<?php
/**
 * Titan Theme Functions
 *
 * @package     Titan_Theme
 * @subpackage  Functions
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

/* Version */
define( 'TITAN_THEME_VERSION', '1.0.0' );

/* Paths */
define( 'TITAN_THEME_PATH', trailingslashit( get_template_directory() ) );
define( 'TITAN_THEME_INC', trailingslashit( TITAN_THEME_PATH . 'includes' ) );
define( 'TITAN_THEME_CORE', trailingslashit( TITAN_THEME_INC . 'core' ) );
define( 'TITAN_THEME_LIB', trailingslashit( TITAN_THEME_INC . 'lib' ) );

/* Urls */
define( 'TITAN_THEME_URL', trailingslashit( get_template_directory_uri() ) );

/* Assets */
define( 'TITAN_THEME_ASSETS', trailingslashit( TITAN_THEME_URL . 'assets' ) );
define( 'TITAN_THEME_FONTS', trailingslashit( TITAN_THEME_URL . 'fonts' ) );
define( 'TITAN_THEME_IMG', trailingslashit( TITAN_THEME_ASSETS . 'img' ) );
define( 'TITAN_THEME_CSS', trailingslashit( TITAN_THEME_ASSETS . 'css' ) );
define( 'TITAN_THEME_JS', trailingslashit( TITAN_THEME_ASSETS . 'js' ) );

/* Google Maps API Key */
define( 'TITAN_THEME_GM_API_KEY', 'AIzaSyCFaUFGpgkEVkD4OEMyoIjRrb6sZOIHavo' );

/* Post excerpt length */
define( 'POST_EXCERPT_LENGTH', 40 );

/* Core Framework */
require_once( TITAN_THEME_CORE . 'titan.php' );

/* Other Theme Libraries */
require_once( TITAN_THEME_LIB . 'setup.php'  );
require_once( TITAN_THEME_LIB . 'assets.php' );
require_once( TITAN_THEME_LIB . 'custom.php' );
